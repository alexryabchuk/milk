<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        'css/theme-default.css',
        'css/site.css',
        'css/style.css',
        'css/screen.css',
        'css/site.css',
    ];
    public $js = [
        'js/plugins/jquery/jquery-ui.min.js',
        'js/plugins/bootstrap/bootstrap.min.js',
        'js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js',
        'js/plugins.js',
        'js/actions.js',
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
