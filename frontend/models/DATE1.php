<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT_E".
 *
 * @property integer $id
 * @property integer $DAT_ID
 * @property integer $N
 * @property integer $NO
 * @property string $SM
 * @property string $SE
 * @property string $TX
 * @property string $TXPR
 * @property string $TXSM
 * @property string $DTPR
 * @property string $DTSM
 * @property integer $TXTY
 * @property integer $TXAL
 * @property integer $CS
 * @property integer $VD
 */
class DATE1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT_E';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DAT_ID', 'N', 'NO', 'SM', 'SE', 'TXPR', 'TXSM', 'DTPR', 'DTSM', 'TXTY', 'TXAL', 'CS', 'VD'], 'required'],
            [['DAT_ID', 'N', 'NO', 'TXTY', 'TXAL', 'CS', 'VD'], 'integer'],
            [['SM', 'SE', 'TXPR', 'TXSM', 'DTPR', 'DTSM'], 'number'],
            [['TX'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'DAT_ID' => 'Dat  ID',
            'N' => 'N',
            'NO' => 'No',
            'SM' => 'Sm',
            'SE' => 'Se',
            'TX' => 'Tx',
            'TXPR' => 'Txpr',
            'TXSM' => 'Txsm',
            'DTPR' => 'Dtpr',
            'DTSM' => 'Dtsm',
            'TXTY' => 'Txty',
            'TXAL' => 'Txal',
            'CS' => 'Cs',
            'VD' => 'Vd',
        ];
    }
}
