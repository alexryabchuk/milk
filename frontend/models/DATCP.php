<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT_CP".
 *
 * @property integer $id
 * @property integer $T
 * @property integer $N
 * @property integer $C
 * @property string $CD
 * @property string $NM
 * @property string $SM
 * @property string $Q
 * @property string $PRC
 * @property string $TX
 * @property integer $W
 * @property integer $DAT_ID
 */
class DATCP extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT_CP';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['T', 'N', 'C', 'W', 'DAT_ID'], 'integer'],
            [['C', 'DAT_ID'], 'required'],
            [['SM', 'Q', 'PRC'], 'number'],
            [['CD'], 'string', 'max' => 13],
            [['NM'], 'string', 'max' => 120],
            [['TX'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'T' => 'T',
            'N' => 'N',
            'C' => 'C',
            'CD' => 'Cd',
            'NM' => 'Nm',
            'SM' => 'Sm',
            'Q' => 'Q',
            'PRC' => 'Prc',
            'TX' => 'Tx',
            'W' => 'W',
            'DAT_ID' => 'Dat  ID',
        ];
    }
}
