<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "LOST_PACKAGE".
 *
 * @property integer $id
 * @property string $cash_id
 * @property integer $package_no
 */
class lostpackage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LOST_PACKAGE';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cash_id', 'package_no'], 'required'],
            [['package_no'], 'integer'],
            [['cash_id'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cash_id' => 'Cash ID',
            'package_no' => 'Package No',
        ];
    }
}
