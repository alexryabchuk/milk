<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT_VD".
 *
 * @property integer $id
 * @property integer $DAT_ID
 * @property integer $N
 * @property integer $NI
 */
class DATVD extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT_VD';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DAT_ID', 'N', 'NI'], 'required'],
            [['DAT_ID', 'N', 'NI'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'DAT_ID' => 'Dat  ID',
            'N' => 'N',
            'NI' => 'Ni',
        ];
    }
}
