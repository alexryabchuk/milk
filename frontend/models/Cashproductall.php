<?php

namespace frontend\models;

use yii\base\Model;

/**
 * Signup form
 */
class Cashproductall extends Model
{
    public $cashdatestart;
    public $cashdateend;

    public function __construct($config = [])
    {
        $this->cashdatestart = date("Y-m-d");
        $this->cashdateend = date("Y-m-d");
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['cashdatestart', 'date'],
            ['cashdateend', 'date'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'cashdatestart' => 'Початкова дата',
            'cashdateend' => 'Кінцева дата',
        ];
    }

}
