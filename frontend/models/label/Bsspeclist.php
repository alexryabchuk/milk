<?php

namespace frontend\models\label;

use Yii;
use backend\models\Bsparam;
use common\models\Bsproductlist1;

/**
 * This is the model class for table "bsspeclist".
 *
 * @property int $id
 * @property int $pid
 * @property int $num
 * @property int $varka
 * @property string $madedate
 * @property string $specdate
 * @property int $used
 * @property int $country_id
 * @property int $ceh_id
 *
 * @property Bsspecitem[] $bsspecitems
 * @property Bsparam $ceh
 * @property Bsproductlist1 $p
 */
class Bsspeclist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bsspeclist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'num', 'varka', 'used', 'country_id', 'ceh_id'], 'integer'],
            [['madedate', 'specdate'], 'safe'],
            [['ceh_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bsparam::className(), 'targetAttribute' => ['ceh_id' => 'id']],
            [['pid'], 'exist', 'skipOnError' => true, 'targetClass' => Bsproductlist1::className(), 'targetAttribute' => ['pid' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Продукція',
            'num' => '№',
            'varka' => 'Варка',
            'madedate' => 'Дата виготовлення',
            'specdate' => 'Дата',
            'used' => 'Used',
            'country_id' => 'Країна',
            'ceh_id' => 'Підрозділ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBsspecitems()
    {
        return $this->hasMany(Bsspecitem::className(), ['pid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCeh()
    {
        return $this->hasOne(Bsparam::className(), ['id' => 'ceh_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getP()
    {
        return $this->hasOne(Bsproductlist1::className(), ['id' => 'pid']);
    }
}