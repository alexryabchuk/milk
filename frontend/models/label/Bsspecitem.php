<?php

namespace frontend\models\label;

use Yii;
use backend\models\Bsparam;

/**
 * This is the model class for table "bsspeclist".
 *
 * @property int $id
 * @property int $pid
 * @property int $sid
 * @property int $varka
 * @property string $madedate
 * @property string $specdate
 * @property int $used
 * @property int $country_id
 * @property int $ceh_id
 *
 * @property Bsspecitem[] $bsspecitems
 * @property Bsparam $ceh
 * @property Bsproductlist1 $p
 */
class Bsspecitem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bsspecitem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'sid', 'place', 'massa', 'yasch'], 'integer'],
            [['useddate', 'barcode1', 'barcode2'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Продукція',
            'sid' => '№ специфікації',
            'place' => 'Місце',
            'useddate' => 'Дата придатності',
            'massa' => 'маса',
            'yasch' => '№ ящика',
            'barcode1' => 'Штрихкод1',
            'barcode2' => 'Штрихкод2',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBsspecitems()
    {
        return $this->hasMany(Bsspecitem::className(), ['pid' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCeh()
    {
        return $this->hasOne(Bsparam::className(), ['id' => 'ceh_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getP()
    {
        return $this->hasOne(Bsproductlist1::className(), ['id' => 'pid']);
    }

    public static function getSpecZvit($sid)
    {
        return static::find()->select(['yasch', 'count(yasch) as countHeads', 'sum(massa) as massanetto'])->where(['sid' => $sid])->groupBy(['yasch'])->asArray()->all();

    }
}