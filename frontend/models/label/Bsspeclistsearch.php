<?php

namespace frontend\models\label;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\label\Bsspeclist;

/**
 * This is the model class for table "bsspeclist".
 *
 * @property int $id
 * @property int $pid
 * @property int $num
 * @property int $varka
 * @property string $madedate
 * @property string $specdate
 * @property int $used
 * @property int $country_id
 * @property int $ceh_id
 *
 * @property Bsspecitem[] $bsspecitems
 * @property Bsparam $ceh
 * @property Bsproductlist1 $p
 */
class Bsspeclistsearch extends Bsspeclist
{

    public function rules()
    {
        return [
            [['pid', 'num', 'varka', 'used', 'country_id', 'ceh_id'], 'integer'],
            [['madedate', 'specdate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bsspeclist::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'pid' => $this->pid,
            'num' => $this->num,
            'varka' => $this->varka,
            'ceh_id' => $this->ceh_id,
            'madedate' => $this->madedate,
            'specdate' => $this->specdate,
            'country_id' => $this->country_id,
        ]);

        return $dataProvider;
    }
}