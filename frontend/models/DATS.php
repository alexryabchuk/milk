<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT_S".
 *
 * @property integer $id
 * @property integer $DAT_ID
 * @property integer $N
 * @property integer $TR
 * @property integer $TY
 * @property string $ST
 * @property integer $NI
 * @property string $TX
 * @property string $PR
 * @property string $SM
 */
class DATS extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT_S';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DAT_ID', 'N', 'TR', 'TY', 'ST', 'NI', 'TX', 'PR', 'SM'], 'required'],
            [['DAT_ID', 'N', 'TR', 'TY', 'NI'], 'integer'],
            [['ST', 'PR', 'SM'], 'number'],
            [['TX'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'DAT_ID' => 'Dat  ID',
            'N' => 'N',
            'TR' => 'Tr',
            'TY' => 'Ty',
            'ST' => 'St',
            'NI' => 'Ni',
            'TX' => 'Tx',
            'PR' => 'Pr',
            'SM' => 'Sm',
        ];
    }
}
