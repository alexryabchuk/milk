<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "log_mirror".
 *
 * @property integer $id
 * @property string $logdate
 * @property string $content
 */
class LogMirror extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_mirror';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logdate'], 'safe'],
            [['content'], 'required'],
            [['content'], 'string', 'max' => 4096],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logdate' => 'Logdate',
            'content' => 'Content',
        ];
    }
}
