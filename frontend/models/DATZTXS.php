<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT_ZTXS".
 *
 * @property integer $id
 * @property integer $DAT_ID
 * @property string $TX
 * @property string $TS
 * @property string $TXPR
 * @property integer $TXI
 * @property integer $TXO
 * @property string $DTNM
 * @property string $DTPR
 * @property integer $DTI
 * @property integer $DTO
 * @property integer $TXTY
 * @property integer $TXAL
 * @property integer $SMI
 * @property integer $SMO
 */
class DATZTXS extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT_ZTXS';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DAT_ID', 'TX', 'TS', 'TXPR', 'TXI', 'TXO', 'DTNM', 'DTPR', 'DTI', 'DTO', 'TXTY', 'TXAL', 'SMI', 'SMO'], 'required'],
            [['DAT_ID', 'TXI', 'TXO', 'DTI', 'DTO', 'TXTY', 'TXAL', 'SMI', 'SMO'], 'integer'],
            [['TXPR', 'DTPR'], 'number'],
            [['TX'], 'string', 'max' => 10],
            [['TS'], 'string', 'max' => 8],
            [['DTNM'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'DAT_ID' => 'Dat  ID',
            'TX' => 'Tx',
            'TS' => 'Ts',
            'TXPR' => 'Txpr',
            'TXI' => 'Txi',
            'TXO' => 'Txo',
            'DTNM' => 'Dtnm',
            'DTPR' => 'Dtpr',
            'DTI' => 'Dti',
            'DTO' => 'Dto',
            'TXTY' => 'Txty',
            'TXAL' => 'Txal',
            'SMI' => 'Smi',
            'SMO' => 'Smo',
        ];
    }
}
