<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property integer $parentname
 * @property string $usercode
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'firstname', 'lastname', 'parentname', 'usercode'], 'required'],
            [['id'], 'integer'],
            [['firstname', 'lastname'], 'string', 'max' => 64],
            [['usercode'], 'string', 'max' => 10],
            [['usercode'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'parentname' => 'Parentname',
            'usercode' => 'Usercode',
        ];
    }
}
