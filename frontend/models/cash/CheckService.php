<?php

namespace frontend\models\cash;


/**
 * This is the model class for table "check_service".
 *
 * @property int $id
 * @property int $mirror_id Ід пакета в зеркалі
 * @property string $cash_id Номер касового апарата
 * @property string $package_date Дата пакета
 * @property int $di DI пакета
 * @property int $m_type Тип оплати
 * @property string $m_name Назва типу оплати
 * @property string $i_sm Сумма внесення
 * @property string $o_sm Сумма видачі
 * @property int $e_vd Відміна
 */
class CheckService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mirror_id', 'di', 'm_type', 'e_vd'], 'integer'],
            [['package_date'], 'safe'],
            [['i_sm', 'o_sm'], 'number'],
            [['cash_id'], 'string', 'max' => 10],
            [['m_name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mirror_id' => 'Ід пакета в зеркалі',
            'cash_id' => 'Номер касового апарата',
            'package_date' => 'Дата пакета',
            'di' => 'DI пакета',
            'm_type' => 'Тип оплати',
            'm_name' => 'Назва типу оплати',
            'i_sm' => 'Сумма внесення',
            'o_sm' => 'Сумма видачі',
            'e_vd' => 'Відміна',
        ];
    }
}
