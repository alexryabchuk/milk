<?php

namespace frontend\models\cash;

use Yii;

/**
 * This is the model class for table "check0_p".
 *
 * @property int $id
 * @property int $check0_id
 * @property string $cash_id Номер касового апарата
 * @property string $package_date Дата пакета
 * @property int $n № в чеку
 * @property int $c Код продукта
 * @property string $nm Назва продукту
 * @property string $sm Сумма
 * @property string $q Кількість в грамах
 * @property string $prc Ціна
 * @property int $w Ваговий/Штучний
 */
class Check0P extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check0_p';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['check0_id', 'n', 'c', 'w'], 'integer'],
            [['package_date'], 'safe'],
            [['sm', 'q', 'prc'], 'number'],
            [['cash_id'], 'string', 'max' => 10],
            [['nm'], 'string', 'max' => 120],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'check0_id' => 'Check0 ID',
            'cash_id' => 'Номер касового апарата',
            'package_date' => 'Дата пакета',
            'n' => '№ в чеку',
            'c' => 'Код продукта',
            'nm' => 'Назва продукту',
            'sm' => 'Сумма ',
            'q' => 'Кількість в грамах',
            'prc' => 'Ціна',
            'w' => 'Ваговий/Штучний',
        ];
    }
}
