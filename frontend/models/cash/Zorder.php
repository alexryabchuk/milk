<?php

namespace frontend\models\cash;


/**
 * This is the model class for table "zorder".
 *
 * @property int $id
 * @property int $mirror_id Ід пакета в зеркалі
 * @property string $cash_id Номер касового апарата
 * @property string $package_date Дата пакета
 * @property int $di DI пакета
 * @property string $m_smi_0 Сумма внесення готівка
 * @property string $m_smo_0 Сумма видачі готівка
 * @property string $m_smi_1 Сумма внесення картка
 * @property string $m_smo_1 Сумма видачі картка
 * @property string $io_smi_0 Сумма внесення готівка
 * @property string $io_smo_0 Сумма видачі готівка
 * @property string $io_smi_1 Сумма внесення картка
 * @property string $io_smo_1 Сумма видачі картка
 * @property int $nc_ni Кількість чеків оплати
 * @property int $nc_no Кількість чеків повернення
 */
class Zorder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zorder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mirror_id', 'di', 'nc_ni', 'nc_no'], 'integer'],
            [['package_date'], 'safe'],
            [['m_smi_0', 'm_smo_0', 'm_smi_1', 'm_smo_1', 'io_smi_0', 'io_smo_0', 'io_smi_1', 'io_smo_1'], 'number'],
            [['cash_id'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mirror_id' => 'Ід пакета в зеркалі',
            'cash_id' => 'Номер касового апарата',
            'package_date' => 'Дата пакета',
            'di' => 'DI пакета',
            'm_smi_0' => 'Сумма внесення готівка',
            'm_smo_0' => 'Сумма видачі готівка',
            'm_smi_1' => 'Сумма внесення картка',
            'm_smo_1' => 'Сумма видачі картка',
            'io_smi_0' => 'Сумма внесення готівка',
            'io_smo_0' => 'Сумма видачі готівка',
            'io_smi_1' => 'Сумма внесення картка',
            'io_smo_1' => 'Сумма видачі картка',
            'nc_ni' => 'Кількість чеків оплати',
            'nc_no' => 'Кількість чеків повернення',
        ];
    }
}
