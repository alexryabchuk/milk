<?php

namespace frontend\models\cash;

use Yii;

/**
 * This is the model class for table "check0".
 *
 * @property int $id
 * @property int $mirror_id Ід пакета в зеркалі
 * @property string $cash_id Номер касового апарата
 * @property string $package_date Дата пакета
 * @property int $di DI пакета
 * @property int $m_type Тип оплати
 * @property string $m_name Назва типу оплати
 * @property int $e_no Номер чека
 * @property string $e_sm Сумма чека
 * @property string $p_sm Сумма по продукції
 * @property int $e_vd Відміна
 */
class Check0 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'check0';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mirror_id', 'di', 'm_type', 'e_no', 'e_vd'], 'integer'],
            [['package_date'], 'safe'],
            [['e_sm', 'p_sm'], 'number'],
            [['cash_id'], 'string', 'max' => 10],
            [['m_name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mirror_id' => 'Ід пакета в зеркалі',
            'cash_id' => 'Номер касового апарата',
            'package_date' => 'Дата пакета',
            'di' => 'DI пакета',
            'm_type' => 'Тип оплати',
            'm_name' => 'Назва типу оплати',
            'e_no' => 'Номер чека',
            'e_sm' => 'Сумма чека',
            'p_sm' => 'Сумма по продукції',
            'e_vd' => 'Відміна',
        ];
    }
}
