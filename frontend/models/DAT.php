<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT".
 *
 * @property integer $id
 * @property string $Cash_id
 * @property integer $package_type
 * @property string $package_date
 * @property integer $package_id
 * @property integer $package_no
 * @property integer $DI
 * @property integer $pay
 * @property integer $paytotal
 */
class DAT extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Cash_id', 'package_type', 'package_date', 'package_id', 'package_no', 'DI'], 'required'],
            [['package_type', 'package_id', 'package_no', 'DI', 'pay', 'paytotal'], 'integer'],
            [['package_date'], 'safe'],
            [['Cash_id'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Cash_id' => 'Cash ID',
            'package_type' => 'Package Type',
            'package_date' => 'Package Date',
            'package_id' => 'Package ID',
            'package_no' => 'Package No',
            'DI' => 'Номер чека',
            'pay' => 'Сума чека',

        ];
    }
}
