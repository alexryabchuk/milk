<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "candel".
 *
 * @property integer $id
 * @property integer $del
 */
class Addcheck extends Model
{
//    public $package_date;
    public $DI;
    public $pay;
    public $checkdate;
    public $checktime;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DI', 'pay', 'checkdate', 'checktime'], 'required'],
//            [['checkdate'],'date'],
//            [['checktime'],'time']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'DI' => '№ чека',
            'checkdate' => 'Дата чека',
            'checktime' => 'Час чека',
            'pay' => 'Сума чека',
        ];
    }
}
