<?php

namespace frontend\models;

use yii\base\Model;

/**
 * Signup form
 */
class Cashlostdelete extends Model
{
    public $checkstart;
    public $checkend;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['checkstart', 'integer'],
            ['checkend', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'checkstart' => 'Початковий чек',
            'checkend' => 'Кінцевий чек',
        ];
    }

}
