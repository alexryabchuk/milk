<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT_ZIO".
 *
 * @property integer $id
 * @property integer $DAT_ID
 * @property integer $T
 * @property string $NM
 * @property integer $SMI
 * @property integer $SMO
 */
class DATZIO extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT_ZIO';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DAT_ID', 'T', 'NM', 'SMI', 'SMO'], 'required'],
            [['DAT_ID', 'T', 'SMI', 'SMO'], 'integer'],
            [['NM'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'DAT_ID' => 'Dat  ID',
            'T' => 'T',
            'NM' => 'Nm',
            'SMI' => 'Smi',
            'SMO' => 'Smo',
        ];
    }
}
