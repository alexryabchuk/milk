<?php

namespace frontend\models;

use yii\base\Model;
use common\models\CashList;

/**
 * Signup form
 */
class Cashproductclient extends Model
{
    public $cashdatestart;
    public $cashdateend;
    public $clientid;
    public $clientList;


    /**
     * @inheritdoc
     */

    public function __construct($config = [])
    {
        $this->cashdatestart = date("Y-m-d");
        $this->cashdateend = date("Y-m-d");
        $this->clientList = CashList::getCashList();
        $this->clientid = key($this->clientList);
        parent::__construct($config);
    }


    public function rules()
    {
        return [
            ['cashdatestart', 'date'],
            ['cashdateend', 'date'],
            ['clientid', 'integer'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'cashdatestart' => 'Початкова дата',
            'cashdateend' => 'Кінцева дата',
            'clientid' => 'Контрагент',

        ];
    }

}
