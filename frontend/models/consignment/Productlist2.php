<?php

namespace frontend\models\consignment;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "productlist2".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category
 * @property integer $barcode
 * @property integer $deleted
 */
class Productlist2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productlist2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category', 'barcode'], 'required'],
            [['category', 'barcode', 'deleted'], 'integer'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'category' => 'Категорія',
            'barcode' => 'Штрихкод',
            'deleted' => 'Знищений',
        ];
    }

    public static function getProduct()
    {
        $productList = Productlist2::find()->asarray()->all();
        $productList = ArrayHelper::map($productList, 'id', 'name');
        return $productList;
    }
}
