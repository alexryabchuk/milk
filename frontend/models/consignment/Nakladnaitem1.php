<?php

namespace frontend\models\consignment;

use Yii;

/**
 * This is the model class for table "nakladnaitem1".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $col
 * @property string $dozr
 * @property string $realiz
 * @property string $prichina
 * @property integer $napravleno1
 * @property string $col1
 * @property integer $napravleno2
 * @property string $col2
 * @property integer $closed
 * @property integer $nakladnalist1_id
 * @property string $nnumn
 * @property string $ndaten
 * @property string $cena
 */
class Nakladnaitem1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nakladnaitem1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'col', 'dozr', 'realiz', 'prichina', 'napravleno1', 'col1', 'napravleno2', 'col2', 'nakladnalist1_id', 'nnumn', 'ndaten'], 'required'],
            [['product_id', 'napravleno1', 'napravleno2', 'closed', 'nakladnalist1_id'], 'integer'],
            [['col', 'col1', 'col2', 'cena'], 'number'],
            [['dozr', 'realiz', 'ndaten'], 'safe'],
            [['prichina'], 'string', 'max' => 128],
            [['nnumn'], 'string', 'max' => 5],
            [['col'], 'default', 'value' => 0],
            ['col1', 'default', 'value' => 0],
            ['col2', 'default', 'value' => 0],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Продукт',
            'col' => 'К-сть',
            'dozr' => 'Дата дозр',
            'realiz' => 'Дата реаліз',
            'prichina' => 'Причина',
            'napravleno1' => 'Цех',
            'col1' => 'К-сть',
            'napravleno2' => 'Цех',
            'col2' => 'К-сть',
            'closed' => 'Closed',
            'nakladnalist1_id' => 'Nakladnalist1 ID',
            'nnumn' => '№',
            'ndaten' => 'Дата',
            'cena' => 'Cena',
        ];
    }
}
