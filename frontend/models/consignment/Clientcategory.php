<?php

namespace frontend\models\consignment;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clientcategory".
 *
 * @property integer $id
 * @property string $name
 */
class Clientcategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clientcategory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public static function getClentCategory()
    {
        $clientList = Clientcategory::find()->asarray()->all();
        $clientList = ArrayHelper::map($clientList, 'id', 'name');
        return $clientList;
    }
}
