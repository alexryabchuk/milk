<?php

namespace frontend\models\consignment;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ReportForm extends Model
{
    public $startdate;
    public $enddate;
    public $vidbir;
    public $client;
    public $category;
    public $product;
    public $categoryList = [];
    public $all = 1;

    public function attributeLabels()
    {
        return [
            'startdate' => 'З',
            'enddate' => 'По',
            'vidbir' => 'Відбір',
            'category' => 'Категорія',
            'categoryList' => 'Категорії',
            'client' => 'Контрагент',
            'all' => '',
        ];
    }
}
