<?php

namespace frontend\models\consignment;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DovNaspunct;

/**
 * DovNaspunctsearch represents the model behind the search form of `\common\models\DovNaspunct`.
 */
class DovNaspunctsearch extends DovNaspunct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'oblast_code', 'region_code'], 'integer'],
            [['name_naspunct', 'postal_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DovNaspunct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'code' => $this->code,
            'oblast_code' => $this->oblast_code,
            'region_code' => $this->region_code,
        ]);

        $query->andFilterWhere(['like', 'name_naspunct', $this->name_naspunct])
            ->andFilterWhere(['like', 'postal_code', $this->postal_code]);

        return $dataProvider;
    }
}
