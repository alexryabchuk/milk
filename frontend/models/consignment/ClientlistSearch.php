<?php

namespace frontend\models\consignment;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\consignment\Clientlist;

/**
 * ClientlistSearch represents the model behind the search form about `frontend\models\consignment\Clientlist`.
 */
class ClientlistSearch extends Clientlist
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'firm', 'deleted', 'naspunct', 'region', 'oblast'], 'integer'],
            [['name', 'address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clientlist::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'firm' => $this->firm,
            'deleted' => $this->deleted,
            'naspunct' => $this->naspunct,
            'region' => $this->region,
            'oblast' => $this->oblast,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
