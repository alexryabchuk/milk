<?php

namespace frontend\models\consignment;

use Yii;

/**
 * This is the model class for table "nakladnaitem2".
 *
 * @property integer $id
 * @property integer $nakladnalist2_id
 * @property integer $product_id
 * @property string $col
 */
class Nakladnaitem2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $ost;


    public static function tableName()
    {
        return 'nakladnaitem2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nakladnalist2_id', 'product_id', 'col'], 'required'],
            [['nakladnalist2_id', 'product_id'], 'integer'],
            [['col'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nakladnalist2_id' => 'Nakladnalist2 ID',
            'product_id' => 'Product ID',
            'col' => 'Col',
        ];
    }
}
