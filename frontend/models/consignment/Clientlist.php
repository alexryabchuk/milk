<?php

namespace frontend\models\consignment;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clientlist".
 *
 * @property integer $id
 * @property string $name
 * @property integer $firm
 * @property integer $deleted
 * @property integer $oblast
 * @property integer $region
 * @property integer $naspunct
 * @property integer $address
 */
class Clientlist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clientlist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['firm', 'deleted', 'oblast', 'region', 'naspunct'], 'integer'],
            [['name', 'address'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Назва',
            'firm' => 'Фірмова',
            'deleted' => 'Знищений',
            'oblast' => 'Область',
            'region' => 'Район',
            'naspunct' => 'Населений пункт',
        ];
    }

    public static function getClientList()
    {
        $clientList = Clientlist::find()->asarray()->all();
        $clientList = ArrayHelper::map($clientList, 'id', 'name');
        return $clientList;
    }

    public static function getActiveClientList()
    {
        $clientList = Clientlist::find()->where(['deleted' => 0])->asarray()->all();
        $clientList = ArrayHelper::map($clientList, 'id', 'name');
        return $clientList;
    }

}
