<?php

namespace frontend\models\consignment;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categorylist".
 *
 * @property integer $id
 * @property string $name
 * @property integer $deleted
 */
class Categorylist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categorylist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['deleted'], 'integer'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'deleted' => 'Deleted',
        ];
    }

    public static function getCategoryList()
    {
        $categoryList = Categorylist::find()->asarray()->all();
        $categoryList = ArrayHelper::map($categoryList, 'id', 'name');
        return $categoryList;
    }
}
