<?php

namespace frontend\models\consignment;

use Yii;

/**
 * This is the model class for table "nakladnalist2".
 *
 * @property integer $id
 * @property string $nnum
 * @property string $ndate
 * @property integer $client_id
 * @property integer $closed
 * @property integer $deleted
 *
 * @property Clientlist $client
 */
class NakladnalistTest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nakladnalist2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nnum', 'ndate', 'client_id', 'closed', 'deleted'], 'required'],
            [['ndate'], 'safe'],
            [['client_id', 'closed', 'deleted'], 'integer'],
            [['nnum'], 'string', 'max' => 10],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientlist::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nnum' => 'Nnum',
            'ndate' => 'Ndate',
            'client_id' => 'Client ID',
            'closed' => 'Closed',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clientlist::className(), ['id' => 'client_id']);
    }
}
