<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT_I".
 *
 * @property integer $id
 * @property integer $DAT_ID
 * @property integer $N
 * @property integer $T
 * @property string $NM
 * @property string $SM
 */
class DATI extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT_I';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DAT_ID', 'N', 'T', 'NM', 'SM'], 'required'],
            [['DAT_ID', 'N', 'T'], 'integer'],
            [['SM'], 'number'],
            [['NM'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'DAT_ID' => 'Dat  ID',
            'N' => 'N',
            'T' => 'T',
            'NM' => 'Nm',
            'SM' => 'Sm',
        ];
    }
}
