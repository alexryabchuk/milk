<?php

namespace frontend\models\milk;

use Yii;

/**
 * This is the model class for table "statementitem".
 *
 * @property integer $id
 * @property integer $statementlist_id
 * @property string $fullname
 * @property string $icode
 * @property integer $natur
 * @property string $zhir
 * @property integer $basis
 */
class Statementitem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statementitem';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statementlist_id', 'fullname', 'icode', 'natur', 'zhir', 'basis'], 'required'],
            [['statementlist_id', 'natur', 'basis'], 'integer'],
            [['zhir'], 'number'],
            [['fullname'], 'string', 'max' => 100],
            [['icode'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'statementlist_id' => 'Statementlist ID',
            'fullname' => 'Прізвище, ім’я, по батькові',
            'icode' => 'Ід.код',
            'natur' => 'Натуральне',
            'zhir' => 'Жир',
            'basis' => 'Базисне',
        ];
    }
}
