<?php

namespace frontend\models\milk;

use Yii;

/**
 * This is the model class for table "statementlist".
 *
 * @property integer $id
 * @property string $usercode
 * @property string $period
 * @property string $naspunct
 * @property integer $basis1
 * @property integer $basis2
 * @property integer $natur1
 * @property integer $natur2
 * @property integer $status
 * @property string $nomerdok
 */
class Statementlist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statementlist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usercode', 'period', 'naspunct', 'nomerdok'], 'required'],
            [['basis1', 'basis2', 'natur1', 'natur2', 'status'], 'integer'],
            [['usercode'], 'string', 'max' => 32],
            [['period'], 'string', 'max' => 100],
            [['naspunct'], 'string', 'max' => 60],
            [['nomerdok'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usercode' => 'Usercode',
            'period' => 'Period',
            'naspunct' => 'Naspunct',
            'basis1' => 'Basis1',
            'basis2' => 'Basis2',
            'natur1' => 'Natur1',
            'natur2' => 'Natur2',
            'status' => 'Status',
            'nomerdok' => 'Nomerdok',
        ];
    }
}
