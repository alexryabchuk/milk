<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "DAT_ZNC".
 *
 * @property integer $id
 * @property integer $DAT_ID
 * @property integer $NI
 * @property integer $NO
 */
class DATZNC extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'DAT_ZNC';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DAT_ID', 'NI', 'NO'], 'required'],
            [['DAT_ID', 'NI', 'NO'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'DAT_ID' => 'Dat  ID',
            'NI' => 'Ni',
            'NO' => 'No',
        ];
    }
}
