<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "packages".
 *
 * @property integer $id
 * @property string $date
 * @property string $cash_number
 * @property string $request_id
 * @property string $pack_body
 * @property integer $package_id
 */
class Packages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'packages';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'cash_number', 'request_id', 'pack_body', 'package_id'], 'required'],
            [['date'], 'safe'],
            [['pack_body'], 'string'],
            [['package_id'], 'integer'],
            [['cash_number'], 'string', 'max' => 15],
            [['request_id'], 'string', 'max' => 1024],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'cash_number' => 'Cash Number',
            'request_id' => 'Request ID',
            'pack_body' => 'Pack Body',
            'package_id' => 'Package ID',
        ];
    }
}
