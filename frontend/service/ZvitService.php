<?php

namespace frontend\service;

use Yii;

/**
 * Site controller
 */
class ZvitService
{

    public static function cashzvtClientProduct($date1, $date2, $client)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $data = Yii::$app->db->createCommand('SELECT `C`, sum(SM) AS `NM1`, sum(Q) AS `Q1` FROM `check0_p` WHERE (`cash_id` = ' . $client . ') AND (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") group by C')->queryAll();
        return $data;
    }

    public static function cashzvtAllProduct($date1, $date2)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $data = Yii::$app->db->createCommand('SELECT `C`, sum(SM) AS `NM1`, sum(Q) AS `Q1` FROM `check0_p` WHERE `package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '" group by C')->queryAll();
        return $data;
    }

    public static function cashzvtZorder($date1, $date2)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $rows1 = Yii::$app->db->createCommand('SELECT `cash_id`, count(id) AS count_check, sum(e_sm) AS `total_e`, sum(p_sm) AS `total_p` FROM `check0` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") group by cash_id')->queryAll();
        $rows2 = Yii::$app->db->createCommand('SELECT `cash_id`,sum(nc_ni) as nc_ni, sum(m_smi_0)+sum(m_smi_1) as `total_z` FROM `zorder`WHERE package_date BETWEEN "' . $date1 . '" AND "' . $date2 . '" group by cash_id order by package_date, cash_id')->queryAll();
        $data = [];
        $summa['p'] = 0;
        $summa['e'] = 0;
        $summa['z'] = 0;
        $summa['c'] = 0;
        $summa['NI'] = 0;
        foreach ($rows1 as $row) {
            $data[$row['cash_id']]['e'] = $row['total_e'];
            $data[$row['cash_id']]['p'] = $row['total_p'];
            $data[$row['cash_id']]['c'] = $row['count_check'];
            $data[$row['cash_id']]['z'] = 0;
            $data[$row['cash_id']]['ni'] = 0;
            $summa['p'] += $row['total_p'];
            $summa['e'] += $row['total_e'];
            $summa['c'] += $row['count_check'];
        }
        foreach ($rows2 as $row) {
            if (!isset($data[$row['cash_id']]['e'])) {
                $data[$row['cash_id']]['e'] = 0;
            }
            if (!isset($data[$row['cash_id']]['p'])) {
                $data[$row['cash_id']]['p'] = 0;
            }
            if (!isset($data[$row['cash_id']]['c'])) {
                $data[$row['cash_id']]['c'] = 0;
            }
            $data[$row['cash_id']]['z'] = $row['total_z'];
            $data[$row['cash_id']]['ni'] = $row['nc_ni'];
            $summa['z'] += $row['total_z'];
            $summa['NI'] += $row['nc_ni'];
        }
        return ['data' => $data, 'summa' => $summa];
    }

    public static function checkzvtClient($date1, $date2, $client)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $rows1 = Yii::$app->db->createCommand('SELECT `package_date`, sum(e_sm) AS `total_e`, sum(p_sm) AS `total_p` FROM `check0` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") and (`cash_id`=' . $client . ') group by package_date')->queryAll();
        $data = [];
        $summa['p'] = 0;
        $summa['e'] = 0;
        foreach ($rows1 as $row) {
            $zorderdate = date('Y-m-d', strtotime($row['package_date']));
            if (!isset($data[$zorderdate]['e'])) {
                $data[$zorderdate]['e'] = $row['total_e'];
                $summa['e'] += $row['total_e'];
            } else {
                $data[$zorderdate]['e'] = $data[$zorderdate]['e'] + $row['total_e'];
                $summa['e'] += $row['total_e'];
            }
            if (!isset($data[$zorderdate]['p'])) {
                $summa['p'] += $row['total_p'];
                $data[$zorderdate]['p'] = $row['total_p'];
            } else {
                $data[$zorderdate]['p'] = $data[$zorderdate]['p'] + $row['total_p'];
                $summa['p'] += $row['total_p'];
            }
        }
        return ['data' => $data, 'summa' => $summa];
    }

    public static function zorderzvtClientAll($date1, $date2)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $rows = Yii::$app->db->createCommand('SELECT `cash_id`, sum(m_smi_0) AS `total_smi0`,sum(m_smi_1) AS `total_smi1`,sum(m_smo_0) AS `total_smo0`,sum(m_smo_1) AS `total_smo1` FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") group by cash_id')->queryAll();
        $data = [];
        $summa = ['smi0' => 0, 'smi1' => 0, 'smo0' => 0, 'smo1' => 0];
        foreach ($rows as $row) {
            $data[$row['cash_id']]['smi0'] = $row['total_smi0'];
            $data[$row['cash_id']]['smi1'] = $row['total_smi1'];
            $data[$row['cash_id']]['smo0'] = $row['total_smo0'];
            $data[$row['cash_id']]['smo1'] = $row['total_smo1'];
            $summa['smi0'] += $row['total_smi0'];
            $summa['smi1'] += $row['total_smi1'];
            $summa['smo0'] += $row['total_smo0'];
            $summa['smo1'] += $row['total_smo1'];

        }
        return ['data' => $data, 'summa' => $summa];
    }

    public static function zorderzvtClient($date1, $date2, $cash)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $rows = Yii::$app->db->createCommand('SELECT di, package_date, m_smi_0, m_smi_1, m_smo_0, m_smo_1 FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") and (`cash_id` = "' . $cash . '") order by package_date')->queryAll();
        $data = [];
        $summa = ['smi0' => 0, 'smi1' => 0, 'smo0' => 0, 'smo1' => 0];
        foreach ($rows as $row) {
            $data[$row['package_date']]['di'] = $row['di'];
            $data[$row['package_date']]['smi0'] = $row['m_smi_0'];
            $data[$row['package_date']]['smi1'] = $row['m_smi_1'];
            $data[$row['package_date']]['smo0'] = $row['m_smo_0'];
            $data[$row['package_date']]['smo1'] = $row['m_smo_1'];
            $summa['smi0'] += $row['m_smi_0'];
            $summa['smi1'] += $row['m_smi_1'];
            $summa['smo0'] += $row['m_smo_0'];
            $summa['smo1'] += $row['m_smo_1'];

        }
        return ['data' => $data, 'summa' => $summa];
    }

    public static function zorderzvtDayAll($date1, $date2)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $rows = Yii::$app->db->createCommand('SELECT package_date, m_smi_0, m_smi_1, m_smo_0, m_smo_1 FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '")')->queryAll();
        $data = [];
        $summa = ['smi0' => 0, 'smi1' => 0, 'smo0' => 0, 'smo1' => 0];
        foreach ($rows as $row) {
            $zorderdate = date('Y-m-d', strtotime($row['package_date']));
            isset($data[$zorderdate]['smi0']) ? $data[$zorderdate]['smi0'] += $row['m_smi_0'] : $data[$zorderdate]['smi0'] = $row['m_smi_0'];
            isset($data[$zorderdate]['smi1']) ? $data[$zorderdate]['smi1'] += $row['m_smi_1'] : $data[$zorderdate]['smi1'] = $row['m_smi_1'];
            isset($data[$zorderdate]['smo0']) ? $data[$zorderdate]['smo0'] += $row['m_smo_0'] : $data[$zorderdate]['smo0'] = $row['m_smo_0'];
            isset($data[$zorderdate]['smo1']) ? $data[$zorderdate]['smo1'] += $row['m_smo_1'] : $data[$zorderdate]['smo1'] = $row['m_smo_1'];
            $summa['smi0'] += $row['m_smi_0'];
            $summa['smi1'] += $row['m_smi_1'];
            $summa['smo0'] += $row['m_smo_0'];
            $summa['smo1'] += $row['m_smo_1'];

        }
        return ['data' => $data, 'summa' => $summa];
    }

    public static function getCheckByCash($date, $cash)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date));
        $date2 = date("Y-m-d 23:59:59", strtotime($date));
        $data = Yii::$app->db->createCommand('SELECT package_date, di, e_no, e_sm, p_sm, e_vd FROM `check0` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") and (cash_id=' . $cash . ') order by package_date')->queryAll();
        return $data;
    }

    public static function getCheckProductByCashDi($di, $cash)
    {
        $id = Yii::$app->db->createCommand('SELECT id FROM `check0` WHERE (`di` = ' . $di . ') and (cash_id=' . $cash . ')')->queryOne();
        $data = Yii::$app->db->createCommand('SELECT * FROM `check0_p` WHERE `check0_id` = ' . $id['id'] . ' order by n')->queryAll();
        return $data;
    }

    public static function getCheckServiceByDay($date)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date));
        $date2 = date("Y-m-d 23:59:59", strtotime($date));
        $rows = Yii::$app->db->createCommand('SELECT cash_id, i_sm, o_sm FROM `check_service` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") order by package_date ')->queryAll();
        $rows1 = Yii::$app->db->createCommand('SELECT cash_id, io_smi_0, io_smo_0 FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") order by package_date ')->queryAll();
        $data = [];
        foreach ($rows as $row) {
            $id = $row['cash_id'];
            if (!isset($data[$id])) {
                $data[$id]['i_sm'] = 0;
                $data[$id]['o_sm'] = 0;
                $data[$id]['io_smi'] = 0;
                $data[$id]['io_smo'] = 0;
            }
            isset($data[$id]['i_sm']) ? $data[$id]['i_sm'] += $row['i_sm'] : $data[$id]['i_sm'] = $row['i_sm'];
            isset($data[$id]['o_sm']) ? $data[$id]['o_sm'] += $row['o_sm'] : $data[$id]['o_sm'] = $row['o_sm'];
        }
        foreach ($rows1 as $row) {
            $id = $row['cash_id'];
            isset($data[$id]['io_smi']) ? $data[$id]['io_smi'] += $row['io_smi_0'] : $data[$id]['io_smi'] = $row['io_smi_0'];
            isset($data[$id]['io_smo']) ? $data[$id]['io_smo'] += $row['io_smo_0'] : $data[$id]['io_smo'] = $row['io_smo_0'];
        }
        return $data;
    }

    public static function checkservicezvtDayAll($date1, $date2)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $rows = Yii::$app->db->createCommand('SELECT package_date, i_sm, o_sm FROM `check_service` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") order by package_date ')->queryAll();
        $rows1 = Yii::$app->db->createCommand('SELECT package_date, io_smi_0, io_smo_0 FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") order by package_date ')->queryAll();
        $data = [];
        $summa = ['i_sm' => 0, 'o_sm' => 0, 'io_smi' => 0, 'io_smo' => 0];
        foreach ($rows as $row) {
            $zorderdate = date('Y-m-d', strtotime($row['package_date']));
            isset($data[$zorderdate]['i_sm']) ? $data[$zorderdate]['i_sm'] += $row['i_sm'] : $data[$zorderdate]['i_sm'] = $row['i_sm'];
            isset($data[$zorderdate]['o_sm']) ? $data[$zorderdate]['o_sm'] += $row['o_sm'] : $data[$zorderdate]['o_sm'] = $row['o_sm'];
            $summa['i_sm'] += $row['i_sm'];
            $summa['o_sm'] += $row['o_sm'];

        }
        foreach ($rows1 as $row) {
            $zorderdate = date('Y-m-d', strtotime($row['package_date']));
            isset($data[$zorderdate]['io_smi']) ? $data[$zorderdate]['io_smi'] += $row['io_smi_0'] : $data[$zorderdate]['io_smi'] = $row['io_smi_0'];
            isset($data[$zorderdate]['io_smo']) ? $data[$zorderdate]['io_smo'] += $row['io_smo_0'] : $data[$zorderdate]['io_smo'] = $row['io_smo_0'];
            $summa['io_smi'] += $row['io_smi_0'];
            $summa['io_smo'] += $row['io_smo_0'];

        }

        return ['data' => $data, 'summa' => $summa];
    }

    public static function checkservicezvtClient($date1, $date2, $cash)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date2));
        $rows = Yii::$app->db->createCommand('SELECT package_date, i_sm, o_sm FROM `check_service` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") and(`cash_id` = ' . $cash . ') order by package_date ')->queryAll();
        $rows1 = Yii::$app->db->createCommand('SELECT package_date, io_smi_0, io_smo_0 FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") and(`cash_id` = ' . $cash . ') order by package_date ')->queryAll();
        $data = [];
        $summa = ['i_sm' => 0, 'o_sm' => 0, 'io_smi' => 0, 'io_smo' => 0];
        foreach ($rows as $row) {
            $zorderdate = date('Y-m-d', strtotime($row['package_date']));
            if (!isset($data[$zorderdate])) {
                $data[$zorderdate]['i_sm'] = 0;
                $data[$zorderdate]['o_sm'] = 0;
                $data[$zorderdate]['io_smi'] = 0;
                $data[$zorderdate]['io_smo'] = 0;
            }
            isset($data[$zorderdate]['i_sm']) ? $data[$zorderdate]['i_sm'] += $row['i_sm'] : $data[$zorderdate]['i_sm'] = $row['i_sm'];
            isset($data[$zorderdate]['o_sm']) ? $data[$zorderdate]['o_sm'] += $row['o_sm'] : $data[$zorderdate]['o_sm'] = $row['o_sm'];
            $summa['i_sm'] += $row['i_sm'];
            $summa['o_sm'] += $row['o_sm'];

        }
        foreach ($rows1 as $row) {
            $zorderdate = date('Y-m-d', strtotime($row['package_date']));
            isset($data[$zorderdate]['io_smi']) ? $data[$zorderdate]['io_smi'] += $row['io_smi_0'] : $data[$zorderdate]['io_smi'] = $row['io_smi_0'];
            isset($data[$zorderdate]['io_smo']) ? $data[$zorderdate]['io_smo'] += $row['io_smo_0'] : $data[$zorderdate]['io_smo'] = $row['io_smo_0'];
            $summa['io_smi'] += $row['io_smi_0'];
            $summa['io_smo'] += $row['io_smo_0'];

        }

        return ['data' => $data, 'summa' => $summa];
    }

    public static function checkservicezvtClientDay($date1, $cash)
    {
        $date1 = date("Y-m-d 0:0:0", strtotime($date1));
        $date2 = date("Y-m-d 23:59:59", strtotime($date1));
        $rows = Yii::$app->db->createCommand('SELECT package_date, i_sm, o_sm FROM `check_service` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") and(`cash_id` = ' . $cash . ') order by package_date ')->queryAll();
        $rows1 = Yii::$app->db->createCommand('SELECT package_date, io_smi_0, io_smo_0 FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") and(`cash_id` = ' . $cash . ') order by package_date ')->queryAll();
        $data = [];
        $summa = ['i_sm' => 0, 'o_sm' => 0, 'io_smi' => 0, 'io_smo' => 0];
        foreach ($rows as $row) {
            $zorderdate = $row['package_date'];
            $data[$zorderdate]['io_smi'] = 0;
            $data[$zorderdate]['io_smo'] = 0;
            $data[$zorderdate]['name'] = "службовий чек";
            $data[$zorderdate]['i_sm'] = $row['i_sm'];
            $data[$zorderdate]['o_sm'] = $row['o_sm'];
            $summa['i_sm'] += $row['i_sm'];
            $summa['o_sm'] += $row['o_sm'];

        }
        foreach ($rows1 as $row) {
            $zorderdate = $row['package_date'];
            $data[$zorderdate]['i_sm'] = 0;
            $data[$zorderdate]['o_sm'] = 0;
            $data[$zorderdate]['name'] = "Z-звіт";
            $data[$zorderdate]['io_smi'] = $row['io_smi_0'];
            $data[$zorderdate]['io_smo'] = $row['io_smo_0'];
            $summa['io_smi'] += $row['io_smi_0'];
            $summa['io_smo'] += $row['io_smo_0'];
        }

        return ['data' => $data, 'summa' => $summa];
    }
}
