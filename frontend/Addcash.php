<?php
namespace frontend;

use Yii;
use frontend\models\DAT;
use frontend\models\LASTCASHDATA;
use frontend\models\LOSTPACKAGE;
use frontend\models\DATP;
use frontend\models\DATCP;
use frontend\models\DATD;
use frontend\models\DATS;
use frontend\models\DATI;
use frontend\models\DATO;
use frontend\models\DATM;
use frontend\models\DATVD;
use frontend\models\DATE1;
use frontend\models\DATZNC;
use frontend\models\DATZM;
use frontend\models\DATZIO;
use frontend\models\DATZTXS;
/**
 * Site controller
 */
class Addcash 
{
    public $_cash=array();
    private $_xml;
    private function _getP($XML) {
        $_p=array();
        $_pitem=  array();
        foreach ($XML as $X) {
            $_pitem['N']= (int) $X['N'];
            $_pitem['C']= (int) $X['C'];
            $_pitem['SM']= (int) $X['SM'];
            $_pitem['PRC']= (int) $X['PRC'];
            $_pitem['Q']= (int) $X['Q'];
            $_pitem['CD']= (string) $X['CD'];
            $_pitem['NM']= (string) $X['NM'];
            $_pitem['TX']= (string) $X['TX'];
            $_pitem['W']= (string) $X['W'];
            $_p[]=$_pitem;
        }
        return $_p;
    }
    
    private function _getCP($XML) {
        $_cp=array();
        $_cpitem=  array();
        foreach ($XML as $X) {
            $_cpitem['N']= (int) $X['N'];
            $_cpitem['C']= (int) $X['С'];
            $_cpitem['SM']= (int) $X['SM'];
            $_cpitem['PRC']= (int) $X['PRC'];
            $_cpitem['CD']= (string) $X['CD'];
            $_cpitem['NM']= (string) $X['NM'];
            $_cpitem['TX']= (string) $X['TX'];
            $_cp[]=$_cpitem;
        }
        return $_cp;
    }
    
    private function _getD($XML) {
        $_d=array();
        $_ditem=  array();
        foreach ($XML as $X) {
            $_ditem['N']= (int) $X['N'];
            $_ditem['TR']= (int) $X['TR'];
            $_ditem['TY']= (float) $X['TY'];
            $_ditem['ST']= (float) $X['ST'];
            $_ditem['NI']= (float) $X['NI'];
            $_ditem['PR']= (float) $X['PR'];
            $_ditem['SM']= (float) $X['SM'];
            $_ditem['TX']= (string) $X['TX'];
            $_d[]=$_ditem;
        }
        return $_d;
    }
    
    private function _getS($XML) {
        $_s=array();
        $_sitem=  array();
        foreach ($XML as $X) {
            $_sitem['N']= (int) $X['N'];
            $_sitem['TR']= (int) $X['TR'];
            $_sitem['TY']= (float) $X['TY'];
            $_sitem['ST']= (float) $X['ST'];
            $_sitem['NI']= (float) $X['NI'];
            $_sitem['PR']= (float) $X['PR'];
            $_sitem['SM']= (float) $X['SM'];
            $_sitem['TX']= (string) $X['TX'];
            $_s[]=$_sitem;
        }
        return $_s;
    }
    
    private function _getI($XML) {
        $_i=array();
        $_iitem=  array();
        foreach ($XML as $X) {
            $_iitem['N']= (int) $X['N'];
            $_iitem['T']= (int) $X['T'];
            $_iitem['SM']= (float) $X['SM'];
            $_iitem['NM']= (string) $X['NM'];
            $_i[]=$_iitem;
        }
        return $_i;
    }
    
    private function _getO($XML) {
        $_o=array();
        $_oitem=  array();
        foreach ($XML as $X) {
            $_oitem['N']= (int) $X['N'];
            $_oitem['T']= (int) $X['T'];
            $_oitem['SM']= (float) $X['SM'];
            $_oitem['NM']= (string) $X['NM'];
            $_o[]=$_oitem;
        }
        return $_o;
    }
    
    private function _getM($XML) {
        $_m=array();
        $_mitem=  array();
        foreach ($XML as $X) {
            $_mitem['N']= (int) $X['N'];
            $_mitem['T']= (int) $X['T'];
            $_mitem['SM']= (int) $X['SM'];
            $_mitem['RM']= (float) $X['RM'];
            $_mitem['NM']= (string) $X['NM'];
            $_m[]=$_mitem;
        }
        return $_m;
    }
    
    private function _getVD($XML) {
        $_vd=array();
        $_vditem=  array();
        foreach ($XML as $X) {
            $XN=(int) $X['N'];
            if (isset($X->NI)) {
                $XML_NI=$X->NI;
                $_vditem1=  array();
                foreach ($XML_NI as $XNI) {
                    $_vditem1['N']= 0;
                    $_vditem1['NI']= (int) $XNI['NI'];
                    $_vd[]=$_vditem1;
                }
            } else {
                $_vditem['N']= $XN;
                $_vditem['NI']= (int)$X['NI'];
                $_vd[]=$_vditem;
            }
            
        }
        return $_vd;
    }
    
    private function _getE($XML) {
        $_e=array();
        $_eitem=  array();
        foreach ($XML as $X) {
            $_eitem['N']= (int) $X['N'];
            $_eitem['NO']= (int) $X['NO'];
            $_eitem['CS']= (int) $X['CS'];
            $_eitem['VD']= (int) $X['VD'];
            $_eitem['SM']= (float) $X['SM'];
            $_eitem['SE']= (float) $X['SE'];
            $_eitem['TXPR']= (float) $X['TXPR'];
            $_eitem['TXSM']= (float) $X['TXSM'];
            $_eitem['DTPR']= (float) $X['DTPR'];
            $_eitem['DTSM']= (float) $X['DTSM'];
            $_eitem['TXTY']= (float) $X['TXTY'];
            $_eitem['TXAL']= (float) $X['TXAL'];
            $_eitem['TX']= (string) $X['TX'];
            $_e[]=$_eitem;
        }
        return $_e;
    }
    
    private function _getZNC($XML) {
        $_znc=array();
        $_zncitem=  array();
        foreach ($XML as $X) {
            $_zncitem['NI']= (int) $X['NI'];
            $_zncitem['NO']= (int) $X['NO'];
            $_znc[]=$_zncitem;
        }
        return $_znc;
    }
    
    private function _getZIO($XML) {
        $_zio=array();
        $_zioitem=  array();
        foreach ($XML as $X) {
            $_zioitem['T']= (int) $X['N'];
            $_zioitem['SMI']= (int) $X['SMI'];
            $_zioitem['SMO']= (int) $X['SMO'];
            $_zioitem['NM']= (string) $X['NM'];
            $_zio[]=$_zioitem;
        }
        return $_zio;
    }
    
    private function _getZM($XML) {
        $_zm=array();
        $_zmitem=  array();
        foreach ($XML as $X) {
            $_zmitem['T']= (int) $X['T'];
            $_zmitem['SMI']= (int) $X['SMI'];
            $_zmitem['SMO']= (int) $X['SMO'];
            $_zmitem['NM']= (string) $X['NM'];
            $_zm[]=$_zmitem;
        }
        return $_zm;
    }
    
    private function _getZTXS($XML) {
        $_ztxs=array();
        $_ztxsitem=  array();
        foreach ($XML as $X) {
            $_ztxsitem['TXPR']= (float) $X['TXPR'];
            $_ztxsitem['DTPR']= (float) $X['DTPR'];
            $_ztxsitem['TXI']= (int) $X['TXI'];
            $_ztxsitem['TXO']= (int) $X['TXO'];
            $_ztxsitem['DTI']= (int) $X['DTI'];
            $_ztxsitem['DTO']= (int) $X['DTO'];
            $_ztxsitem['TXTY']= (int) $X['TXTY'];
            $_ztxsitem['TXAL']= (int) $X['TXAL'];
            $_ztxsitem['SMI']= (int) $X['SMI'];
            $_ztxsitem['SMO']= (int) $X['SMO'];
            $_ztxsitem['TX']= (string) $X['TX'];
            $_ztxsitem['TS']= (string) $X['TS'];
            $_ztxsitem['DTPR']= (string) $X['DTPR'];
            $_ztxs[]=$_ztxsitem;
        }
        return $_ztxs;
    }
    
     public static function getCash($PackBody) {
        $cash = new Addcash;
        $xml = simplexml_load_string('<?xml version="1.0" standalone="yes"?>'.$PackBody);
        $cash->_cash['Cash_id']=(string) $xml['ZN'];
        $cash->_cash['Cash_id']=  substr($cash->_cash['Cash_id'], 2);
        
        $cash->_cash['DI'] =(string) $xml['DI'];
        $cash->_cash['package_date'] = \DateTime::createFromFormat("YmdHis", (string) $xml->TS)->format('Y-m-d H:i:s');
        $cash->_cash['package_no'] = 0;
        
        
        if (isset($xml->Z)) {
            if (isset($xml->Z['NO'])) {
                $cash->_cash['package_no']=(int) $xml->Z['NO'];
            }
            if (isset($xml->Z->NC)) {
                $cash->_cash['Z']['NC']=$cash->_getZNC($xml->Z->NC);
            }
            if (isset($xml->Z->IO)) {
                $cash->_cash['Z']['IO']=$cash->_getZIO($xml->Z->IO);
            }
            if (isset($xml->Z->M)) {
                $cash->_cash['Z']['M']=$cash->_getZM($xml->Z->M);
            }
            if (isset($xml->Z->TXS)) {
                $cash->_cash['Z']['TXS']=$cash->_getZTXS($xml->Z->TXS);
            }
            $cash->_cash['package_type']=3;
        }
        
        if (isset($xml->C)) {
            if (isset($xml->C['T'])) {
                $cash->_cash['package_type']=(int) $xml->C['T'];
            }
            if (isset($xml->C->P)) {
                $cash->_cash['C']['P']=$cash->_getP($xml->C->P);
            }
            if (isset($xml->C->CP)) {
                $cash->_cash['C']['CP']=$cash->_getCP($xml->C->CP);
            }
            if (isset($xml->C->D)) {
                $cash->_cash['C']['D']=$cash->_getD($xml->C->D);
            }
            if (isset($xml->C->S)) {
                $cash->_cash['C']['S']=$cash->_getS($xml->C->S);
            }
            if (isset($xml->C->I)) {
                $cash->_cash['C']['I']=$cash->_getI($xml->C->I);
            }
            if (isset($xml->C->O)) {
                $cash->_cash['C']['O']=$cash->_getO($xml->C->O);
            }
            if (isset($xml->C->M)) {
                $cash->_cash['C']['M']=$cash->_getM($xml->C->M);
            }
            if (isset($xml->C->VD)) {
                $cash->_cash['C']['VD']=$cash->_getVD($xml->C->VD);
            }
            if (isset($xml->C->E)) {
                $cash->_cash['C']['E']=$cash->_getE($xml->C->E);
            }
        }
        return $cash;
    }
    //Перевірити наявність пакету+
    public function isSetPackage(){
        $DAT = DAT::find()->where(['DI'=>$this->_cash['DI']])->andWhere(['Cash_id'=>$this->_cash['Cash_id']])->count();
        return (bool)$DAT>0;
    }
    //Отримати останній пакет +   
    public function getLastPackage(){
        $DAT= LASTCASHDATA::findOne(['cash_id' => $this->_cash['Cash_id']]);
        if ($DAT) {
            return (int) $DAT->last_check;
        } else {
            return 0;
        }
    }
    //Встановити останній пакет +    
    public function setLastPackage(){
        $DAT= LASTCASHDATA::findOne(['cash_id' => $this->_cash['Cash_id']]);
        if (!$DAT) {
            $DAT = new LASTCASHDATA();
        }
        $DAT->last_check= $this->_cash['DI'];
        $DAT->cash_id= $this->_cash['Cash_id'];
        $DAT->save();
    }
        
    public static function setLastPackage0($last_check){
        $DAT= LASTCASHDATA::findOne(['cash_id' => 0]);
        if (!$DAT) {
            $DAT = new LASTCASHDATA();
            
        }
        $DAT->last_check = (string)$last_check;
        
        if ($DAT->save()) {
            echo 'sdfsdf'.$last_check;
        } else {
            print_r($DAT->errors);
            var_dump($last_check);
        }
    }
    public static function getLastPackage0(){
        $DAT= LASTCASHDATA::findOne(['cash_id' => 0]);
        return (int) $DAT->last_check;
    }
        
    public function setLostPackage($num){
        $DAT = new LOSTPACKAGE();
        $DAT->cash_id=  $this->_cash['Cash_id'];
        $DAT->package_no= $num;
        $DAT->save();
    }
        
    public function delLostPackage(){
        $DAT = LOSTPACKAGE::findOne(['cash_id'=>$this->_cash['Cash_id'],'package_no' => $this->_cash['DI']]);
        if ($DAT) {
            $DAT->delete();
        }
    }
    
    private function _saveDATP($id,$param) {
        $DATP = new DATP();
        $DATP->DAT_ID = $id;
        $DATP->N = $param['N'];
        $DATP->C = $param['C'];
        $DATP->SM = $param['SM'];
        $DATP->Q = $param['Q'];
        $DATP->PRC = $param['PRC'];
        $DATP->CD = $param['CD'];
        $DATP->NM = $param['NM'];
        $DATP->TX = $param['TX'];
        $DATP->W = $param['W'];
        $DATP->save();
    }

    private function _saveDATCP($id,$param) {
        $DATCP = new DATCP();
        $DATCP->DAT_ID = $id;
        $DATCP->N = $param['N'];
        $DATCP->C = $param['C'];
        $DATCP->SM = $param['SM'];
        $DATCP->Q = $param['Q'];
        $DATCP->PRC = $param['PRC'];
        $DATCP->CD = $param['CD'];
        $DATCP->NM = $param['NM'];
        $DATCP->TX = $param['TX'];
        $DATCP->save();
    }
    
    private function _saveDATD($id,$param) {
        $DATD = new DATD();
        $DATD->DAT_ID = $id;
        $DATD->N = $param['N'];
        $DATD->TR = $param['TR'];
        $DATD->TY = $param['TY'];
        $DATD->ST = $param['ST'];
        $DATD->NI = $param['NI'];
        $DATD->PR = $param['PR'];
        $DATD->SM = $param['SM'];
        $DATD->TX = $param['TX'];
        $DATD->save();
    }
    
    private function _saveDATS($id,$param) {
        $DATS = new DATS();
        $DATS->DAT_ID = $id;
        $DATS->N = $param['N'];
        $DATS->TR = $param['TR'];
        $DATS->TY = $param['TY'];
        $DATS->ST = $param['ST'];
        $DATS->NI = $param['NI'];
        $DATS->PR = $param['PR'];
        $DATS->SM = $param['SM'];
        $DATS->TX = $param['TX'];
        $DATS->save();
    }
    
    private function _saveDATI($id,$param) {
        $DATI = new DATI();
        $DATI->DAT_ID = $id;
        $DATI->N = $param['N'];
        $DATI->T = $param['T'];
        $DATI->NM = $param['NM'];
        $DATI->SM = $param['SM'];
        $DATI->save();
    }
    
    private function _saveDATO($id,$param) {
        $DATO = new DATO();
        $DATO->DAT_ID = $id;
        $DATO->N = $param['N'];
        $DATO->T = $param['T'];
        $DATO->NM = $param['NM'];
        $DATO->SM = $param['SM'];
        $DATO->save();
    }
    
    private function _saveDATM($id,$param) {
        $DATM = new DATM();
        $DATM->DAT_ID = $id;
        $DATM->N = $param['N'];
        $DATM->T = $param['T'];
        $DATM->NM = $param['NM'];
        $DATM->SM = $param['SM'];
        $DATM->RM = $param['RM'];
        $DATM->save();
        $DAT = DAT::findOne(['id'=>$id]);
        if ($DAT) {
            $DAT->pay = $param['SM'];
            $DAT->save();
        }
    }
    
    private function _saveDATVD($id,$param) {
        $DATVD = new DATVD();
        $DATVD->DAT_ID = $id;
        $DATVD->N = $param['N'];
        $DATVD->NI = $param['NI'];
        $DATVD->save();
        $DATP = DATP::findOne(['DAT_ID'=>$id, 'N'=>$param['NI']]);
        if ($DATP) {
            $DATP->delete();
        }
    }
    
    private function _saveDATE($id,$param) {
        $DATE = new DATE1();
        $DATE->DAT_ID = $id;
        $DATE->N = $param['N'];
        $DATE->NO = $param['NO'];
        $DATE->CS = $param['CS'];
        $DATE->VD = $param['VD'];
        $DATE->SM = $param['SM'];
        $DATE->SE = $param['SE'];
        $DATE->TXPR = $param['TXPR'];
        $DATE->TXSM = $param['TXSM'];
        $DATE->DTPR = $param['DTPR'];
        $DATE->DTSM = $param['DTSM'];
        $DATE->TXTY = $param['TXTY'];
        $DATE->TXAL = $param['TXAL'];
        $DATE->TX = $param['TX'];
        $DATE->save();
        $DAT = DAT::findOne(['id'=>$id]);
        if ($DAT) {
            $DAT->paytotal = $param['SM'];
            $DAT->save();
        }
        
    }
    
    private function _saveDATZNC($id,$param) {
        $DATZNC = new DATZNC();
        $DATZNC->DAT_ID = $id;
        $DATZNC->NI = $param['NI'];
        $DATZNC->NO = $param['NO'];
        $DATZNC->save();
    }
    
    private function _saveDATZM($id,$param) {
        $DATZM = new DATZM();
        $DATZM->DAT_ID = $id;
        $DATZM->T = $param['T'];
        $DATZM->NM = $param['NM'];
        $DATZM->SMI = $param['SMI'];
        $DATZM->SMO = $param['SMO'];
        $DATZM->save();
    }
    
    private function _saveDATZIO($id,$param) {
        $DATZIO = new DATEZIO();
        $DATZIO->DAT_ID = $id;
        $DATZIO->T = $param['N'];
        $DATZIO->NM = $param['NM'];
        $DATZIO->SMI = $param['SMI'];
        $DATZIO->SMO = $param['SMO'];
        $DATZIO->save();
    }
    
    private function _saveDATZTXS($id,$param) {
        $DATZTXS = new DATZTXS();
        $DATZTXS->DAT_ID = $id;
        $DATZTXS->TX = $param['TX'];
        $DATZTXS->TS = $param['TS'];
        $DATZTXS->DTNM = $param['DTNM'];
        $DATZTXS->TXPR = $param['TXPR'];
        $DATZTXS->DTPR = $param['DTPR'];
        $DATZTXS->TXI = $param['TXI'];
        $DATZTXS->TXO = $param['TXO'];
        $DATZTXS->DTI = $param['DTI'];
        $DATZTXS->DTO = $param['DTO'];
        $DATZTXS->TXTY = $param['TXTY'];
        $DATZTXS->TXAL = $param['TXAL'];
        $DATZTXS->SMI = $param['SMI'];
        $DATZTXS->SMO = $param['SMO'];
        $DATZTXS->save();
    }
    
    public function saveCash($package_id) {
        $DAT = new DAT();
        $DAT->Cash_id = $this->_cash['Cash_id'];
        $DAT->package_id = $package_id;
        $DAT->package_type = $this->_cash['package_type'];
        $DAT->package_date = $this->_cash['package_date'];
        $DAT->package_no = $this->_cash['package_no'];
        $DAT->DI = $this->_cash['DI'];
        $DAT->save();
        $id = $DAT->primaryKey;
        if (isset($this->_cash['C']['P'])) {
            foreach ($this->_cash['C']['P'] as $item) {
                $this->_saveDATP($id, $item);
            }
        }
        if (isset($this->_cash['C']['CP'])) {
            foreach ($this->_cash['C']['CP'] as $item) {
                $this->_saveDATCP($id, $item);
            }
        }
        if (isset($this->_cash['C']['D'])) {
            foreach ($this->_cash['C']['D'] as $item) {
                $this->_saveDATD($id, $item);
            }
        }
        if (isset($this->_cash['C']['S'])) {
            foreach ($this->_cash['C']['S'] as $item) {
                $this->_saveDATS($id, $item);
            }
        }
        if (isset($this->_cash['C']['I'])) {
            foreach ($this->_cash['C']['I'] as $item) {
                $this->_saveDATI($id, $item);
            }
        }
        if (isset($this->_cash['C']['O'])) {
            foreach ($this->_cash['C']['O'] as $item) {
                $this->_saveDATO($id, $item);
            }
        }
        if (isset($this->_cash['C']['M'])) {
            foreach ($this->_cash['C']['M'] as $item) {
                $this->_saveDATM($id, $item);
            }
        }
        if (isset($this->_cash['C']['VD'])) {
            foreach ($this->_cash['C']['VD'] as $item) {
                $this->_saveDATVD($id, $item);
            }
        }
        if (isset($this->_cash['C']['E'])) {
            foreach ($this->_cash['C']['E'] as $item) {
                $this->_saveDATE($id, $item);
            }
        }
        if (isset($this->_cash['Z']['NC'])) {
            foreach ($this->_cash['Z']['NC'] as $item) {
                $this->_saveDATZNC($id, $item);
            }
        }
        
        if (isset($this->_cash['Z']['M'])) {
            foreach ($this->_cash['Z']['M'] as $item) {
                $this->_saveDATZM($id, $item);
            }
        }
        if (isset($this->_cash['Z']['IO'])) {
            foreach ($this->_cash['Z']['NC'] as $item) {
                $this->_saveDATZNC($id, $item);
            }
        }
        if (isset($this->_cash['Z']['TXS'])) {
            foreach ($this->_cash['Z']['NC'] as $item) {
                $this->_saveDATZNC($id, $item);
            }
        }
        
    }
    
}
