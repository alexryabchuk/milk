<?php

namespace frontend\controllers\label;

use Yii;
use common\models\Bsproductlist1;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Bscountry;
use app\models\forms\CountryForm;
use backend\models\Bsproductlist1Search;

/**
 * Site controller
 */
class LabelController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'label';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Bsproductlist1 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Bsproductlist1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,

        ]);
    }

    /**
     * Displays a single Bsproductlist1 model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bsproductlist1 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionPic1($id)
    {
        return $this->render('pic1', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionPic2($id)
    {
        return $this->render('pic2', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Bsproductlist1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Bsproductlist1 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $label = $this->findModel($id);
        $label->used = 0;
        $label->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bsproductlist1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bsproductlist1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bsproductlist1::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}