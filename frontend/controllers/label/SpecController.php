<?php

namespace frontend\controllers\label;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\label\Bsspeclist;
use frontend\models\label\Bsspecitem;

/**
 * Site controller
 */
class SpecController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'label';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['cash_main'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($date = null)
    {
        $searchModel = new \frontend\models\label\Bsspeclistsearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,

        ]);
    }

    public function actionView($id)
    {
        $id = (int)$id;
        $spec = Bsspeclist::find($id)->one();
        $specItem = Bsspecitem::getSpecZvit($id);
        return $this->render('view', [
            'spec' => $spec,
            'specItem' => $specItem,

        ]);
    }


}
