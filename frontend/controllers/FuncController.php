<?php

namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\base\Model;
use common\models\User;
use frontend\models\Profile;

/**
 * Site controller
 */
class FuncController extends Controller
{
    /**
     * @inheritdoc
     */
    public $auth;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

    }

    public function actionAddusermilk()
    {
        $users = [
            ['username' => 'Брітік', 'username2' => 'Надія', 'username3' => ' Петрівна', 'usercode' => '2659217546', 'password' => '203137456'],
            ['username' => 'Бурлака', 'username2' => 'Ніна', 'username3' => ' Михайлівна', 'usercode' => '2151815541', 'password' => '241249310'],
            ['username' => 'Гандула', 'username2' => 'Наталя', 'username3' => ' Василівна', 'usercode' => '2782715869', 'password' => '936773450'],
            ['username' => 'Головащенко', 'username2' => 'Людмила', 'username3' => ' Василівна', 'usercode' => '2368218363', 'password' => '621552865'],
            ['username' => 'Горецька', 'username2' => 'Надія', 'username3' => ' Миколаївна', 'usercode' => '2575321125', 'password' => '610334468'],
            ['username' => 'Заблоцький', 'username2' => 'Павло', 'username3' => ' Анатолійович', 'usercode' => '3168221956', 'password' => '490594172'],
            ['username' => 'Каплун', 'username2' => 'Микола', 'username3' => ' Петрович', 'usercode' => '2388319199', 'password' => '417480636'],
            ['username' => 'Коваль', 'username2' => 'Галина', 'username3' => ' Петрівна', 'usercode' => '2534204826', 'password' => '823394327'],
            ['username' => 'Когутюк', 'username2' => ' Юрій', 'username3' => ' Петрович', 'usercode' => '2239917194', 'password' => '873722872'],
            ['username' => 'Король', 'username2' => ' Тетяна', 'username3' => ' Володимирівна', 'usercode' => '2669421844', 'password' => '289431289'],
            ['username' => 'Куліш', 'username2' => ' Олена', 'username3' => ' Володимирівна', 'usercode' => '2603520642', 'password' => '343936365'],
            ['username' => 'Кушко', 'username2' => ' Світлана', 'username3' => ' Вікторівна', 'usercode' => '2840103540', 'password' => '707485757'],
            ['username' => 'Кушпета', 'username2' => ' Алла', 'username3' => ' Федорівна', 'usercode' => '2835919707', 'password' => '044263217'],
            ['username' => 'Левченко', 'username2' => ' Тетяна', 'username3' => ' Іванівна', 'usercode' => '3077113181', 'password' => '345380783'],
            ['username' => 'Лисяна', 'username2' => ' Лариса', 'username3' => ' Анатоліївна', 'usercode' => '3255316924', 'password' => '319880638'],
            ['username' => 'Лісова', 'username2' => ' Любов', 'username3' => ' Іванівна', 'usercode' => '2417913427', 'password' => '214018445'],
            ['username' => 'Мартинюк', 'username2' => ' Неля', 'username3' => ' Іванівна', 'usercode' => '2622304740', 'password' => '877511140'],
            ['username' => 'Марчук', 'username2' => ' Ігор', 'username3' => ' Іванович', 'usercode' => '2745507635', 'password' => '128550448'],
            ['username' => 'Матвійчук', 'username2' => ' Інна', 'username3' => ' Іванівна', 'usercode' => '2967507088', 'password' => '512333845'],
            ['username' => 'Мовчан', 'username2' => ' Олена', 'username3' => ' Михайлівна', 'usercode' => '2332414284', 'password' => '780277928'],
            ['username' => 'Педоренко', 'username2' => ' Валентина', 'username3' => ' Іванівна', 'usercode' => '2232217864', 'password' => '143155001 '],
            ['username' => 'Сакевич', 'username2' => ' Лариса', 'username3' => ' Леонідівна', 'usercode' => '2755610666', 'password' => '055917141'],
            ['username' => 'Сергієць', 'username2' => ' Світлана', 'username3' => ' Іванівна', 'usercode' => '2356612687', 'password' => '535104121'],
            ['username' => 'Ткаченко', 'username2' => ' Майя', 'username3' => ' Сергіївна', 'usercode' => '2510616806', 'password' => '924738154'],
            ['username' => 'Свірень', 'username2' => ' Олександр', 'username3' => ' Григорович', 'usercode' => '2552404052', 'password' => '665871362'],
            ['username' => 'Романюк', 'username2' => ' Ольга', 'username3' => ' Сергіївна', 'usercode' => '2528111183', 'password' => '187240850'],
            ['username' => 'Сергієць', 'username2' => ' Іван', 'username3' => ' Григорович', 'usercode' => '2283806013', 'password' => '263455839'],
            ['username' => 'Добжанський', 'username2' => ' Анатолій', 'username3' => ' Вячеславович', 'usercode' => '2283112894', 'password' => '745211898'],
            ['username' => 'Король', 'username2' => ' Олександр', 'username3' => ' Миколайович', 'usercode' => '2514904472', 'password' => '035244078'],
            ['username' => 'Васильчук', 'username2' => ' Василь', 'username3' => ' Іванович', 'usercode' => '2193112674', 'password' => '153270515'],
            ['username' => 'Паламарчук', 'username2' => ' Михайло', 'username3' => ' Леонтійович', 'usercode' => '2435009717', 'password' => '109083615'],
            ['username' => 'Ставнійчук', 'username2' => ' Віктор', 'username3' => ' Олександрович', 'usercode' => '2218214471', 'password' => '894698222'],
            ['username' => 'Сакевич', 'username2' => ' Сергій', 'username3' => ' Іванович', 'usercode' => '2589610476', 'password' => '620445863'],
            ['username' => 'Мовчан', 'username2' => ' Станіслав', 'username3' => ' Степанович', 'usercode' => '2306920197', 'password' => '815425334'],
            ['username' => 'Солоненко', 'username2' => ' Володимир', 'username3' => ' Тимофійович', 'usercode' => '2686504170', 'password' => '795274744'],
            ['username' => 'Пасуля', 'username2' => ' Петро', 'username3' => ' Іванович', 'usercode' => '2403517272', 'password' => '576839883'],
            ['username' => 'Романюк', 'username2' => ' Василь', 'username3' => ' Іванович', 'usercode' => '2387805759', 'password' => '469751393'],
            ['username' => 'Сергієць', 'username2' => ' Людмила', 'username3' => ' Анатоліївна', 'usercode' => '4576376471', 'password' => '827397825'],
        ];
        foreach ($users as $u) {
            $user = new User();
            $user->username = $u['usercode'];
            $user->email = $u['usercode'] . '@bilozgar.ua';
            $user->setPassword($u['password']);
            $user->generateAuthKey();
            if (!$user->save()) {
                print_r($user->errors);
            }
            $profile = new Profile();
            $profile->id = $user->id;
            $profile->firstname = $u['username'];
            $profile->lastname = trim($u['username2']);
            $profile->parentname = trim($u['username3']);
            $profile->usercode = $u['usercode'];
            $profile->save();
            if (!$profile->save()) {
                print_r($profile->errors);
            }
        }
    }


    public function addGrant($name, $description)
    {
        $permision = $this->auth->createPermission('p_' . $name);
        $permision->description = $description;
        $this->auth->add($permision);
        $role = $this->auth->createRole($name);
        $role->description = $description;
        $this->auth->add($role);
        $this->auth->addChild($role, $permision);
    }

    public function actionFillgrant()
    {
        $this->auth = Yii::$app->authManager;
//        $this->auth->removeAll();
//        
//    //Касові апарати
//        //Загальний доступ
//            $this->addGrant('cash_main','Касові апарати: Загальний доступ');
//        //Касові апарати        
//            $this->addGrant('cash_workcheck_lostpackage','Касові апарати: Список пропущених');
//            $this->addGrant('cash_workcheck_status','Касові апарати: Стан передачі');
//            $this->addGrant('cash_workcheck_editzorder','Касові апарати: Коригування z-звітів');
//        //Звіт по продукції
//            $this->addGrant('cash_zvitcash_cashall','Касові апарати: Звіт продукція загальний');
//            $this->addGrant('cash_zvitcash_cashclient','Касові апарати: Звіт продукція по контрагенту');	
//            $this->addGrant('cash_zvitcash_zorder','Касові апарати: Звіт продукція і z-звіти');
//        //Звіт по чекам
//            $this->addGrant('cash_zvitcheck_paycheck','Касові апарати: Звіт по чекам оплата по контрагентах');
//            $this->addGrant('cash_zvitcheck_payproduct','Касові апарати: Звіт по чекам продукція по контрагентах');
//        //Звіт по коштам
//            $this->addGrant('cash_zvitzorder_client','Касові апарати: Звіт по коштах по контрагенту'); 
//            $this->addGrant('cash_zvitzorder_clientall','Касові апарати: Звіт по коштах по контрагентах загальний');
//            $this->addGrant('cash_zvitzorder_dayall','Касові апарати: Звіт по коштах загальний по днях');
//        //Внесення/видача
//            $this->addGrant('cash_zvitlending_lending','Касові апарати: Службова видача коштів по днях');
//            $this->addGrant('cash_zvitlending_lendingclient','Касові апарати: Службова видача коштів по контрагентах');	
//            $this->addGrant('cash_zvitlending_making','Касові апарати: Службове внесення коштів по днях');	
//            $this->addGrant('cash_zvitlending_makingclient','Касові апарати: Службове внесення коштів по контрагентах');
//    //Приймання молока
//        //Загальний доступ
//            $this->addGrant('milk_main','Приймання молока: Загальний доступ');
//        //Відомості
//            $this->addGrant('milk_statement','Приймання молока: Відомості');
//        //Загальний доступ
//            $this->addGrant('milk_profile','Приймання молока: Профіль');
//    //Повернення продукції
//        //Загальний доступ
//            $this->addGrant('consignment_main','Повернення продукції: Загальний доступ');
//        //Довідники
//            $this->addGrant('consignment_dictionary_product','Повернення продукції: Довідник продукції');
//            $this->addGrant('consignment_dictionary_client','Повернення продукції: Довідник клієнтів');
//        //Накладні на повернення
//            $this->addGrant('consignment_invoice_main','Повернення продукції: Накладні на повернення - Список');
//            $this->addGrant('consignment_invoice_add','Повернення продукції: Накладні на повернення - Додавання');
//            $this->addGrant('consignment_invoice_edit','Повернення продукції: Накладні на повернення - Редагування');
//            $this->addGrant('consignment_invoice_editlab','Повернення продукції: Накладні на повернення - Редагування лабораторія');
//            $this->addGrant('consignment_invoice_delete','Повернення продукції: Накладні на повернення - Знищення');
//            $this->addGrant('consignment_invoice_activate','Повернення продукції: Накладні на повернення - Підтвердження лабораторія');
//            $this->addGrant('consignment_invoice_deactivate','Повернення продукції: Накладні на повернення - Зняття підтвердження лабораторія');
//            $this->addGrant('consignment_invoice_activate_guard','Повернення продукції: Накладні на повернення - Підтвердження охорона');
//            $this->addGrant('consignment_invoice_deactivate_guard','Повернення продукції: Накладні на повернення - Зняття підтвердження охорона');
//        //Накладні на повернення
//            $this->addGrant('consignment_consignment_main','Повернення продукції: Накладні на заміну - Список');
//            $this->addGrant('consignment_consignment_add','Повернення продукції: Накладні на заміну - Додавання');
//            $this->addGrant('consignment_consignment_edit','Повернення продукції: Накладні на заміну - Редагування');
//            $this->addGrant('consignment_consignment_delete','Повернення продукції: Накладні на заміну - Знищення');
//        //Накладні на повернення    
//            $this->addGrant('consignment_report_invoice','Повернення продукції: Звіт повернення');
//            $this->addGrant('consignment_report_consignment','Повернення продукції: Звіт заміна');
//            $this->addGrant('consignment_report_invoice_consignment','Повернення продукції: Звіт загальні');
    }
}
