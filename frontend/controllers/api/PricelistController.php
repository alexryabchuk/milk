<?php

namespace frontend\controllers\api;

use common\models\Pricelist;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Productlist;

/**
 * Site controller
 */
class PricelistController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $token = \yii::$app->request->get('token');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        if ($token == "cJkjVWZYgCkM8AmWPF3dhXtTqWgXiIWr") {
            if (!isset($_GET['product_id'])) return "Не указан id продукта";
            if (!isset($_GET['price_date'])) return "Не указана дата прайса";
            if (!isset($_GET['price'])) return "Не указана цена продукта";

            $price = new Pricelist();
            $price->product_id = (int)$_GET['product_id'];
            $price->price_date = $_GET['price_date'];
            $price->price = $_GET['price'];
            $price->save();
            return "Запись добавлена";
        } else {
            return "invalid token";
        }


    }


}
