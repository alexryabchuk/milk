<?php

namespace frontend\controllers\api;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\CashList;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class CashlistController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $token = \yii::$app->request->get('token');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        if ($token == "cJkjVWZYgCkM8AmWPF3dhXtTqWgXiIWr") {
            $cashList = CashList::find()->asarray()->all();
            return $cashList;
        } else {
            return "invalid token";
        }


    }


}
