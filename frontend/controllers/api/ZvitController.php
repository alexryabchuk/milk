<?php

namespace frontend\controllers\api;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Productlist;
use frontend\models\Cashproductall;
use frontend\models\Cashproductclient;
use frontend\models\CashList;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class ZvitController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionProduct()
    {
        $token = \yii::$app->request->get('token');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        if ($token == "cJkjVWZYgCkM8AmWPF3dhXtTqWgXiIWr") {

            $clientid = \yii::$app->request->get('clientid', '0');
            $date1 = date("Y-m-d 0:0:0", strtotime(\yii::$app->request->get('cashdatestart')));
            $date2 = date("Y-m-d 23:59:59", strtotime(\yii::$app->request->get('cashdateend')));
            $data = Yii::$app->db->createCommand('SELECT `C` AS `ProductCode`, sum(SM) AS `Summa`, sum(Q) AS `Quantity` FROM `check0_p` WHERE (`cash_id` = ' . $clientid . ') AND (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") group by C')->queryAll();
            return $data;
        } else {
            return "invalid token";
        }


    }

    public function actionCashstatus()
    {
        $token = \yii::$app->request->get('token');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        if ($token == "cJkjVWZYgCkM8AmWPF3dhXtTqWgXiIWr") {

            $date1 = date("Y-m-d 0:0:0", strtotime(\yii::$app->request->get('cashdate')));
            $date2 = date("Y-m-d 23:59:59", strtotime(\yii::$app->request->get('cashdate')));
            $rows1 = Yii::$app->db->createCommand('SELECT `cash_id`, count(id) AS count_check, sum(e_sm) AS `total_e`, sum(p_sm) AS `total_p` FROM `check0` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") group by cash_id')->queryAll();
            $rows2 = Yii::$app->db->createCommand('SELECT `cash_id`,sum(nc_ni) as nc_ni, sum(m_smi_0)+sum(m_smi_1) as `total_z` FROM `zorder`WHERE package_date BETWEEN "' . $date1 . '" AND "' . $date2 . '" group by cash_id order by package_date, cash_id')->queryAll();
            $data = [];
            $summa['p'] = 0;
            $summa['e'] = 0;
            $summa['z'] = 0;
            $summa['c'] = 0;
            $summa['NI'] = 0;
            foreach ($rows1 as $row) {
                $data[$row['cash_id']]['cash_id'] = $row['cash_id'];
                $data[$row['cash_id']]['e'] = $row['total_e'];
                $data[$row['cash_id']]['p'] = $row['total_p'];
                $data[$row['cash_id']]['c'] = $row['count_check'];
                $data[$row['cash_id']]['z'] = 0;
                $data[$row['cash_id']]['ni'] = 0;
                $summa['p'] += $row['total_p'];
                $summa['e'] += $row['total_e'];
                $summa['c'] += $row['count_check'];
                $data[$row['cash_id']]['status'] = 0;
                if ($data[$row['cash_id']]['e'] != $data[$row['cash_id']]['p']) {
                    $data[$row['cash_id']]['status'] = 1;
                }
            }
            foreach ($rows2 as $row) {
                if (!isset($data[$row['cash_id']]['e'])) {
                    $data[$row['cash_id']]['e'] = 0;
                }
                if (!isset($data[$row['cash_id']]['p'])) {
                    $data[$row['cash_id']]['p'] = 0;
                }
                if (!isset($data[$row['cash_id']]['c'])) {
                    $data[$row['cash_id']]['c'] = 0;
                }
                if (!isset($data[$row['cash_id']]['status'])) {
                    $data[$row['cash_id']]['status'] = 0;
                }
                if (!isset($data[$row['cash_id']]['cash_id'])) {
                    $data[$row['cash_id']]['cash_id'] = $row['cash_id'];
                }

                $data[$row['cash_id']]['z'] = $row['total_z'];
                $data[$row['cash_id']]['ni'] = $row['nc_ni'];
                $summa['z'] += $row['total_z'];
                $summa['NI'] += $row['nc_ni'];

            }
            foreach ($data as $i => $item) {
                if ($item['e'] !== $item['z']) {
                    $data[$i]['status'] = 2;
                }
                //echo "<p>".$item['e'].$item['z']."</p>";
            }
            return ['data' => $data, 'summa' => $summa];
        } else {
            return "invalid token";
        }


    }

    public function actionZorder()
    {
        $token = \yii::$app->request->get('token');
        \Yii::$app->response->format = \yii\web\Response::FORMAT_XML;
        if ($token == "cJkjVWZYgCkM8AmWPF3dhXtTqWgXiIWr") {

            $date1 = date("Y-m-d 0:0:0", strtotime(\yii::$app->request->get('cashdatestart')));
            $date2 = date("Y-m-d 23:59:59", strtotime(\yii::$app->request->get('cashdateend')));
            $clientid = \yii::$app->request->get('clientid', '0');
            $rows = Yii::$app->db->createCommand('SELECT di, package_date, m_smi_0, m_smi_1, m_smo_0, m_smo_1 FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") and (`cash_id` = "' . $clientid . '") order by package_date')->queryAll();
            $data = [];
            $summa = ['smi0' => 0, 'smi1' => 0, 'smo0' => 0, 'smo1' => 0];
            foreach ($rows as $row) {
                $data[$row['package_date']]['date'] = date('Y-m-d H:i:s', strtotime($row['package_date']));
                $data[$row['package_date']]['di'] = $row['di'];
                $data[$row['package_date']]['smi0'] = $row['m_smi_0'];
                $data[$row['package_date']]['smi1'] = $row['m_smi_1'];
                $data[$row['package_date']]['smo0'] = $row['m_smo_0'];
                $data[$row['package_date']]['smo1'] = $row['m_smo_1'];
                $summa['smi0'] += $row['m_smi_0'];
                $summa['smi1'] += $row['m_smi_1'];
                $summa['smo0'] += $row['m_smo_0'];
                $summa['smo1'] += $row['m_smo_1'];

            }
            return ['data' => $data, 'summa' => $summa];
        } else {
            return "invalid token";
        }


    }


}
