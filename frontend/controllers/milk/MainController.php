<?php

namespace frontend\controllers\milk;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\Profile;
use frontend\models\milk\Statementlist;
use frontend\models\milk\Statementitem;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use yii\base\Model;

/**
 * Site controller
 */
class MainController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'milkmain';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['print'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['send'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $profile = Profile::findOne(Yii::$app->user->id);
        if ($profile) {
            $usercode = $profile->usercode;
        } else {
            $usercode = '2569805154';
        }
        $query = Statementlist::find()->where(['usercode' => $usercode]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 20]);
        $statementList = $query->orderBy('id DESC')->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', [
            'statementList' => $statementList,
            'pages' => $pages,
        ]);
    }

    public function actionView($id)
    {
        $id = (int)$id;
        $statementItem = Statementitem::find()->where(['statementlist_id' => $id])->all();
        return $this->render('view', [
            'statementItem' => $statementItem,
        ]);
    }

    public function actionEdit($id)
    {
        $id = (int)$id;
        $statementItem = Statementitem::find()->where(['statementlist_id' => $id])->indexBy('id')->all();
        if (Model::loadMultiple($statementItem, Yii::$app->request->post()) && Model::validateMultiple($statementItem)) {
            $natur = 0;
            $basis = 0;
            foreach ($statementItem as $item) {
                $item->save(false);
                $natur = $natur + ((int)$item->natur);
                $basis = $basis + (((int)$item->natur) * ((float)$item->zhir) / 3.4);

            }
            $statement = Statementlist::find()->where(['id' => $id])->one();
            $statement->natur2 = $natur;
            $statement->basis2 = (int)$basis;
            $statement->save();
            return $this->redirect(['/milk/main']);
        }
        return $this->render('edit', [
            'statementItem' => $statementItem,
        ]);
    }

    public function actionPrint($id)
    {
        $this->layout = 'print';
        $id = (int)$id;
        $statement = Statementlist::find()->where(['id' => $id])->one();
        $statementItem = Statementitem::find()->where(['statementlist_id' => $id])->andWhere([">", 'natur', 0])->all();

        return $this->render('print', [
            'statement' => $statement,
            'statementItem' => $statementItem,
        ]);
    }

    public function actionSend($id)
    {
        $id = (int)$id;
        $statement = Statementlist::find()->where(['id' => $id])->one();
        if ($statement->status == 0) {
            $statement->status = 1;
            $statement->save();
        }
        return $this->redirect(['/milk/main']);
    }

}
