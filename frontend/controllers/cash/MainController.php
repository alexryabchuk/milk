<?php

namespace frontend\controllers\cash;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\service\ZvitService;
use common\models\Advanced;

/**
 * Site controller
 */
class MainController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'cashmain';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['cash_main'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($date = null)
    {
        if (!$date) {
            $date = date('Y-m-d');
        }
//        $data = Yii::$app->db->createCommand('SELECT B,C,D,E FROM h  GROUP BY B,C,D ORDER BY  B COLLATE  utf8_unicode_ci,C COLLATE  utf8_unicode_ci, D COLLATE  utf8_unicode_ci limit 5000 offset 25000')->queryAll();
//        $dov= \common\models\DovOblast::getOblastByName();
//                //var_dump($data);
//        foreach ($data as $d) {
//            $item =new \common\models\DovNaspunct();
//            $item->oblast_code= $dov[$d['B']];
//            $region = \common\models\DovRegion::getRegion($dov[$d['B']], $d['C']);
//            $item->region_code= $region->code;
//            $item->name_naspunct = $d['D'];
//            $item->postal_code=$d['E'];
//            $item->save();
//        }
        return $this->render('index', ['date' => $date]);
    }


}
