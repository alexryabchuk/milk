<?php

namespace frontend\controllers\cash;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\service\ZvitService;
use frontend\models\Cashproductall;
use frontend\models\Cashproductclient;
use yii\web\Response;

class ZvitlendingController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'cashmain';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['day'],
                        'allow' => true,
                        'roles' => ['cash_zvitlending_lending'],
                    ],
                    [
                        'actions' => ['client'],
                        'allow' => true,
                        'roles' => ['cash_zvitlending_lendingclient'],
                    ],
                    [
                        'actions' => ['making'],
                        'allow' => true,
                        'roles' => ['cash_zvitlending_making'],
                    ],
                    [
                        'actions' => ['makingclient'],
                        'allow' => true,
                        'roles' => ['cash_zvitlending_makingclient'],
                    ],
                    [
                        'actions' => ['logout', 'getdetailcheck', 'getdetailclient'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    public function actionDay()
    {
        $model = new Cashproductall;
        $data = ['data' => [], 'summa' => []];
        if ($model->load(Yii::$app->request->post())) {
            $data = ZvitService::checkservicezvtDayAll($model->cashdatestart, $model->cashdateend);
        }
        return $this->render('day', ['zvtdata' => $data['data'], 'summa' => $data['summa'], 'model' => $model]);
    }

    public function actionGetdetailcheck()
    {
        //if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $date = Yii::$app->request->post('date');
        //$date = "2018-01-03";
        $data = ZvitService::getCheckServiceByDay($date);
        $html = $this->renderPartial('_detaillending', ['data' => $data]);
        return ['html' => $html];// $this->render('searchidentical',['model'=>$model]);
        //var_dump($html);
        //}
    }

    public function actionClient()
    {
        $model = new Cashproductclient();
        $data = ['data' => [], 'summa' => []];
        if ($model->load(Yii::$app->request->post())) {
            $data = ZvitService::checkservicezvtClient($model->cashdatestart, $model->cashdateend, $model->clientid);
        }
        return $this->render('client', ['zvtdata' => $data['data'], 'summa' => $data['summa'], 'model' => $model]);
    }

    public function actionGetdetailclient()
    {
        //if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $date = Yii::$app->request->post('date');
        $cash = Yii::$app->request->post('cashid');
        //$date = "2018-01-04";
        //$cash = '4101217065';
        $data = ZvitService::checkservicezvtClientDay($date, $cash);
        //var_dump($data);
        $html = $this->renderPartial('_detaillendingclient', ['data' => $data['data']]);
        //return $html;
        return ['html' => $html];// $this->render('searchidentical',['model'=>$model]);
        //var_dump($html);
        //}
    }
}
