<?php

namespace frontend\controllers\cash;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Cashproductall;
use frontend\models\Cashproductclient;
use frontend\service\ZvitService;

/**
 * Site controller
 */
class ZvitzorderController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'cashmain';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['dayall'],
                        'allow' => true,
                        'roles' => ['cash_zvitzorder_dayall'],
                    ],
                    [
                        'actions' => ['clientall'],
                        'allow' => true,
                        'roles' => ['cash_zvitzorder_clientall'],
                    ],
                    [
                        'actions' => ['client'],
                        'allow' => true,
                        'roles' => ['cash_zvitzorder_client'],
                    ],
                    [
                        'actions' => ['editzorder', 'upcheck', 'convertcheck'],
                        'allow' => true,
                        'roles' => ['cash_workcheck_editzorder'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Z-звіти загальний по клієнтах
     *
     * @return mixed
     */
    public function actionClientall()
    {
        $model = new Cashproductall;
        $zvtdata = ['data' => [], 'summa' => []];
        if ($model->load(Yii::$app->request->post())) {
            $zvtdata = ZvitService::zorderzvtClientAll($model->cashdatestart, $model->cashdateend);
        }
        return $this->render('zorderclientall', ['zvtdata' => $zvtdata['data'], 'summa' => $zvtdata['summa'], 'model' => $model]);
    }

    /**
     * Z-звіти загальний по днях
     *
     * @return mixed
     */
    public function actionDayall()
    {
        $model = new Cashproductall;
        $zvtdata = ['data' => [], 'summa' => []];
        if ($model->load(Yii::$app->request->post())) {
            $zvtdata = ZvitService::zorderzvtDayAll($model->cashdatestart, $model->cashdateend);
        }
        return $this->render('zorderdayall', ['zvtdata' => $zvtdata['data'], 'summa' => $zvtdata['summa'], 'model' => $model]);
    }

    /**
     * Z-звіти по контрагенту
     *
     * @return mixed
     */
    public function actionClient()
    {
        $model = new Cashproductclient();
        $zvtdata = ['data' => [], 'summa' => []];
        if ($model->load(Yii::$app->request->post())) {
            $zvtdata = ZvitService::zorderzvtClient($model->cashdatestart, $model->cashdateend, $model->clientid);
        }
        return $this->render('zorderclient', ['zvtdata' => $zvtdata['data'], 'summa' => $zvtdata['summa'], 'model' => $model]);
    }


    public function actionUpcheck($di, $cash)
    {
        $zorder = \console\PackageService::searchPackage($di, $cash);
        $prevDi = ((int)$di) - 1;
        $prevPackage = \console\PackageService::searchPackage($prevDi, $cash);
        return $this->render('upcheck', ['zorder' => $zorder, 'prevPackage' => $prevPackage]);
    }

    public function actionConvertcheck($di, $cash)
    {
        $prevDi = ((int)$di) - 1;
        $prevPackage = \console\PackageService::searchPackage($prevDi, $cash);
        $zorder = \frontend\models\cash\Zorder::find()->where(['di' => $di])->andWhere(['cash_id' => $cash])->one();
        if ($zorder) {
            $zorder->package_date = $prevPackage['package_date'];
            $zorder->save();
        }
        return $this->redirect(['cash/zvitzorder/client']);

    }


}
