<?php

namespace frontend\controllers\cash;

use backend\models\Version;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\CashList;
use frontend\models\DAT;
use common\models\LastCashData;
use console\PackageService;

/**
 * Site controller
 */
class WorkcheckController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'cashmain';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['status', 'status2'],
                        'allow' => true,
                        'roles' => ['cash_workcheck_status'],
                    ],
                    [
                        'actions' => ['lostpackages', 'lostcheck', 'lostcashpackages'],
                        'allow' => true,
                        'roles' => ['cash_workcheck_lostpackage'],
                    ],
                    [
                        'actions' => ['dellostpackage'],
                        'allow' => true,
                        'roles' => ['cash_workcheck_dellostcheck'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionStatus()
    {
        $statusList = LastCashData::find()->orderBy('last_date')->indexBy('cash_id')->asArray()->all();
        $cashList = CashList::getCashList();
        unset($statusList['0']);
        return $this->render('status', ['statusList' => $statusList, 'cashList' => $cashList]);
    }

    public function actionStatus2()
    {
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>19.03.2018 ver:1.04.00 </b>Оптимізовано роботу парсера та перероблено структуру зберігання чеків</p>";
        $version->version = "1.04.00";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>23.03.2018 ver:1.05.00 </b>Внесено зміни до звітів</p>";
        $version->version = "1.05.00";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>25.03.2018 ver:1.05.07 </b>Додано перегляд списку чеків</p>";
        $version->version = "1.05.07";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>26.03.2018 ver:1.05.18 </b>Додано перегляд змісту чека</p>";
        $version->version = "1.05.18";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>28.03.2018 ver:1.05.31 </b>Розширено набір даних по Z-звітах</p>";
        $version->version = "1.05.31";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>28.03.2018 ver:1.05.36 </b>Розширено набір даних по службових чеках</p>";
        $version->version = "1.05.36";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>31.03.2018 ver:1.05.41 </b>Перенесено коригування Z-звітів до звіту по контрагенту</p>";
        $version->version = "1.05.41";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>02.04.2018 ver:1.05.43 </b>Внесено зміни до механізму коригування Z-звітів</p>";
        $version->version = "1.05.43";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>05.04.2018 ver:1.06.07 </b>Винесено парсер у консольний контролер</p>";
        $version->version = "1.06.07";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>10.04.2018 ver:1.06.25 </b>Додано календар відображення статусу роботи</p>";
        $version->version = "1.06.25";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>12.04.2018 ver:1.06.27 </b>Додано консольний контролер оновлення календаря </p>";
        $version->version = "1.06.27";
        $version->save();
        $version = new Version();
        $version->comments = "<p class=\'version\'><B>14.04.2018 ver:1.06.53 </b>Додано до адмін панелі контроль назв продукції</p>";
        $version->version = "1.06.53";
        $version->save();
        print_r($version->errors);


    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLostpackages()
    {
        $lostPackageList = Yii::$app->db->createCommand('SELECT cash_id, COUNT(cash_id) as lostcount FROM lost_packages GROUP BY cash_id ORDER BY cash_id')->queryAll();
        $cashList = CashList::getCashList();
        return $this->render('lostpackages', ['lostPackageList' => $lostPackageList, 'cashList' => $cashList]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionLostcashpackages($id)
    {
        $lostPackageList = Yii::$app->db->createCommand('SELECT di FROM lost_packages where cash_id = ' . $id . ' ORDER BY di')->queryAll();
        $cashList = CashList::getCashList();
        return $this->render('lostcashpackages', ['lostPackageList' => $lostPackageList, 'cashId' => $id, 'cashName' => $cashList[$id]]);
    }

    public function actionLostcheck($id1, $id2, $cash)
    {
        $prevCheckId = (int)$id1 - 1;
        $nextCheckId = (int)$id2 + 1;
        $prevCheck = PackageService::searchPackage($prevCheckId, $cash);
        $nextCheck = PackageService::searchPackage($nextCheckId, $cash);
        $cashList = CashList::getCashList();
        return $this->render('lostcheck', ['prevCheck' => $prevCheck, 'nextCheck' => $nextCheck, 'cashName' => $cashList[$cash], 'cashId' => $cash, 'id1' => $id1, 'id2' => $id2]);
    }

    public function actionAddcheck($id, $cash)
    {
        $nextCheckId = (int)$id + 1;
        $cashList = CashList::getCashList();
        $model = new \frontend\models\Addcheck();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            //сохраняем
            $DAT = new DAT();
            $DAT->package_date = $model->checkdate . " " . $model->checktime;
            $DAT->Cash_id = $cash;
            $DAT->package_type = 0;
            $DAT->package_id = 0;
            $DAT->package_no = 0;
            $DAT->pay = $model->pay;
            $DAT->paytotal = $model->pay;
            $DAT->DI = $model->DI;
            $DAT->save();
            print_r($DAT->errors);
        }
        return $this->render('addcheck', ['nextCheckId' => $nextCheckId, 'cashName' => $cashList[$cash], 'DI' => $id, 'cashId' => $cash, 'model' => $model]);
    }


    public function actionDellostpackage($id1, $id2, $cash)
    {
        $lostPackageList = Yii::$app->db->createCommand('DELETE FROM lost_packages where ((`cash_id` = ' . $cash . ') and (`di` between ' . $id1 . ' and ' . $id2 . ' ))')
            ->execute();
        $this->redirect(['cash/workcheck/lostcashpackages', 'id' => $cash]);

    }


}
