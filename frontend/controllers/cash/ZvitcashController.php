<?php

namespace frontend\controllers\cash;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\CashList;
use frontend\models\Cashproductall;
use frontend\models\Cashproductclient;
use frontend\service\ZvitService;
use common\models\LostPackages;
use yii\web\Response;

/**
 * Site controller
 */
class ZvitcashController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'cashmain';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['cashall', 'cashall2'],
                        'allow' => true,
                        'roles' => ['cash_zvitcash_cashall'],
                    ],
                    [
                        'actions' => ['cashclient', 'cashclient2'],
                        'allow' => true,
                        'roles' => ['cash_zvitcash_cashclient'],
                    ],
                    [
                        'actions' => ['zorder', 'zorder2'],
                        'allow' => true,
                        'roles' => ['cash_zvitcash_zorder'],
                    ],
                    [
                        'actions' => ['logout', 'payproduct', 'getdetailcheck'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    /**
     * Displays
     *
     * @return mixed
     */
    public function actionCashclient()
    {
        $model = new Cashproductclient();
        $data = [];
        if ($model->load(Yii::$app->request->post())) {
            $data = ZvitService::cashzvtClientProduct($model->cashdatestart, $model->cashdateend, $model->clientid);
        }

        return $this->render('cashclient', ['zvtdata' => $data, 'model' => $model]);
    }

    public function actionCashclient2()
    {
        $model = new Cashproductclient();
        $rows1 = array();
        if ($model->load(Yii::$app->request->post())) {
            $date1 = date("Y-m-d 0:0:0", strtotime($model->cashdatestart));
            $date2 = date("Y-m-d 23:59:59", strtotime($model->cashdateend));
            $rows1 = Yii::$app->db->createCommand('SELECT `C`, sum(SM) AS `NM1`, sum(Q) AS `Q1` FROM `DAT_P` WHERE `DAT_ID` IN (SELECT `id` FROM `DAT` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") AND (`Cash_id` = ' . $model->clientid . ') AND (`package_type`=0) AND (`pay`>0)) group by C')->queryAll();
        }
        return $this->render('cashclient', ['zvtdata' => $rows1, 'model' => $model]);
    }

    /**
     * Displays
     *
     * @return mixed
     */

    public function actionCashall()
    {
        $model = new Cashproductall;
        $data = array();
        if ($model->load(Yii::$app->request->post())) {
            $data = ZvitService::cashzvtAllProduct($model->cashdatestart, $model->cashdateend);
        }

        return $this->render('cashall', ['zvtdata' => $data, 'model' => $model]);
    }


    public function actionCashall2()
    {
        $model = new Cashproductall;
        $rows1 = array();
        if ($model->load(Yii::$app->request->post())) {
            $date1 = date("Y-m-d 0:0:0", strtotime($model->cashdatestart));
            $date2 = date("Y-m-d 23:59:59", strtotime($model->cashdateend));
            $rows1 = Yii::$app->db->createCommand('SELECT `C`, sum(SM) AS `NM1`, sum(Q) AS `Q1` FROM `DAT_P` WHERE `DAT_ID` IN (SELECT `id` FROM `DAT` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") AND (`package_type`=0) AND (`pay`>0)) group by C')->queryAll();
        }
        return $this->render('cashall', ['zvtdata' => $rows1, 'model' => $model]);
    }

    public function actionZorder()
    {
        $model = new Cashproductall;
        $data = ['data' => [], 'summa' => []];
        if ($model->load(Yii::$app->request->post())) {
            $data = ZvitService::cashzvtZorder($model->cashdatestart, $model->cashdatestart);
        }

        return $this->render('zorder', ['zvtdata' => $data['data'], 'summa' => $data['summa'], 'model' => $model]);
    }

    public function actionGetdetailcheck()
    {
        //if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $date = Yii::$app->request->post('date');
        $cash = Yii::$app->request->post('cash');
        //$date = "2017-10-10";
        $data = ZvitService::getCheckByCash($date, $cash);
        $html = $this->renderPartial('_detailcheck', ['data' => $data]);
        return ['html' => $html];// $this->render('searchidentical',['model'=>$model]);
        //var_dump($html);
        //} 
    }

    public function actionZorder2()
    {
        $model = new Cashproductall;
        $clientlist = CashList::getCashList();
        $rows1 = array();
        if ($model->load(Yii::$app->request->post())) {
        }
        $date1 = date("Y-m-d 0:0:0", strtotime($model->cashdatestart));
        $date2 = date("Y-m-d 23:59:59", strtotime($model->cashdatestart));
        $rows1 = Yii::$app->db->createCommand('SELECT `Cash_id`, sum(paytotal) AS `total` FROM `DAT` WHERE ((`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") AND (`package_type`=0) AND (`pay`>0)) group by Cash_id')->queryAll();
        $rows2 = Yii::$app->db->createCommand('SELECT DAT.Cash_id, sum(DAT_ZM.SMI) as `ztotal` FROM DAT INNER JOIN DAT_ZM ON DAT_ZM.DAT_ID = DAT.id WHERE DAT.package_date BETWEEN "' . $date1 . '" AND "' . $date2 . '" group by DAT.Cash_id order by DAT.package_date, DAT.Cash_id')->queryAll();
        $zvtdata = [];
        foreach ($rows1 as $row) {
            $zvtdata[$row['Cash_id']]['total'] = $row['total'];
        }
        foreach ($rows2 as $row) {
            $zvtdata[$row['Cash_id']]['ztotal'] = $row['ztotal'];
        }
        return $this->render('zorder_1', ['zvtdata' => $zvtdata, 'clientlist' => $clientlist, 'model' => $model]);
    }

    public function actionPayproduct()
    {
        $lost = new LostPackages();
        $lost->cash_id = '0';
        $lost->di = 15;
        if (!$lost->save()) {
            print_r($lost->errors);
        }
    }

}
