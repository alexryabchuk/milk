<?php

namespace frontend\controllers\cash;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\Cashproductclient;
use frontend\service\ZvitService;
use yii\web\Response;


/**
 * Site controller
 */
class ZvitcheckController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'cashmain';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['paycheck'],
                        'allow' => true,
                        'roles' => ['cash_zvitcheck_paycheck'],
                    ],
                    [
                        'actions' => ['clientcheck'],
                        'allow' => true,
                        'roles' => ['cash_zvitcheck_paycheck'],
                    ],
                    [
                        'actions' => ['logout', 'getdetailproduct'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionPaycheck()
    {
        $model = new Cashproductclient();
        $data = ['data' => [], 'summa' => []];
        if ($model->load(Yii::$app->request->post())) {
            $data = ZvitService::checkzvtClient($model->cashdatestart, $model->cashdateend, $model->clientid);
        }
        return $this->render('paycheck', ['zvtdata' => $data['data'], 'model' => $model]);
    }

    public function actionClientcheck()
    {
        $model = new Cashproductclient();
        $data = [];
        if ($model->load(Yii::$app->request->post())) {
            $data = ZvitService::getCheckByCash($model->cashdatestart, $model->clientid);
        }
        //var_dump($data);
        return $this->render('checkclient', ['zvtdata' => $data, 'model' => $model]);
    }

    public function actionGetdetailproduct()
    {
        //if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $di = Yii::$app->request->post('di');
        $cash = Yii::$app->request->post('cash');
        //$cash =4101217065;
        //$di =95531;
        $data = ZvitService::getCheckProductByCashDi($di, $cash);
        $html = $this->renderPartial('_detailcheckproduct', ['data' => $data]);
//        echo $html;
        return ['html' => $html];// $this->render('searchidentical',['model'=>$model]);
        //var_dump($html);
        //}

    }


}
