<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\Packages;
use frontend\Addcash;
use frontend\models\Maxpackage;
use frontend\models\LogMirror;
use frontend\ParseCheck;
use frontend\PackageService;
use app\models\cash\LastCashData;
use app\models\cash\LostPackages;

class ReadcashController1 extends Controller
{

    public $mirrorInterval = [6, 7, 10, 11, 14, 15, 18, 19, 22, 23, 26, 27, 30, 31, 34, 35, 38, 39, 42, 43, 46, 47, 50, 51, 54, 55, 58, 59];
    public $cashInterval = [8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54];

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    private function FinishingXMLStr($st)
    {
        $s = '';
        While (mb_strpos($st, '<?') !== false) {
            $st = substr_replace($st, '', strpos($st, '<?'), strpos($st, '?>') - strpos($st, '<?') + 2);
        }
        While (strpos($st, ' NM=') !== FALSE) {
            $s = $s . substr($st, 0, mb_strpos($st, ' NM=') + 3);
            $st = substr_replace($st, '', 0, strpos($st, ' NM=') + 3);
            $s_nm = substr($st, 0, strpos($st, '='));
            $st = substr_replace($st, '', 0, strpos($st, '='));
            While (strpos($s_nm, '"') !== FALSE) {
                $s_nm = substr_replace($s_nm, '', strpos($s_nm, '"'), 1);
            }
            $nc = strrpos(" ", $s_nm);
            if ($nc !== false) {
                $s_nm = substr($s_nm, 0, $nc + 1) . '"' . substr($s_nm, $nc + 1);
            }
            $s = $s . $s_nm;
        }
        $s = $s . $st;
        return $s;
    }

    private function GetDATStr($DATStr)
    {
        return mb_substr($DATStr, mb_strpos($DATStr, '<DAT'), mb_strpos($DATStr, '</DAT>') - mb_strpos($DATStr, '<DAT') + 6);

    }

    private function DeleteDATStr($DATStr)
    {
        return substr_replace($DATStr, '', strpos($DATStr, '<DAT'), strpos($DATStr, '</DAT>') - strpos($DATStr, '<DAT') + 6);
    }


    private function copyPackageMirror()
    {
        $maxPackage = Yii::$app->db3->createCommand('SELECT max(id) as maxid FROM packages')->queryScalar();
        $maxPackageMirror = Yii::$app->db2->createCommand('SELECT id  FROM maxpackage')->queryOne();
        $logMessage = '';
        $logPackageCount = 0;
        $packages = Yii::$app->db3->createCommand('select id, CAST(date as string) as date, cash_number,request_id, pack_body from packages  where id> ' . $maxPackageMirror['id'] . ' limit 25')->queryAll();
        foreach ($packages as $package) {
            $PackBody = $this->FinishingXMLStr($package['PACK_BODY']);
            $checkCount = 0;
            while (strpos($PackBody, '<DAT')) {
                $DatBody = $this->GetDATStr($PackBody);
                $PackBody = $this->DeleteDATStr($PackBody);
                $res = $mirrorPackages = Yii::$app->db2->createCommand()->insert('packages', [
                    'package_id' => $package['ID'],
                    'date' => date('Y-m-d H:i:s', substr($package['date'], 0, 10)),
                    'cash_number' => $package['CASH_NUMBER'],
                    'request_id' => $package['REQUEST_ID'],
                    'pack_body' => $DatBody,
                ])->execute();
                if ($res == 1) {
                    $checkCount += 1;
                }
            }
            if ($checkCount > 0) {
                $logMessage .= 'Додано пакет: Касовий апарат - ' . $package['CASH_NUMBER'] . ". Чеків -" . $checkCount . "\n";
                $logPackageCount += 1;
            } else {
                $logMessage .= 'Не вдалось додати пакет: Касовий апарат - ' . $package['CASH_NUMBER'] . "\n";
            }
            $lastId = $package['ID'];
        }
        $logMessage .= 'Додано ' . $logPackageCount . ' пакетів';
        $maxPackageModel = Maxpackage::find()->one();
        $maxPackageModel->id = $lastId;
        $maxPackageModel->save();
        $logMessageModel = new LogMirror();
        $logMessageModel->content = $logMessage;
        $logMessageModel->save();
    }

    private function isSetDat($packBody, $id)
    {
        if (strpos($packBody, '<DAT') > -1) {
            return TRUE;
        } else {
            Yii::warning('Відсутній тег DAT: ID - ' . $id, 'parse_package');
            return FALSE;
        }
    }


    private function parserCheck($packBody, $id)
    {
        $package = ParseCheck::parsePackage($packBody);
        if (!PackageService::isSetPackage($package)) {
            $cashId = $package->_package['cash_id'];
            $last_package = LastCashData::getLastPackage($cashId);
            $cashDi = $package->_package['di'];
            if ($last_package == 0) {
                $last_package = $cashDi - 1;
            }
            if (($cashDi - $last_package) > 0) {
                LastCashData::setLastPackage($cashId, $cashDi, $package->_package['package_date']);
                LostPackages::setLostPackages($cashId, $last_package, $cashDi);
                $Pname = 'новий';
            } else {
                LostPackages::delLostPackage($cashId, $cashDi);
                $Pname = 'пропущений';
            }
            PackageService::savePackage($package, $id);
            Yii::info(date('d.m.y H:i:s') . ' Додано ' . $Pname . ' чек № ' . $package->_package['di'] . ' від ' . $package->_package['package_date'], 'parse_package');
        } else {
            Yii::info(date('d.m.y H:i:s') . ' Пропущено існуючий чек № ' . $package->_package['di'] . ' від ' . $package->_package['package_date'], 'parse_package');
        }
    }

    private function parserPackages()
    {
        for ($h = 1; $h < 11; $h++) {
            $cash_packs = PackageService::getPackages(50);
            foreach ($cash_packs as $cash_pack) {
                $packBody = $cash_pack->pack_body;
                $packId = $cash_pack->id;
                if ($this->isSetDat($packBody, $packId)) {
                    $this->parserCheck($packBody, $packId);
                }
                LastCashData::setLastPackage('0', $packId, date('Y-m-d H:i:s'));
            }
        }

    }

    public function actionIndex()
    {
//        $minute= date('i');
//        if (in_array($minute,  $this->mirrorInterval)) {
////          $this->copyPackageMirror();    
//        }
//        if (in_array($minute,  $this->cashInterval)) {
        $this->parserPackages();
//        }
//       if ($minute==56 ) {
//           $this->deleteIdentical();
//       }
//	
        return $this->render('index');
    }


}
