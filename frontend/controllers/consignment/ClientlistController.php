<?php

namespace frontend\controllers\consignment;

use common\models\DovNaspunct;
use Yii;
use frontend\models\consignment\Clientlist;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\consignment\ClientlistSearch;
use yii\web\Response;
use yii\helpers\Html;

/**
 * ClientlistController implements the CRUD actions for Clientlist model.
 */
class ClientlistController extends Controller
{
    /**
     * @inheritdoc
     */
    public $layout = '@app/views/consignment/layouts/main';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'citylist'],
                        'allow' => true,
                        'roles' => ['consignment_dictionary_client'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clientlist models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientlistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clientlist model.
     * @param integer $id
     * @return mixed
     */

    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Перегляд  :" . $model->name,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Змінити', ['update', 'id' => $id], ['class' => 'btn btn-warning', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Clientlist model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Clientlist();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Створити новий Clientlist",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Створити новий Clientlist",
                    'content' => '<span class="text-success">Create Clientlist success</span>',
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Створити ще', ['create'], ['class' => 'btn btn-info', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Створити новий Clientlist",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Clientlist model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post())) {
//            $naspunct = DovNaspunct::getNaspunct($model->naspunct);
//            $model->oblast = $naspunct->oblast_code;
//            $model->region = $naspunct->region_code;
////            $model->naspunct = $naspunct->naspunct;
//            $model->save();
//
//            return $this->redirect(['view', 'id' => $model->id]);
//
//        }
//           else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Змінити: " . $model->name,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => $model->name,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Змінити', ['update', 'id' => $id], ['class' => 'btn btn-warning', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Змінити: " . $model->name,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            }
        } else {

            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Clientlist model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();


        return $this->redirect(['index']);
    }

    /**
     * Finds the Clientlist model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clientlist the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clientlist::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCitylist($q = null, $id = null)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = DovNaspunct::searchByName($q);
        $out['results'] = array_values($data);
        return $out;
    }

}
