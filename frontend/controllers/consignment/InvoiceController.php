<?php

namespace frontend\controllers\consignment;

use frontend\models\consignment\Nakladnalist1Search;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\consignment\Clientlist;
use frontend\models\consignment\Productlist2;
use frontend\models\consignment\Nakladnalist1;
use frontend\models\consignment\Nakladnaitem1;
use yii\web\Response;
use yii\base\Model;

/**
 * Site controller
 */
class InvoiceController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = '@app/views/consignment/layouts/main';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_main'],
                    ],
                    [
                        'actions' => ['add'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_add'],
                    ],
                    [
                        'actions' => ['editlab'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_editlab'],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_edit'],
                    ],
                    [
                        'actions' => ['activate'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_activate'],
                    ],
                    [
                        'actions' => ['deactivate'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_deactivate'],
                    ],
                    [
                        'actions' => ['activateguard'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_activate_guard'],
                    ],
                    [
                        'actions' => ['deactivateguard'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_deactivate_guard'],
                    ],
                    [
                        'actions' => ['addproduct'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_main'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['consignment_invoice_delete'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Nakladnalist1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id' => 'DESC'];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAdd()
    {
        $request = Yii::$app->request;
        $model = new Nakladnalist1();
        $model->ndate = date("Y-m-d");
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Створити нову накладну",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else if ($model->load($request->post())) {
                $invoicemax = Nakladnalist1::find()->orderBy('nnum DESC')->one();
                $invoicenum = $invoicemax->nnum;
                $model->nnum = $invoicenum + 1;
                if ($model->save()) {
                    return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
                }
            } else {
                return [
                    'title' => "Створити нову накладну",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionEditlab($id)
    {
        $request = Yii::$app->request;
        $model = Nakladnalist1::findOne($id);
        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id' => $id])->indexBy('id')->all();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редагування: Лабораторія",
                    'content' => $this->renderAjax('editlab', [
                        'model' => $model,
                        'invoiceItem' => $invoiceItem
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $model->saveItem(Yii::$app->request->post('Nakladnaitem1'));
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                return [
                    'title' => "Редагування: Лабораторія",
                    'content' => $this->renderAjax('editlab', [
                        'model' => $model,
                        'invoiceItem' => $invoiceItem
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                $model->saveItem(Yii::$app->request->post('Nakladnaitem1'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('editlab', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = Nakladnalist1::findOne($id);
        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id' => $id])->indexBy('id')->all();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редагування: Охорона",
                    'content' => $this->renderAjax('edit', [
                        'model' => $model,
                        'invoiceItem' => $invoiceItem
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                if (Model::loadMultiple($invoiceItem, Yii::$app->request->post()) && Model::validateMultiple($invoiceItem)) {
                    foreach ($invoiceItem as $item) {
                        $item->save(false);
                    }
                    $model->save();
                }
                return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            } else {
                return [
                    'title' => "Редагування: Охорона",
                    'content' => $this->renderAjax('edit', [
                        'model' => $model,
                        'invoiceItem' => $invoiceItem
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])

                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                $model->saveItem(Yii::$app->request->post('Nakladnaitem1'));
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('editlab', [
                    'model' => $model,
                ]);
            }
        }
//        $invoice = Nakladnalist1::findOne($id);
//        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id'=>$invoice->id])->indexBy('id')->all();
//        if (Yii::$app->request->post('save')!==NULL) {
//            if (Model::loadMultiple($invoiceItem, Yii::$app->request->post()) && Model::validateMultiple($invoiceItem)) {
//                foreach ($invoiceItem as $item) {
//                    $item->save(false);
//                }
//                $invoice->load(Yii::$app->request->post());
//                $invoice->save();
//                $this->redirect(['consignment/invoice']);
//            }
//        }
//        return $this->render('edit',[
//            'invoice'=>$invoice,
//            'invoiceItem'=>$invoiceItem,
//            'productList'=>  Productlist2::getProduct(),
//            'clientList'=>Clientlist::getActiveClientList()]);
    }

    public function actionView($id)
    {
        $this->layout = "print";
        $invoice = Nakladnalist1::findOne($id);
        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id' => $invoice->id])->indexBy('id')->all();
        return $this->render('print', [
            'invoice' => $invoice,
            'invoiceItem' => $invoiceItem,
            'productList' => Productlist2::getProduct(),
            'clientList' => Clientlist::getActiveClientList()]);
    }

    public function actionActivate($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoice->closedlab = 1;
        $invoice->save();
        $this->redirect(['consignment/invoice']);
    }

    public function actionDeactivate($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoice->closedlab = 0;
        $invoice->save();
        $this->redirect(['consignment/invoice']);
    }

    public function actionActivateguard($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoice->closeguard = 1;
        $invoice->save();
        $this->redirect(['consignment/invoice']);
    }

    public function actionDeactivateguard($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoice->closeguard = 0;
        $invoice->save();
        $this->redirect(['consignment/invoice']);
    }

    public function actionDelete($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id' => $invoice->id])->all();
        foreach ($invoiceItem as $item) {
            $item->delete();
        }
        $invoice->delete();
        $this->redirect(['consignment/invoice']);
    }

    public function actionAddproduct()
    {

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $this->layout = false;
            $item = new Nakladnaitem1();
            $item->col = 0;
            $item->col1 = 0;
            $item->col2 = 0;
            $item->dozr = date("Y-m-d");
            $item->realiz = date("Y-m-d");
            $item->prichina = "Причина";
            $item->nnumn = 1;
            $item->ndaten = date("Y-m-d");
            $td = $this->render('addproduct', ['item' => $item, 'productList' => Productlist2::getProduct(),
                'clientList' => Clientlist::getActiveClientList()]);
            return ['td' => $td];
        }

    }

}
