<?php

namespace frontend\controllers\consignment;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\Productlist;
use frontend\models\Cashproductall;
use frontend\models\Cashproductclient;
use frontend\models\CashList;
use frontend\models\Cashlostdelete;
use frontend\models\lostpackage;
use yii\helpers\ArrayHelper;

/**
 * Site controller
 */
class MainController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = '@app/views/consignment/layouts/main';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['consignment_main'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        //створити модель
        //вивести
        return $this->render('index');
    }


}
