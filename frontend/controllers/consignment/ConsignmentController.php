<?php

namespace frontend\controllers\consignment;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\consignment\Clientlist;
use frontend\models\consignment\Productlist2;
use frontend\models\consignment\Nakladnalist2;
use frontend\models\consignment\Nakladnaitem2;
use yii\data\Pagination;
use yii\web\Response;

/**
 * Site controller
 */
class ConsignmentController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'consignmentmain';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['consignment_consignment_main'],
                    ],
                    [
                        'actions' => ['add'],
                        'allow' => true,
                        'roles' => ['consignment_consignment_add'],
                    ],
                    [
                        'actions' => ['edit'],
                        'allow' => true,
                        'roles' => ['consignment_consignment_edit'],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => ['consignment_consignment_delete'],
                    ],
                    [
                        'actions' => ['addproduct'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['ostat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $clientList = Clientlist::getClientList();
        $query = Nakladnalist2::find();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 20]);
        $consignmentList = $query->orderBy('ndate DESC')->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        return $this->render('index', [
            'consignmentList' => $consignmentList,
            'pages' => $pages,
            'clientList' => $clientList]);
    }

    public function actionAdd()
    {
        $consignment = new Nakladnalist2();
        $consignment->closed = 0;
        $consignment->deleted = 0;
        $consignment->ndate = date("Y-m-d");
        $consignmentItem = [];
        $consignmentItem[] = new Nakladnaitem2();
        if (Yii::$app->request->post('save') !== NULL) {
            if ($consignment->load(Yii::$app->request->post()) && $consignment->validate()) {
                $consignment->save();
                $Nakladnaitem2 = Yii::$app->request->post('Nakladnaitem2');
                foreach ($Nakladnaitem2 as $item) {
                    $consignment1 = new Nakladnaitem2();
                    $consignment1->nakladnalist2_id = $consignment->id;
                    $consignment1->product_id = $item['product_id'];
                    $consignment1->col = $item['col'];
                    $consignment1->save();
                }
                $this->redirect(['consignment/consignment']);
            }
        }
        return $this->render('add', [
            'consignmentItem' => $consignmentItem,
            'consignment' => $consignment,
            'productList' => Productlist2::getProduct(),
            'clientList' => Clientlist::getActiveClientList(),
        ]);
    }

    public function actionEdit($id)
    {
        $consignment = Nakladnalist2::findOne($id);
        $consignmentItem = Nakladnaitem2::find()->where(['nakladnalist2_id' => $consignment->id])->indexBy('id')->all();
        if (Yii::$app->request->post('save') !== NULL) {
            if ($consignment->load(Yii::$app->request->post()) && $consignment->validate()) {
                $consignment->save();
                foreach ($consignmentItem as $item) {
                    $item->delete();
                }
                $Nakladnaitem2 = Yii::$app->request->post('Nakladnaitem2');
                foreach ($Nakladnaitem2 as $item) {
                    $consignment1 = new Nakladnaitem2();
                    $consignment1->nakladnalist2_id = $consignment->id;
                    $consignment1->product_id = $item['product_id'];
                    $consignment1->col = $item['col'];
                    $consignment1->save();
                }
                $this->redirect(['consignment/consignment']);
            }
        }
        return $this->render('add', [
            'consignmentItem' => $consignmentItem,
            'consignment' => $consignment,
            'productList' => Productlist2::getProduct(),
            'clientList' => Clientlist::getActiveClientList(),
        ]);
    }

    public function actionDelete($id)
    {
        $consignment = Nakladnalist2::findOne($id);
        $consignmentItem = Nakladnaitem2::find()->where(['nakladnalist2_id' => $consignment->id])->indexBy('id')->all();
        foreach ($consignmentItem as $item) {
            $item->delete();
        }
        $consignment->delete();
        $this->redirect(['consignment/consignment']);
    }

    public function actionAddproduct()
    {

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $this->layout = false;
            $item = new Nakladnaitem2();
            $item->col = 0;
            $item->ost = 0;
            $td = $this->render('addproduct', ['item' => $item, 'productList' => Productlist2::getProduct(),
                'clientList' => Clientlist::getClietList()]);
            return ['td' => $td];
        }

    }

    public function actionOstat()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $client = Yii::$app->request->get('client');
            $product = Yii::$app->request->get('product');
            $data1 = Yii::$app->db->createCommand('SELECT SUM(`nakladnaitem1`.`col`) as col FROM `nakladnalist1` INNER JOIN `nakladnaitem1` ON `nakladnalist1`.`id` = `nakladnaitem1`.`nakladnalist1_id` WHERE (`client_id` =' . $client . ') AND (`product_id`=' . $product . ') AND (`closeguard`=1)')->queryAll();
            $data2 = Yii::$app->db->createCommand('SELECT   SUM( `nakladnaitem2`.`col` ) as col FROM `nakladnalist2` INNER JOIN `nakladnaitem2`  ON `nakladnalist2`.`id` = `nakladnaitem2`.`nakladnalist2_id` WHERE    ( `client_id` =' . $client . ') AND ( `product_id` = ' . $product . ' ) AND ( `closed` =1  )')->queryAll();
            $ost = ($data1[0]['col'] - $data2[0]['col']);
//            $ost=0;
            return ['ost' => $ost];
        }
    }
}
