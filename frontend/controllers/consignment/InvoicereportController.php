<?php

namespace frontend\controllers\consignment;

use frontend\models\consignment\Categorylist;
use kartik\mpdf\Pdf;
use Yii;
use yii\base\InvalidParamException;
use yii\data\ArrayDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\consignment\Clientlist;
use frontend\models\consignment\Productlist2;
use frontend\models\consignment\Nakladnalist1;
use frontend\models\consignment\Nakladnaitem1;
use yii\data\Pagination;
use yii\web\Response;
use yii\base\Model;
use frontend\models\consignment\ReportForm;

/**
 * Site controller
 */
class InvoicereportController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = '@app/views/consignment/layouts/main';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['client', 'days'],
                        'allow' => true,
                        'roles' => ['consignment_report_invoice'],
                    ],
                    [
                        'actions' => ['product'],
                        'allow' => true,
                        'roles' => ['consignment_report_invoice'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionClient()
    {
        $model = new ReportForm();
        $model->startdate = date("Y-m-d");
        $model->enddate = date("Y-m-d");
        $clientList = \frontend\models\consignment\Clientlist::getActiveClientList();
        if (Yii::$app->request->post('save') !== NULL) {
            $this->layout = "print";
            $r = Yii::$app->request->post('ReportForm');
            $firstdate = $r['startdate'];
            $lastdate = $r['enddate'];
            $client = $r['client'];
            $all = $r['all'];
            if ($all == 1) {
                $data = Yii::$app->db->createCommand('CALL rep1("' . $firstdate . '","' . $lastdate . '")')->queryAll();
            } else {
                $data = Yii::$app->db->createCommand('CALL rep3("' . $firstdate . '","' . $lastdate . '","' . $client . '")')->queryAll();
            }
            $Rep_data = array();
            foreach ($data as $key => $value) {
                $Rep_data[$value['clientname']][] = $value;
            }
            return $this->createZvitPDF($this->render('rep2', ['startdate' => $firstdate, 'enddate' => $lastdate, 'Rep_data' => $Rep_data]));
        }
        return $this->render('client', [
            'model' => $model,
            'clientList' => $clientList]);
    }

    public function actionDays()
    {
        $model = new ReportForm();
        $model->startdate = date("Y-m-d");
        $model->enddate = date("Y-m-d");
        $category = Categorylist::getCategoryList();
        if (Yii::$app->request->post('save') !== NULL) {
            $this->layout = "print";
            $r = Yii::$app->request->post('ReportForm');
            $firstdate = $r['startdate'];
            $lastdate = $r['enddate'];
            $data = Yii::$app->db->createCommand('CALL rep5("' . $firstdate . '","' . $lastdate . '")')->queryAll();

            $Rep_data = array();
            foreach ($data as $key => $value) {
                $Rep_data[$value['ndate']][] = $value;
            }
            return $this->createZvitPDF($this->render('rep5', ['startdate' => $firstdate, 'enddate' => $lastdate, 'Rep_data' => $Rep_data, 'CategoryList' => $category]));
        }
        return $this->render('client', [
            'model' => $model,
            'clientList' => $clientList]);
    }

    public function actionProduct()
    {

        $model = new ReportForm();
        $model->startdate = date("Y-m-d");
        $model->enddate = date("Y-m-d");
        $categoryList = \frontend\models\consignment\Clientcategory::getClentCategory();
        $categoryList[0] = 'Всі';
        if (Yii::$app->request->post('save') !== NULL) {
            return $this->createZvitPDF($this->rep2());

        }
        return $this->render('product', [
            'model' => $model,
            'categoryList' => $categoryList
        ]);
    }

    public function rep1()
    {
        $this->layout = "print";
        $r = Yii::$app->request->post('ReportForm');
        $firstdate = $r['startdate'];
        $lastdate = $r['enddate'];
        $vidbir = $r['vidbir'];
        $category = $r['category'];
        $client = $r['client'];
        if ($vidbir == 0) {
            if ($category == 0) {
                $data = Yii::$app->db->createCommand('CALL rep1("' . $firstdate . '","' . $lastdate . '")')->queryAll();
            } else {
                $data = Yii::$app->db->createCommand('CALL rep2("' . $firstdate . '","' . $lastdate . '","' . $category . '")')->queryAll();
            }
            $Rep_data = array();
            foreach ($data as $key => $value) {
                $Rep_data[$value['ndate']][] = $value;
            }
            return $this->render('rep1', ['startdate' => $firstdate, 'enddate' => $lastdate, 'Rep_data' => $Rep_data]);
        }
        if ($vidbir == 1) {
            if ($client == 0) {
                $data = Yii::$app->db->createCommand('CALL rep1("' . $firstdate . '","' . $lastdate . '")')->queryAll();
            } else {
                $data = Yii::$app->db->createCommand('CALL rep3("' . $firstdate . '","' . $lastdate . '","' . $client . '")')->queryAll();
            }
            $Rep_data = array();
            foreach ($data as $key => $value) {
                $Rep_data[$value['clientname']][] = $value;
            }
            return $this->render('rep2', ['startdate' => $firstdate, 'enddate' => $lastdate, 'Rep_data' => $Rep_data]);
        }
    }

    public function rep2()
    {
        $this->layout = "print";
        $r = Yii::$app->request->post('ReportForm');
        $firstdate = $r['startdate'];
        $lastdate = $r['enddate'];
        $vidbir_cat = $r['categoryList'];
        if (!$vidbir_cat) {
            $vidbir_cat = [0];
        }
        $Clientcategory_list = \frontend\models\consignment\Clientcategory::getClentCategory();
        $Category_Type = \frontend\models\consignment\Categorylist::getCategoryList();
        if (in_array(0, $vidbir_cat)) {
            unset($vidbir_cat);
            foreach ($Clientcategory_list as $id => $Category) {
                $vidbir_cat[] = $id;
            }
        }
        $Clientcategory_list[0] = 'Всі';
        $data = Yii::$app->db->createCommand('CALL rep4("' . $firstdate . '","' . $lastdate . '")')->queryAll();

        $Rep_data = array();
        foreach ($data as $key => $value) {
            if (in_array($value['firm'], $vidbir_cat)) {
                if (!isset($Rep_data[$value['category']][$value['name']])) {
                    $Rep_data[$value['category']][$value['name']]['allsum'] = 0;
                    $Rep_data[$value['category']][$value['name']]['sum1'] = 0;
                    $Rep_data[$value['category']][$value['name']]['sum2'] = 0;
                    foreach ($vidbir_cat as $cat) {
                        $Rep_data[$value['category']][$value['name']]['cat' . $cat] = 0;
                    };
                };
                $Rep_data[$value['category']][$value['name']]['allsum'] = $Rep_data[$value['category']][$value['name']]['allsum'] + $value['col'];
                $Rep_data[$value['category']][$value['name']]['sum1'] = $Rep_data[$value['category']][$value['name']]['sum1'] + $value['col1'];
                $Rep_data[$value['category']][$value['name']]['sum2'] = $Rep_data[$value['category']][$value['name']]['sum2'] + $value['col2'];
                $Rep_data[$value['category']][$value['name']]['cat' . $value['firm']] = $Rep_data[$value['category']][$value['name']]['cat' . $value['firm']] + $value['col'];;
            };
        }
        return $this->render('rep3', [
            'catlist' => $vidbir_cat,
            'Category_list' => $Clientcategory_list,
            'Category_product' => $Category_Type,
            'startdate' => $firstdate,
            'enddate' => $lastdate,
            'Rep_data' => $Rep_data]);
    }

    protected function createZvitPDF($render)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $render,
            'cssFile' => 'css/print.css',
            'cssInline' => '.img-circle {border-radius: 50%;}',
            'options' => [
                'title' => 'Звіт',
                'subject' => 'PDF'
            ],
            'methods' => [
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);
        //        return $this->render('rep2',['startdate'=>$firstdate,'enddate'=>$lastdate,'Rep_data'=>$Rep_data]);
        return $pdf->render();
    }

}
