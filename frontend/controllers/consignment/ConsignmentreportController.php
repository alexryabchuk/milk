<?php

namespace frontend\controllers\consignment;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use frontend\models\consignment\Clientlist;
use frontend\models\consignment\Productlist2;
use frontend\models\consignment\Nakladnalist1;
use frontend\models\consignment\Nakladnaitem1;
use yii\data\Pagination;
use yii\web\Response;
use yii\base\Model;
use frontend\models\consignment\ReportForm;

/**
 * Site controller
 */
class ConsignmentreportController extends Controller
{
    /**
     * @inheritdoc
     */

    public $layout = 'consignmentmain';


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['client'],
                        'allow' => true,
                        'roles' => ['consignment_report_invoice'],
                    ],
                    [
                        'actions' => ['product'],
                        'allow' => true,
                        'roles' => ['consignment_report_invoice'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionClient()
    {
        $model = new ReportForm();
        $productList = Productlist2::getProduct();
        $productList[0] = 'Всі';
        $categoryList = \frontend\models\consignment\Clientcategory::getClentCategory();
        $categoryList[0] = 'Всі';
        $clientList = \frontend\models\consignment\Clientcategory::getClentCategory();
        $clientList[0] = 'Всі';
        if (Yii::$app->request->post('save') !== NULL) {
            return $this->rep1();
        }
        return $this->render('product', [
            'productList' => $productList,
            'model' => $model,
            'categoryList' => $categoryList,
            'clientList' => $clientList]);
    }

    public function actionProduct()
    {
        $model = new ReportForm();
        $productList = Productlist2::getProduct();
        $productList[0] = 'Всі';
        $categoryList = \frontend\models\consignment\Clientcategory::getClentCategory();
        $categoryList[0] = 'Всі';
        $clientList = \frontend\models\consignment\Clientcategory::getClentCategory();
        $clientList[0] = 'Всі';
        if (Yii::$app->request->post('save') !== NULL) {
            print_r(Yii::$app->request->post());
            return $this->rep2();
        }
        return $this->render('product', [
            'productList' => $productList,
            'model' => $model,
            'categoryList' => $categoryList,
            'clientList' => $clientList]);
    }

    public function rep1()
    {
        $r = Yii::$app->request->post('ReportForm');
        $firstdate = $r['startdate'];
        $lastdate = $r['enddate'];
        $vidbir = $r['vidbir'];
        $category = $r['category'];
        $client = $r['client'];
        if ($vidbir == 0) {
            if ($category == 0) {
                $data = Yii::$app->db->createCommand('CALL rep1("' . $firstdate . '","' . $lastdate . '")')->queryAll();
            } else {
                $data = Yii::$app->db->createCommand('CALL rep2("' . $firstdate . '","' . $lastdate . '","' . $category . '")')->queryAll();
            }
            $Rep_data = array();
            foreach ($data as $key => $value) {
                $Rep_data[$value['ndate']][] = $value;
            }
            return $this->render('rep1', ['startdate' => $firstdate, 'enddate' => $lastdate, 'Rep_data' => $Rep_data]);
        }
        if ($vidbir == 1) {
            if ($client == 0) {
                $data = Yii::$app->db->createCommand('CALL rep1("' . $firstdate . '","' . $lastdate . '")')->queryAll();
            } else {
                $data = Yii::$app->db->createCommand('CALL rep3("' . $firstdate . '","' . $lastdate . '","' . $client . '")')->queryAll();
            }
            $Rep_data = array();
            foreach ($data as $key => $value) {
                $Rep_data[$value['clientname']][] = $value;
            }
            return $this->render('rep2', ['startdate' => $firstdate, 'enddate' => $lastdate, 'Rep_data' => $Rep_data]);
        }
    }

    public function rep2()
    {
        $r = Yii::$app->request->post('ReportForm');
        $firstdate = $r['startdate'];
        $lastdate = $r['enddate'];
        $vidbir_cat = $r['categoryList'];


        $Clientcategory_list = \frontend\models\consignment\Clientcategory::getClentCategory();


        $Category_Type = \frontend\models\consignment\Categorylist::getCategoryList();

        if (in_array(0, $vidbir_cat)) {
            unset($vidbir_cat);
            foreach ($Clientcategory_list as $Category) {
                $vidbir_cat[] = $Category['id'];
            }
        }
        $Clientcategory_list[0] = 'Всі';
        $data = Yii::$app->db->createCommand('CALL rep4("' . $firstdate . '","' . $lastdate . '")')->queryAll();

        $Rep_data = array();
        foreach ($data as $key => $value) {
            if (in_array($value['firm'], $vidbir_cat)) {
                if (!isset($Rep_data[$value['category']][$value['name']])) {
                    $Rep_data[$value['category']][$value['name']]['allsum'] = 0;
                    $Rep_data[$value['category']][$value['name']]['sum1'] = 0;
                    $Rep_data[$value['category']][$value['name']]['sum2'] = 0;
                    foreach ($vidbir_cat as $cat) {
                        $Rep_data[$value['category']][$value['name']]['cat' . $cat] = 0;
                    };
                };
                $Rep_data[$value['category']][$value['name']]['allsum'] = $Rep_data[$value['category']][$value['name']]['allsum'] + $value['col'];
                $Rep_data[$value['category']][$value['name']]['sum1'] = $Rep_data[$value['category']][$value['name']]['sum1'] + $value['col1'];
                $Rep_data[$value['category']][$value['name']]['sum2'] = $Rep_data[$value['category']][$value['name']]['sum2'] + $value['col2'];
                $Rep_data[$value['category']][$value['name']]['cat' . $value['firm']] = $Rep_data[$value['category']][$value['name']]['cat' . $value['firm']] + $value['col'];;
            };
            //$Rep_data[$value['productname']=$value;
        }
        echo '<pre>';
//        print_r($Rep_data);
        echo '</pre>';
        return $this->render('rep3', [
            'catlist' => $vidbir_cat,
            'Category_list' => $Clientcategory_list,
            'Category_product' => $Category_Type,
            'startdate' => $firstdate,
            'enddate' => $lastdate,
            'Rep_data' => $Rep_data]);
    }

    public function actionAdd()
    {
        $invoice = new Nakladnalist1();
        $invoice->ndate = date("Y-m-d");
        if ($invoice->load(Yii::$app->request->post())) {
            $invoicemax = Nakladnalist1::find()->orderBy('nnum DESC')->one();
            $invoicenum = $invoicemax->nnum;
            $invoice->nnum = $invoicenum + 1;
            if ($invoice->save()) {
                return $this->redirect(['consignment/invoice']);
            }
            print_r($invoice->errors);
        } else
            return $this->render('add', [
                'invoice' => $invoice,
                'clientList' => Clientlist::getClietList()]);
    }

    public function actionEditlab($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id' => $invoice->id])->indexBy('id')->all();
        if (Yii::$app->request->post('save') !== NULL) {

            foreach ($invoiceItem as $item) {
                $item->delete();
            }
            $Nakladnaitem1 = Yii::$app->request->post('Nakladnaitem1');
            foreach ($Nakladnaitem1 as $item) {
                $invoice1 = new Nakladnaitem1();
                $invoice1->nakladnalist1_id = $id;
                $invoice1->product_id = $item['product_id'];
                $invoice1->col = $item['col'];
                $invoice1->col1 = $item['col1'];
                $invoice1->col2 = $item['col2'];
                $invoice1->dozr = $item['dozr'];
                $invoice1->realiz = $item['realiz'];
                $invoice1->napravleno1 = $item['napravleno1'];
                $invoice1->napravleno2 = $item['napravleno2'];
                $invoice1->prichina = $item['prichina'];
                $invoice1->nnumn = $item['nnumn'];
                $invoice1->ndaten = $item['ndaten'];
                $invoice1->save();
                $this->redirect(['consignment/invoice']);
            }

        }
        return $this->render('editlab', [
            'invoice' => $invoice,
            'invoiceItem' => $invoiceItem,
            'productList' => Productlist2::getProduct(),
            'clientList' => Clientlist::getClietList()]);
    }

    public function actionEdit($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id' => $invoice->id])->indexBy('id')->all();
        if (Yii::$app->request->post('save') !== NULL) {
            if (Model::loadMultiple($invoiceItem, Yii::$app->request->post()) && Model::validateMultiple($invoiceItem)) {
                foreach ($invoiceItem as $item) {
                    $item->save(false);
                }
                $invoice->load(Yii::$app->request->post());
                $invoice->save();
                $this->redirect(['consignment/invoice']);
            }
        }
        return $this->render('edit', [
            'invoice' => $invoice,
            'invoiceItem' => $invoiceItem,
            'productList' => Productlist2::getProduct(),
            'clientList' => Clientlist::getClietList()]);
    }

    public function actionView($id)
    {
        $this->layout = "print";
        $invoice = Nakladnalist1::findOne($id);
        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id' => $invoice->id])->indexBy('id')->all();
        return $this->render('print', [
            'invoice' => $invoice,
            'invoiceItem' => $invoiceItem,
            'productList' => Productlist2::getProduct(),
            'clientList' => Clientlist::getClietList()]);
    }

    public function actionActivate($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoice->closedlab = 1;
        $invoice->save();
        $this->redirect(['consignment/invoice']);
    }

    public function actionDeactivate($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoice->closedlab = 0;
        $invoice->save();
        $this->redirect(['consignment/invoice']);
    }

    public function actionActivateguard($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoice->closeguard = 1;
        $invoice->save();
        $this->redirect(['consignment/invoice']);
    }

    public function actionDeactivateguard($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoice->closeguard = 0;
        $invoice->save();
        $this->redirect(['consignment/invoice']);
    }

    public function actionDelete($id)
    {
        $invoice = Nakladnalist1::findOne($id);
        $invoiceItem = Nakladnaitem1::find()->where(['nakladnalist1_id' => $invoice->id])->all();
        foreach ($invoiceItem as $item) {
            $item->delete();
        }
        $invoice->delete();
        $this->redirect(['consignment/invoice']);
    }

    public function actionAddproduct()
    {

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $this->layout = false;
            $item = new Nakladnaitem1();
            $item->col = 0;
            $item->col1 = 0;
            $item->col2 = 0;
            $item->dozr = date("Y-m-d");
            $item->realiz = date("Y-m-d");
            $item->prichina = "Причина";
            $item->nnumn = 1;
            $item->ndaten = date("Y-m-d");
            $td = $this->render('addproduct', ['item' => $item, 'productList' => Productlist2::getProduct(),
                'clientList' => Clientlist::getClietList()]);
            return ['td' => $td];
        }

    }

}
