<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #login_block {
        width: 348px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 50px;
    }

    #login_block img {
        margin-bottom: 25px;


    }
</style>

<div class="row" id="login_block">
    <?php echo Html::img('/image/logo.gif'); ?>
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    <div class="col-lg-10 col-lg-offset-1">
        <div class="form-group">
            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="form-group">
            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>
        <div>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <div style="color:#999;margin:1em 0">
                Якщо ви забули пароль, то можете <?= Html::a('відновити його', ['site/request-password-reset']) ?>.
            </div>
            <div class="form-group">
                <?= Html::submitButton('Вхід', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

