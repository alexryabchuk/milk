<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

$this->title = 'Портал Літинського мочного заводу';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-lg-4">
                <?php
                if (\Yii::$app->user->can('cash_main')) {
                    echo Html::a(Html::img('/image/project1.jpg', ['class' => "img-responsive img-circle"]), yii\helpers\Url::to(['/cash/main']));
                }
                ?>
            </div>
            <div class="col-lg-4">
                <?php
                if (\Yii::$app->user->can('consignment_main')) {
                    echo Html::a(Html::img('/image/project2.jpg', ['class' => "img-responsive img-circle"]), yii\helpers\Url::to(['/consignment/main']));
                }
                ?>
            </div>
            <div class="col-lg-4">
                <?php
                if (\Yii::$app->user->can('milk_main')) {
                    echo Html::a(Html::img('/image/project3.jpg', ['class' => "img-responsive img-circle"]), yii\helpers\Url::to(['/milk/main']));
                }
                ?>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-4">
                <?php
                if (\Yii::$app->user->can('cash_main')) {
                    echo '<h2 class="text-center">Касові апарати</h2>';
                }
                ?>
            </div>
            <div class="col-lg-4">
                <?php
                if (\Yii::$app->user->can('consignment_main')) {
                    echo '<h2 class="text-center">Повернення продукції</h2>';
                }
                ?>
            </div>
            <div class="col-lg-4">
                <?php
                if (\Yii::$app->user->can('milk_main')) {
                    echo '<h2 class="text-center">Приймання молока</h2>';
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <?php
                if (\Yii::$app->user->can('cash_main')) {
                    echo Html::a(Html::img('/image/project4.jpg', ['class' => "img-responsive img-circle"]), yii\helpers\Url::to(['/label/spec']));
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <?php
                if (\Yii::$app->user->can('cash_main')) {
                    echo '<h2 class="text-center">Зважування та маркування</h2>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
