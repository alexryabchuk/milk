<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\FormatConverter;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo Yii::$app->formatter->format($time1, 'time'); ?>
    <br>
    <?php echo Yii::$app->formatter->format($time2, 'time'); ?>
</div>
