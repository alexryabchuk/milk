<?php

/* @var $this yii\web\View */

//use yii;
use common\models\Productlist;
use components\widgets\SelectPeriod;


$this->title = 'Звіт: Продукція загальний';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <?php
        $productList = Productlist::getProductList();
        echo SelectPeriod::widget(['model' => $model]);
        $AllSumma = 0;
        $AllVes = 0;
        echo "<thead><tr><th>Назва продукту</th><th>Сумма</th><th>Кількість</th></tr></thead>";
        foreach ($zvtdata as $data) {
            $summa = $data['NM1'];
            $AllSumma = $AllSumma + $summa;
            $ves = $data['Q1'];
            $AllVes = $AllVes + $ves;
            if (isset($productList[$data['C']])) {
                echo "<tr><td>" . $productList[$data['C']]['name'] . "</td>";
            } else {
                echo "<tr><td>Не визначений товар: Код " . $data['C'] . "</td>";
            }
            echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($summa) . "</td>";
            echo "<td class='text-right'>" . $ves . "</td></tr>";
        }
        echo "<tfoot><tr><td>Всього</td><td class='text-right'>" . yii::$app->formatter->asCurrency($AllSumma) . "</td><td class='text-right'>" . $AllVes . "</td></tr></tfoot>";
        ?>
    </table>
</div>
