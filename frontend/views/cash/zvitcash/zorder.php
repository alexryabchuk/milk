<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\CashList;


$this->title = 'Звіт: Продукція і z-звіти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <?php
        $clientList = CashList::getCashList();
        $form = ActiveForm::begin(['id' => 'n-form']);
        echo "<div class='row'><div class='col-lg-6'>" . $form->field($model, 'cashdatestart')->input('date') . "</div>";
        echo "<div class='col-lg-6'></div></div>";
        echo '<div class="form-group">';
        echo Html::submitButton("Зформувати", ["class" => "btn btn-primary", "name" => "login-button"]);
        echo '</div>';
        ActiveForm::end();
        echo "<thead><tr><th></th><th>Контрагент</th><th>Сумма продажу</th><th>Сумма продукції</th><th>К-сть чеків</th><th>Сумма Z-звіту</th><th>К-сть чеків</th></tr></thead>";
        if (count($zvtdata) > 0) {
            foreach ($zvtdata as $id => $data) {
                echo "<tr ";
                if ($data['p'] != $data['e']) {
                    echo 'style="background:#FF1493"';
                }
                if ($data['z'] != $data['e']) {
                    echo 'style="background:red"';
                }
                echo ">";
                echo '<td><button type="button" id="' . $id . '" class="btn btn-default btn-xs detail-check" aria-label="Left Align"><span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></button></td>';
                if (isset($clientList[$id])) {
                    echo "<td>" . $clientList[$id] . "</td>";
                } else {
                    echo "<td>Не визначений касовий: Код " . $id . "</td>";
                }
                echo "<td class='text-right'>" . $data['p'] . "</td>";
                echo "<td class='text-right'>" . $data['e'] . "</td>";
                echo "<td class='text-right'>" . $data['c'] . "</td>";
                echo "<td class='text-right'>" . $data['z'] . "</td>";
                echo "<td class='text-right'>" . $data['ni'] . "</td>";
                echo "</tr>";
                echo "<tr id='detail-" . $id . "' style='display:none'><td></td></tr>";
            }
            echo "<tr><td colspan='2'>Всього</td>";
            echo "<td class='text-right'>" . $summa['p'] . "</td>";
            echo "<td class='text-right'>" . $summa['e'] . "</td>";
            echo "<td class='text-right'>" . $summa['c'] . "</td>";
            echo "<td class='text-right'>" . $summa['z'] . "</td>";
            echo "<td class='text-right'>" . $summa['NI'] . "</td>";
            echo "</tr>";

        }

        ?>
    </table>
</div>

<script>
    $(".detail-check").click(function () {
            var id = $(this).attr("id");
            var date = $("#cashproductall-cashdatestart").val();

            function send(cash, cashdate) {
                $.ajax({
                    url: '/index.php?r=cash%2Fzvitcash%2Fgetdetailcheck',
                    type: "POST",
                    timeout: 0,
                    data: {
                        date: cashdate,
                        cash: cash
                    },
                    success: function (data) {
                        $("#detail-" + cash).html("<td colspan='7'>" + data.html + "</td>");
                        $("#detail-" + cash).toggle();
                    },
                    error: function (jqXHR, status, e) {
                        if (status === "timeout") {
                            alert("Время ожидания ответа истекло!");
                        } else {
                            alert(status); // Другая ошибка
                        }
                    }
                });
            }

            send(id, date);
        }
    );

</script>
