<?php

/* @var $this yii\web\View */

use yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<style>
    .table-detailcheck td {
        border-top: 1px solid #ddd;
        font-size: 12px;
    }

    .table-detailcheck th {

        font-size: 12px;
    }

    .table-detailcheck {
        width: 60%;
    }
</style>
<table class="table-detailcheck">
    <thead>
    <th style="width:30%">Дата</th>
    <th style="width: 10%">№чека</th>
    <th style="width: 15%">Сума чека</th>
    <th style="width: 15%">Сума по продукції</th>
    </thead>
    <tbody>
    <?php $lastNo = 0 ?>
    <?php foreach ($data as $item): ?>
        <?php echo "<tr"; ?>
        <?php if (($lastNo != 0) && (($item['e_no'] - $lastNo)) > 1) {
            echo ' style="background:purple"';
        }
        ?>
        <?php if ($item['e_no'] == $lastNo) {
            echo ' style="background:red"';
        }
        ?>
        <?php if ($item['e_sm'] != $item['p_sm']) {
            echo ' style="background:#FF1493"';
        }
        ?>
        <?php echo ">"; ?>
        <td><?= yii::$app->formatter->asDateTime($item['package_date']) ?></td>
        <td><?= $item['e_no'] ?></td>
        <td><?= yii::$app->formatter->asCurrency($item['e_sm']) ?></td>
        <td><?= yii::$app->formatter->asCurrency($item['p_sm']) ?></td>
        </tr>
        <?php $lastNo = $item['e_no'] ?>
    <?php endforeach ?>

    </tbody>
</table>
