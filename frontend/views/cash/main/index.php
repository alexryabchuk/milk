<style>

    .calendar-status {
        display: block;
        width: 812px;
        margin: 40px auto 10px;
        padding: 10px;
        background-color: #FFF;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        border-radius: 6px;
        border: 2px solid black;
        -moz-box-shadow: 0 0 14px #123;
        -webkit-box-shadow: 0 0 14px #123;
        box-shadow: 0 0 14px #123;
    }

    .calendar-status ul {
        display: block;
        clear: left;
        height: 62px;
        width: 772px;
        margin: 0 auto;
        padding: 0;
        list-style: none;
        background-color: #FFF;
        text-align: center;
        border: 1px solid black;
        border-top: 0;
        border-bottom: 2px solid black;
    }

    .calendar-status li {
        position: relative;
        float: left;
        margin: 0;
        padding: 20px 2px 2px;
        border-left: 1px solid black;
        border-right: 1px solid black;
        width: 110px;
        height: 60px;
        overflow: hidden;
        background-color: white;
    }


    ul.weekdays {
        height: 62px;

        border-bottom: 2px solid black;
    }

    .weekdays li {
        height: 60px;
        padding: 2px 2px;
        background-color: #BCF;
        border-top: 2px solid black;
    }

    li.fill {
        background-color: #BCD;
    }

    li.status-nopackage {
        background-color: red;
    }

    .status-nopackage:hover::after {
        content: "За цей день є пропущені чеки"; /* Выводим текст */
        position: absolute; /* Абсолютное позиционирование */
        left: 0%;
        top: 30%; /* Положение подсказки */
        z-index: 80; /* Отображаем подсказку поверх других элементов */
        background: rgba(255, 255, 230, 0.9); /* Полупрозрачный цвет фона */
        font-family: Arial, sans-serif; /* Гарнитура шрифта */
        font-size: 11px; /* Размер текста подсказки */
        padding: 5px 10px; /* Поля */
        border: 1px solid #333; /* Параметры рамки */
    }

    li.status-ok {
        background-color: green;
    }

    .status-ok:hover::after {
        content: "За цей день дані актуальні"; /* Выводим текст */
        position: absolute; /* Абсолютное позиционирование */
        left: 0%;
        top: 30%; /* Положение подсказки */
        z-index: 80; /* Отображаем подсказку поверх других элементов */
        background: rgba(255, 255, 230, 0.9); /* Полупрозрачный цвет фона */
        font-family: Arial, sans-serif; /* Гарнитура шрифта */
        font-size: 11px; /* Размер текста подсказки */
        padding: 5px 10px; /* Поля */
        border: 1px solid #333; /* Параметры рамки */
    }

    li.status-sumerror {
        background-color: pink;
    }

    .status-sumerror:hover::after {
        content: "За цей день є різниця сум"; /* Выводим текст */
        position: absolute; /* Абсолютное позиционирование */
        left: 0%;
        top: 30%; /* Положение подсказки */
        z-index: 80; /* Отображаем подсказку поверх других элементов */
        background: rgba(255, 255, 230, 0.9); /* Полупрозрачный цвет фона */
        font-family: Arial, sans-serif; /* Гарнитура шрифта */
        font-size: 11px; /* Размер текста подсказки */
        padding: 5px 10px; /* Поля */
        border: 1px solid #333; /* Параметры рамки */
    }


    .calendar-status li strong {
        position: absolute;
        top: 2px;
        right: 2px;
    }

    .calendar-title {
        padding-left: 100px;
        padding-right: 100px;
        font-size: 16px;
        font-weight: bolder;
    }

    .calendar-status li a {
        position: relative;
        display: block;
        border: 1px dotted black;
        margin: 2px;
        padding: 2px;
        font-size: 11px;
        background-color: #DEF;
        text-align: left;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        border-radius: 6px;
        z-index: 1;
        text-decoration: none;
        color: black;
        font-weight: bold;
        font-style: italic;
    }

</style>
<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use components\widgets\CalendarStatusWidget;

$this->title = 'Портал Літинського мочного заводу';
?>
<div class="site-index">
    <div class="body-content">
        <div class="calendar-status">
            <?php echo CalendarStatusWidget::widget(['date' => $date]); ?>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <h2>Касові апарати</h2>
                <p><?php echo Html::a('Стан передачі', yii\helpers\Url::to(['/cash/workcheck/status'])); ?> - інформація
                    про роботу касових апаратів</p>
                <p><?php echo Html::a('Пропущені пакети', yii\helpers\Url::to(['/cash/workcheck/lostpackage'])); ?> -
                    інформація про пропущені пакети, які з якихось причин не були передані</p>

            </div>
            <div class="col-lg-4">
                <h2>Звіт по продукції</h2>
                <p>Продукція загальний - звіт про продану продукцію</p>
                <p>Продукція по контрагенту - звіт про продану продукцію по окремому контрагенту</p>
                <p>Продукція і z-звіти - порівняльний звіт про продану продукцію та отримані кошти</p>
            </div>
            <div class="col-lg-4">
                <h2>Звіт по чекам</h2>
                <p>Оплата по контрагенту - сума оплати по чеках по контрагенту</p>
                <p>Продукція по контрагенту - звіт про продану продукцію по окремому контрагенту в розрізі чеків</p>
            </div>
        </div>
    </div>

</div>
