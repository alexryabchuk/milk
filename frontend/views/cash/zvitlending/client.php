<?php

/* @var $this yii\web\View */

use components\widgets\SelectPeriodCash;


$this->title = 'Звіт загальний внесення/видача коштів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <?php
        echo SelectPeriodCash::widget(['model' => $model]);
        echo "<thead><tr><th rowspan='2' colspan='2'>Дата</th><th colspan='2' class='text-center'>Чеки</th><th colspan='2' class='text-center'>Z-звіти</th></tr>";
        echo "<tr><th  class='text-right'>Сумма внесення</th><th  class='text-right'>Сумма видачі</th><th  class='text-right'>Сумма внесення</th><th  class='text-right'>Сумма видачі</th></tr></>";
        if (count($zvtdata) > 0) {
            foreach ($zvtdata as $day => $data) {
                echo "<tr ";
                if (abs($data['i_sm'] - $data['io_smi']) > 0.01) {
                    echo 'style="background:#FF1493"';
                }
                if (abs($data['o_sm'] - $data['io_smo']) > 0.01) {
                    echo 'style="background:#FF1493"';
                }
                echo ">";
                echo '<td><button type="button" id="' . $day . '" class="btn btn-default btn-xs detail-lend" aria-label="Left Align"><span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></button></td>';
                echo "<td>" . date('d.m.Y', strtotime($day)) . "</td>";
                echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['i_sm']) . "</td>";
                echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['o_sm']) . "</td>";
                echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['io_smi']) . "</td>";
                echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['io_smo']) . "</td></tr>";
                echo "<tr id='detail-" . $day . "' style='display:none'><td></td></tr>";
            }
            echo "<tr><td>Всього</td><td class='text-right'>" . yii::$app->formatter->asCurrency($summa['i_sm']) . "</td><td class='text-right'>" . yii::$app->formatter->asCurrency($summa['o_sm']) . "</td></tr>";

        }
        ?>
    </table>
</div>

<script>
    $(".detail-lend").click(function () {
            var id = $(this).attr("id");
            var cash = $("#cashproductclient-clientid").val();

            function send(cashdate, cash) {
                $.ajax({
                    url: '/index.php?r=cash%2Fzvitlending%2Fgetdetailclient',
                    type: "POST",
                    timeout: 0,
                    data: {
                        date: cashdate,
                        cashid: cash
                    },
                    success: function (data) {
                        $("#detail-" + cashdate).html("<td colspan='7'>" + data.html + "</td>");
                        $("#detail-" + cashdate).toggle();
                    },
                    error: function (jqXHR, status, e) {
                        if (status === "timeout") {
                            alert("Время ожидания ответа истекло!");
                        } else {
                            alert(status); // Другая ошибка
                        }
                    }
                });
            }

            send(id, cash);
        }
    );

</script>