<?php

/* @var $this yii\web\View */

use yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\CashList;

?>
<style>
    .table-detailcheck td {
        border-top: 1px solid #ddd;
        font-size: 12px;
    }

    .table-detailcheck th {

        font-size: 12px;
    }

    .table-detailcheck {
        width: 60%;
    }
</style>
<table class="table-detailcheck">
    <thead>
    <th style="width:30%">Дата та час</th>
    <th style="width:30%">Документ</th>
    <th style="width: 10%">Чек:внесення</th>
    <th style="width: 15%">Чек:видача</th>
    <th style="width: 15%">Z-звіт:внесення</th>
    <th style="width: 15%">Z-звіт:видача</th>
    </thead>
    <tbody>
    <?php $cashList = CashList::getCashList();
    $sum1 = 0;
    $sum2 = 0;
    $sum3 = 0;
    $sum4 = 0;

    ?>
    <?php foreach ($data as $id => $item): ?>
        <tr>
            <?php
            $sum1 += $item['i_sm'];
            $sum2 += $item['o_sm'];
            $sum3 += $item['io_smi'];
            $sum4 += $item['io_smo'];
            ?>
            <td><?= $id ?></td>
            <td><?= $item['name'] ?></td>
            <td><?= yii::$app->formatter->asCurrency($item['i_sm']) ?></td>
            <td><?= yii::$app->formatter->asCurrency($item['o_sm']) ?></td>
            <td><?= yii::$app->formatter->asCurrency($item['io_smi']) ?></td>
            <td><?= yii::$app->formatter->asCurrency($item['io_smo']) ?></td>
        </tr>

    <?php endforeach ?>
    <td colspan="2">Всього</td>
    <td><?= yii::$app->formatter->asCurrency($sum1) ?></td>
    <td><?= yii::$app->formatter->asCurrency($sum2) ?></td>
    <td><?= yii::$app->formatter->asCurrency($sum3) ?></td>
    <td><?= yii::$app->formatter->asCurrency($sum4) ?></td>

    </tbody>
</table>
