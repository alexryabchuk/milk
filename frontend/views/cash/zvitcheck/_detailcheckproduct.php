<?php

/* @var $this yii\web\View */

use yii;
use common\models\Productlist;

$productList = Productlist::getProductList();
?>
<style>
    .table-detailcheck td {
        border-top: 1px solid #ddd;
        font-size: 12px;
    }

    .table-detailcheck th {

        font-size: 12px;
    }

    .table-detailcheck {
        width: 90%;
    }
</style>
<table class="table-detailcheck">
    <thead>

    <th style="width: 10%">№</th>
    <th style="width: 15%">Назва</th>
    <th style="width: 15%">Код</th>
    <th style="width: 15%">Назва в касовому</th>
    <th class="text-right" style="width: 15%">Кількість</th>
    <th class="text-right" style="width: 15%">Ціна</th>
    <th class="text-right" style="width: 15%">Сума</th>
    </thead>
    <tbody>
    <?php foreach ($data as $item): ?>
        <tr>
            <td><?= $item['n'] ?></td>
            <?php
            if (isset($productList[$item['c']])) {
                echo "<td>" . $productList[$item['c']]['name'] . "</td>";
            } else {
                echo "<td>Не визначений касовий: Код " . $item['c'] . "</td>";
            }
            ?>


            <td><?= $item['c'] ?></td>
            <td><?= $item['nm'] ?></td>
            <td class="text-right"><?= yii::$app->formatter->asDecimal($item['q'], 3) ?></td>
            <td class="text-right"><?= yii::$app->formatter->asCurrency($item['prc']) ?></td>
            <td class="text-right"><?= yii::$app->formatter->asCurrency($item['sm']) ?></td>
        </tr>

    <?php endforeach ?>

    </tbody>
</table>
