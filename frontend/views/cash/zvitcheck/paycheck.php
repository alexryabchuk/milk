<?php

/* @var $this yii\web\View */


use components\widgets\SelectPeriodCash;


$this->title = 'Звіт: Оплата по контрагенту';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <?php
        echo SelectPeriodCash::widget(['model' => $model]);
        $AllSumma = 0;
        $AllSumma2 = 0;
        echo "<thead><tr><th></th><th>Дата чека</th><th class='text-right'>Сумма товару</th><th class='text-right'>Сума оплати</th></tr></thead>";
        foreach ($zvtdata as $id => $data) {
            $AllSumma = $AllSumma + $data['p'];
            $AllSumma2 = $AllSumma2 + $data['e'];
            echo '<tr>';
            echo '<td><button type="button" id="' . $id . '" class="btn btn-default btn-xs detail-check" aria-label="Left Align"><span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></button></td>';
            echo "<td>" . yii::$app->formatter->asDate($id) . "</td>";
            echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['p']) . "</td>";
            echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['e']) . "</td>";
            echo "</tr>";
            echo "<tr id='detail-" . $id . "' style='display:none'><td></td></tr>";
        }
        echo "<tr><td colspan='2'>Всього:</td><td>" . yii::$app->formatter->asCurrency($AllSumma) . "</td><td>" . yii::$app->formatter->asCurrency($AllSumma2) . "</td></tr>";
        ?>
    </table>
</div>
<script>
    $(".detail-check").click(function () {
            var date = $(this).attr("id");
            var id = $("#cashproductclient-clientid").val();

            function send(cash, cashdate) {
                $.ajax({
                    url: '/index.php?r=cash%2Fzvitcash%2Fgetdetailcheck',
                    type: "POST",
                    timeout: 0,
                    data: {
                        date: cashdate,
                        cash: cash
                    },
                    success: function (data) {
                        $("#detail-" + cashdate).html("<td colspan='7'>" + data.html + "</td>");
                        $("#detail-" + cashdate).toggle();
                    },
                    error: function (jqXHR, status, e) {
                        if (status === "timeout") {
                            alert("Время ожидания ответа истекло!");
                        } else {
                            alert(status); // Другая ошибка
                        }
                    }
                });
            }

            send(id, date);
        }
    );

</script>