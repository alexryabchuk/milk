<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use components\widgets\SelectPeriodCash;


$this->title = 'Звіт: Продукція загальний';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <?php
        echo SelectPeriodCash::widget(['model' => $model]);
        $AllSumma = 0;
        $AllVes = 0;
        ?>
        <thead>
        <th colspan="2">Дата</th>
        <th>№чека</th>
        <th>Сума чека</th>
        <th>Сума по продукції</th>
        </thead>
        <tbody>
        <?php $lastNo = 0 ?>
        <?php foreach ($zvtdata as $id => $item): ?>
            <?php echo "<tr"; ?>
            <?php if (($lastNo != 0) && (($item['e_no'] - $lastNo)) > 1) {
                echo ' style="background:purple"';
            }
            ?>
            <?php if ($item['e_no'] == $lastNo) {
                echo ' style="background:red"';
            }
            ?>
            <?php if ($item['e_sm'] != $item['p_sm']) {
                echo ' style="background:#FF1493"';
            }
            ?>
            <?php echo ">"; ?>
            <?php echo '<td><button type="button" id="' . $item['di'] . '" class="btn btn-default btn-xs detail-check" aria-label="Left Align"><span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></button></td>'; ?>
            <td><?= yii::$app->formatter->asDateTime($item['package_date']) ?></td>
            <td><?= $item['e_no'] ?></td>
            <td><?= yii::$app->formatter->asCurrency($item['e_sm']) ?></td>
            <td><?= yii::$app->formatter->asCurrency($item['p_sm']) ?></td>
            </tr>
            <?php echo "<tr id='detail-" . $item['di'] . "' style='display:none'><td></td></tr>"; ?>
            <?php $lastNo = $item['e_no'] ?>
        <?php endforeach ?>


        <?php
        echo "<tr><td>Всього</td><td>" . $AllSumma . "</td><td>" . $AllVes . "</td></tr>";
        ?>
        </tbody>
    </table>
</div>

<script>
    $(".detail-check").click(function () {
            var date = $(this).attr("id");
            var id = $("#cashproductclient-clientid").val();

            function send(cash, di) {
                $.ajax({
                    url: '/index.php?r=cash%2Fzvitcheck%2Fgetdetailproduct',
                    type: "POST",
                    timeout: 0,
                    data: {
                        di: di,
                        cash: cash
                    },
                    success: function (data) {
                        $("#detail-" + date).html("<td colspan='7'>" + data.html + "</td>");
                        $("#detail-" + date).toggle();
                    },
                    error: function (jqXHR, status, e) {
                        if (status === "timeout") {
                            alert("Время ожидания ответа истекло!");
                        } else {
                            alert(status); // Другая ошибка
                        }
                    }
                });
            }
            send(id, date);
        }
    );

</script>