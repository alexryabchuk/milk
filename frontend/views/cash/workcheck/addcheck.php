<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Касові апарати: Список пропущених пакетів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h2>Додавання пакету касового апарату: <?= $cashName ?></h2>
    <?php
    $form = ActiveForm::begin();
    echo "<div id='checkitem'>";
    echo $form->field($model, "DI");
    echo $form->field($model, "checkdate")->input('date');
    echo $form->field($model, "checktime")->input('time');
    echo $form->field($model, "pay");
    echo '</div>';
    echo '<div class="form-group">';
    echo Html::submitButton("Зформувати", ["class" => "btn btn-primary", "name" => "login-button"]);
    echo '</div>';
    ActiveForm::end();
    ?>
    <hr>


</div>
