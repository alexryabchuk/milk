<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Касові апарати: Пропущені пакети';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <thead>
        <tr>
            <th>№ касового</th>
            <th>Назва</th>
            <th>Кількість пропущених</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($lostPackageList as $lostPackageListItem) : ?>
            <tr>
                <td>
                    <?php echo HTML::a($lostPackageListItem['cash_id'], ['cash/workcheck/lostcashpackages', 'id' => $lostPackageListItem['cash_id']]) ?>
                </td>
                <td>
                    <?php if (isset($cashList[$lostPackageListItem['cash_id']])) : ?>
                        <?= $cashList[$lostPackageListItem['cash_id']] ?>
                    <?php else: ?>
                        Невідомий
                    <?php endif ?>
                </td>
                <td>
                    <?= $lostPackageListItem['lostcount'] ?>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>
