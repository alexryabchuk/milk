<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Касові апарати: Статус передачі';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <H1>Перетворення дати Z-звіту</H1>
    <p>Поточна дата: <?= Yii::$app->formatter->asDatetime($zorder['package_date'], 'dd.MM.yyyy HH:mm:ss') ?></p>
    <p>Можлива дата: <?= Yii::$app->formatter->asDatetime($check['package_date'], 'dd.MM.yyyy HH:mm:ss') ?></p>
    <?= Html::a('Перетворити', ['cash/workcheck/convertcheck', 'id' => $zorder['id']], ['class' => 'btn btn-primary']) ?>
</div>
