<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $cashName;
$this->params['breadcrumbs'][] = ['label' => 'Касові апарати: Пропущені пакети', 'url' => ['cash/workcheck/lostpackages']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h2>Список пропущених пакетів касового апарату: <?= $cashName ?></h2>
    <?php
    $Last = 0;
    $BeginLan = 0;
    $EndLan = 0;
    foreach ($lostPackageList as $lostPackageListItem) {
        if ($Last === 0) {
            $BeginLan = $lostPackageListItem['di'];
            $Last = $lostPackageListItem['di'];
        }
        if (($lostPackageListItem['di'] - $Last) > 1) {
            $EndLan = $Last;
            $Last = $lostPackageListItem['di'];
            if ($BeginLan == $EndLan) {
                echo HTML::a($BeginLan, ['cash/workcheck/lostcheck', 'id1' => $BeginLan, 'id2' => $EndLan, 'cash' => $cashId]) . ', ';
            } else {
                echo HTML::a($BeginLan . '<=>' . $EndLan, ['cash/workcheck/lostcheck', 'id1' => $BeginLan, 'id2' => $EndLan, 'cash' => $cashId]) . ', ';
            }
            $BeginLan = $lostPackageListItem['di'];
        } else {
            $Last = $lostPackageListItem['di'];
        }
    }
    $EndLan = $Last;
    if ($BeginLan == $EndLan) {
        echo HTML::a($BeginLan, ['cash/workcheck/lostcheck', 'id1' => $BeginLan, 'id2' => $EndLan, 'cash' => $cashId]);
    } else {
        echo HTML::a($BeginLan . '<=>' . $EndLan, ['cash/workcheck/lostcheck', 'id1' => $BeginLan, 'id2' => $EndLan, 'cash' => $cashId]);
    }


    ?>

    <hr>


</div>
