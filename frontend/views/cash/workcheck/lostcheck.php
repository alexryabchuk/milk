<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Інформація про пропущений чек';
$this->params['breadcrumbs'][] = ['label' => 'Касові апарати: Список пропущених пакетів: ' . $cashName, 'url' => ['cash/workcheck/lostcashpackages', 'id' => $cashId]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h3><b>Касовий:</b><?= $cashName ?> </h3>
    <?php
    if ($id1 == $id2) {
        echo "<h3><b>Номер пакету:</b>" . $prevCheck['di'] . "</h3>";
    } else {
        $countLostCheck = $id2 - $id1 + 1;
        echo "<h3><b>Ланцюг пакетів:</b> від " . $id1 . " до " . $id2 . " ($countLostCheck шт)</h3>";
    }
    ?>
    <div class="row">
        <div class="col-lg-6">
            <h4>Інформація про передній чек</h4>
            <?php
            if (!$prevCheck) {
                echo "Інформація відсутня";
            } else {
                if ($prevCheck['package_type'] == 0) {
                    echo "<b>Тип:</b> чек<br>";
                    echo "<b>Дата:</b> " . Yii::$app->formatter->asDateTime($prevCheck['package_date']) . "<br>";
                    echo "<b>Номер:</b> " . $prevCheck['e_no'] . "<br>";
                    echo "<b>Сумма:</b> " . $prevCheck['e_sm'] . "грн<br>";
                }
                if ($prevCheck['package_type'] == 1) {
                    echo "<b>Тип:</b> чек повернення<br>";
                    echo "<b>Дата:</b> " . Yii::$app->formatter->asDateTime($prevCheck['package_date']) . "<br>";
                    echo "<b>Номер:</b> " . $prevCheck['e_no'] . "<br>";
                    echo "<b>Сумма:</b> " . $prevCheck['e_sm'] . "грн<br>";
                }
                if ($prevCheck['package_type'] == 2) {
                    echo "<b>Тип:</b> службовий чек<br>";
                    echo "<b>Дата:</b> " . Yii::$app->formatter->asDateTime($prevCheck['package_date']) . "<br>";
                    echo "<b>DI:</b> " . $prevCheck['di'] . "<br>";
                    echo "<b>Сумма внесення:</b> " . $prevCheck['i_sm'] . "грн<br>";
                    echo "<b>Сумма видачі:</b> " . $prevCheck['o_sm'] . "грн<br>";
                }
                if ($prevCheck['package_type'] == 3) {
                    echo "<b>Тип:</b> z-звіт<br>";
                    echo "<b>Дата:</b> " . Yii::$app->formatter->asDateTime($prevCheck['package_date']) . "<br>";
                }
            }
            ?>
        </div>
        <div class="col-lg-6">
            <h4>Інформація про наступний чек</h4>
            <?php
            if (!$nextCheck) {
                echo "Інформація відсутня";
            } else {
                if ($nextCheck['package_type'] == 0) {
                    echo "<b>Тип:</b> чек<br>";
                    echo "<b>Дата:</b> " . Yii::$app->formatter->asDateTime($nextCheck['package_date']) . "<br>";
                    echo "<b>Номер:</b> " . $nextCheck['e_no'] . "<br>";
                    echo "<b>Сумма:</b> " . $nextCheck['e_sm'] . "грн<br>";
                }
                if ($nextCheck['package_type'] == 1) {
                    echo "<b>Тип:</b> чек повернення<br>";
                    echo "<b>Дата:</b> " . Yii::$app->formatter->asDateTime($nextCheck['package_date']) . "<br>";
                    echo "<b>Номер:</b> " . $nextCheck['e_no'] . "<br>";
                    echo "<b>Сумма:</b> " . $nextCheck['e_sm'] . "грн<br>";
                }
                if ($nextCheck['package_type'] == 2) {
                    echo "<b>Тип:</b> службовий чек<br>";
                    echo "<b>Дата:</b> " . Yii::$app->formatter->asDateTime($nextCheck['package_date']) . "<br>";
                    echo "<b>DI:</b> " . $nextCheck['di'] . "<br>";
                    echo "<b>Сумма внесення:</b> " . $nextCheck['i_sm'] . "грн<br>";
                    echo "<b>Сумма видачі:</b> " . $nextCheck['o_sm'] . "грн<br>";
                }
                if ($nextCheck['package_type'] == 3) {
                    echo "<b>Тип:</b> z-звіт<br>";
                    echo "<b>Дата:</b> " . Yii::$app->formatter->asDateTime($prevCheck['package_date']) . "<br>";
                }
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= html::a('Вилучити', ['cash/workcheck/dellostpackage', 'id1' => $id1, 'id2' => $id2, 'cash' => $cashId], ['class' => 'btn btn-primary']) ?>
            <?= html::a('Додати чек', ['cash/workcheck/addcheck', 'id' => $prevCheck['di'], 'cash' => $cashId], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>


</div>
