<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Касові апарати: Статус передачі';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <?php
        $form = ActiveForm::begin(['id' => 'n-form']);
        echo "<div class='row'><div class='col-lg-6'>" . $form->field($model, 'cashdatestart')->input('date') . "</div>";
        echo "<div class='col-lg-6'>" . $form->field($model, 'cashdateend')->input('date') . "</div></div>";
        echo $form->field($model, 'clientid')->dropDownList($clientlist);
        echo '<div class="form-group">';
        echo Html::submitButton("Зформувати", ["class" => "btn btn-primary", "name" => "login-button"]);
        echo '</div>';
        ActiveForm::end();
        $AllSumma = 0;
        $AllVes = 0;
        echo "<thead><tr><th>Дата </th><th>Сумма</th><th>Оплата</th><th>Дії</th></tr></thead>";
        foreach ($zorderList as $data) {
            echo "<tr><td>" . $data['package_date'] . "</td>";
            $summa = $data['SMI'] / 100;
            echo "<td>" . $summa . "</td>";
            if ($data['NM'] == "??????") {
                echo "<td>Картка</td>";
            } else {
                echo "<td>Готівка</td>";
            }


            echo "<td>" . Html::a('Коригувати', ['cash/workcheck/upcheck', 'id' => $data['id']], ['class' => 'btn btn-primary']) . "</td>";
        }
        echo "<tr><td>Всього</td><td>" . $AllSumma . "</td><td>" . $AllVes . "</td></tr>";
        ?>
    </table>
</div>
