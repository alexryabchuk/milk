<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Касові апарати: Статус передачі';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <thead>
        <tr>
            <th>№ касового</th>
            <th>Назва</th>
            <th>Дата останньої передачі</th>
            <th>№ чека</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($statusList as $statusItem) : ?>
            <tr
                <?php
                $date1 = new DateTime(date("Y-m-d H:i:s"));
                $date2 = new DateTime($statusItem['last_date']);
                $interval = date_diff($date1, $date2);
                $res = (int)$interval->format('%R%a');
                if ($res < -1) {
                    echo 'style="background:red"';
                } else {
                    echo 'style="background:#A9F9AE"';
                }
                ?>
            >
                <td>
                    <?= $statusItem['cash_id'] ?>
                </td>
                <td>
                    <?php if (isset($cashList[$statusItem['cash_id']])) : ?>
                        <?= $cashList[$statusItem['cash_id']] ?>
                    <?php else: ?>
                        Невідомий
                    <?php endif ?>
                </td>
                <td>
                    <?= yii::$app->formatter->asDatetime($statusItem['last_date']) ?>
                    <?php
                    if ($res < -1) {
                        echo ' ' . $res . ' днів';
                    }
                    ?>
                </td>
                <td>
                    <?= $statusItem['last_di'] ?>
                </td>

            </tr>
        <?php endforeach ?>
        <tr>
            <td colspan="4">Всього: <?= count($statusList) ?> апаратів</td>
        </tr>
        </tbody>
    </table>
</div>
