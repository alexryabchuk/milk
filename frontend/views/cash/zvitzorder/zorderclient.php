<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use components\widgets\SelectPeriodCash;

$this->title = 'Звіт: Грошові кошти загальний';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <table class="table table-striped table-condensed">
        <?php
        echo SelectPeriodCash::widget(['model' => $model]);
        echo "<thead><tr><th rowspan='2'>Дата</th><th rowspan='2'>№ пакету</th><th colspan='3' class='text-center'>Надходження</th><th colspan='3' class='text-center'>Видача</th></tr>";
        echo "<tr><th class='text-right'>Готівка</th><th class='text-right'>Картка</th><th class='text-right'>Всього</th><th class='text-right'>Готівка</th><th class='text-right'>Картка</th><th class='text-right'>Всього</th></tr></thead>";
        foreach ($zvtdata as $id => $data) {
            echo "<tr><td>" . yii::$app->formatter->asDateTime($id) . "</td>";
            echo "<td>" . $data['di'] . "</td>";
            echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['smi0']) . "</td>";
            echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['smi1']) . "</td>";
            $smi = $data['smi0'] + $data['smi1'];
            echo "<td class='text-right bg-success'>" . yii::$app->formatter->asCurrency($smi) . "</td>";
            echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['smo0']) . "</td>";
            echo "<td class='text-right'>" . yii::$app->formatter->asCurrency($data['smo1']) . "</td>";
            $smo = $data['smo0'] + $data['smo1'];
            echo "<td class='text-right bg-success '>" . yii::$app->formatter->asCurrency($smo) . "</td>";
            $h = date('H', strtotime($id));
            $h = (int)$h;
            if ($h < 12) {
                echo "<td>" . Html::a('Коригувати', ['cash/zvitzorder/upcheck', 'di' => $data['di'], 'cash' => $model->clientid], ['class' => 'btn-xs btn-success']) . "</td>";
            } else {
                echo "<td>" . Html::a('Коригувати', ['cash/zvitzorder/upcheck', 'di' => $data['di'], 'cash' => $model->clientid], ['class' => 'btn-xs btn-primary']) . "</td>";
            }

        }
        if (!empty($summa)) {
            $smi = $summa['smi0'] + $summa['smi1'];
            $smo = $summa['smo0'] + $summa['smo1'];
            echo "<tr>"
                . "<td colspan='2'><strong>Всього</strong></td>"
                . "<td class='text-right'><strong>" . yii::$app->formatter->asCurrency($summa['smi0']) . "</strong></td>"
                . "<td class='text-right'><strong>" . yii::$app->formatter->asCurrency($summa['smi1']) . "</strong></td>"
                . "<td class='text-right'><strong>" . yii::$app->formatter->asCurrency($smi) . "</strong></td>"
                . "<td class='text-right'><strong>" . yii::$app->formatter->asCurrency($summa['smo0']) . "</strong></td>"
                . "<td class='text-right'><strong>" . yii::$app->formatter->asCurrency($summa['smo1']) . "</strong></td>"
                . "<td class='text-right'><strong>" . yii::$app->formatter->asCurrency($smo) . "</strong></td></tr>";

        }
        ?>
    </table>
</div>
