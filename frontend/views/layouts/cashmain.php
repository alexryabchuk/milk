<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'ТОВ "Літинський молзавод"',
        'brandUrl' => yii\helpers\Url::to(['/cash/main']),
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Касові апарати',
            'items' => [
                ['label' => 'Стан передачі', 'url' => ['/cash/workcheck/status']],
                ['label' => 'Пропущені пакети', 'url' => ['/cash/workcheck/lostpackages']],

            ],
        ],
        ['label' => 'Звіт по продукції',
            'items' => [
                ['label' => 'Продукція загальний', 'url' => ['/cash/zvitcash/cashall']],
                ['label' => 'Продукція по контрагенту', 'url' => ['/cash/zvitcash/cashclient']],
                ['label' => 'Продукція і z-звіти', 'url' => ['/cash/zvitcash/zorder']],
            ],
        ],
        ['label' => 'Звіт по чекам',
            'items' => [
                ['label' => 'Оплата по контрагенту', 'url' => ['/cash/zvitcheck/paycheck']],
                ['label' => 'Продукція по контрагенту', 'url' => ['/cash/zvitcash/payproduct']],
            ],
        ],
        ['label' => 'Звіт по коштах',
            'items' => [
                ['label' => 'Звіт загальний по днях', 'url' => ['/cash/zvitzorder/dayall']],
                ['label' => 'Звіт загальний по контрагентах', 'url' => ['/cash/zvitzorder/clientall']],
                ['label' => 'Звіт по контрагенту(Коригування z-звітів)', 'url' => ['/cash/zvitzorder/client']],
            ],
        ],
        ['label' => 'Внесення/видача',
            'items' => [
                ['label' => 'Звіт загальний внесення по днях', 'url' => ['/cash/zvitlending/making']],
                ['label' => 'Звіт загальний внесення по контрагентах', 'url' => ['/cash/zvitlending/makingclient']],
                ['label' => 'Звіт загальний видача по днях', 'url' => ['/cash/zvitlending/lending']],
                ['label' => 'Звіт загальний видача по контрагентах', 'url' => ['/cash/zvitlending/lendingclient']],

            ],
        ],

    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Реєстрація', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Вхід', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Вихід (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
