<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\widgets\LinkPager;

$this->title = 'Портал Літинського мочного заводу';
?>
<div class="site-index">
    <?php
    if ($profile) {
        echo "<div class='row'><div class='col-lg-6'><p>Прізвище: " . $profile->lastname . "</p></div></div>";
        echo "<div class='row'><div class='col-lg-6'><p>Ім’я: " . $profile->firstname . "</p></div></div>";
        echo "<div class='row'><div class='col-lg-6'><p>По батькові: " . $profile->parentname . "</p></div></div>";
        echo "<div class='row'><div class='col-lg-6'><p>Ід.код: " . $profile->usercode . "</p></div></div>";
    }
    ?>
</div>
