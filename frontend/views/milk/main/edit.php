<?php
//        

//        
//        var tagsA = $("div[id^='bas']");
//        var price = 0;
//        $(tagsA).each(function (){
//        var tmpText = $(this).text();
//        price=price + parseInt(tmpText);
//        $("#sum2").text(price);
//        });
//alert(s3);
//

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;

$this->title = 'Портал Літинського мочного заводу';
?>
<script type="text/javascript">
    $(document).ready(function () {
        var tagsA = $(":text[id$='natur']");
        var price = 0;
        $(tagsA).each(function () {
            var tmpText = $(this).val();
            price = price + parseInt(tmpText);
            $("#sum1").text(price);
        });

        var tagsA = $(":text[id^='bas']");
        var price = 0;
        $(tagsA).each(function () {
            var tmpText = $(this).val();
            price = price + parseInt(tmpText);
            $("#sum2").text(price);
        });
        $(":text").change(function () {
            var str = $(this).attr('id');
            var st1 = str.substring(14);
            var n = 0;
            n = st1.indexOf('-');
            st1 = st1.substring(0, n);
            var s1 = $("#statementitem-" + st1 + "-zhir").val();
            var s2 = $("#statementitem-" + st1 + "-natur").val();
            var s3 = Math.round(s1 * s2 / 3.4);
            $("#bas" + st1).val(s3);

            var tagsA = $(":text[id$='natur']");
            var price = 0;
            $(tagsA).each(function () {
                var tmpText = $(this).val();
                price = price + parseInt(tmpText);
                $("#sum1").text(price);
            });

            var tagsA = $(":text[id^='bas']");
            var price = 0;
            $(tagsA).each(function () {
                var tmpText = $(this).val();
                price = price + parseInt(tmpText);
                $("#sum2").text(price);
            });

        })
    });

</script>
<div class="site-index">
    <?php
    $form = ActiveForm::begin(['id' => 'Statementitem-edit-form']);
    echo Html::submitButton("Зберегти", ["class" => "btn btn-primary", "name" => "Statementitem-edit-button"]);
    ?>
    <table class="table table-striped table-condensed">
        <thead>
        <tr>
            <th class="col-lg-4">Прізвище, імя, по батькові</th>
            <th class="col-lg-2">Ід.код</th>
            <th class="col-lg-2">Натуральне</th>
            <th class="col-lg-2">Жир</th>
            <th class="col-lg-2">Базисне</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($statementItem as $index => $Item) {
            echo "<tr>";
            echo "<td>" . $Item->fullname . "</td>";
            echo "<td>" . $Item->icode . "</td>";
            echo "<td>" . $form->field($Item, "[$index]natur", ['template' => '<div class="col-lg-12">{input}</div><div class="col-lg-12">{error}</div>'])->label(FALSE) . "</td>";;
            echo "<td>" . $form->field($Item, "[$index]zhir", ['template' => '<div class="col-lg-12">{input}</div><div class="col-lg-12">{error}</div>'])->label(FALSE) . "</td>";;
            //echo "<td id='bas".$index."'>".round($Item->zhir*$Item->natur/3.4)."</td>";
            echo "<td>" . Html::input('text', "basis", round($Item->zhir * $Item->natur / 3.4), ['class' => 'form-control', 'id' => 'bas' . $index, "readonly" => true]) . "</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2">Всього</td>
            <td id="sum1"></td>
            <td></td>
            <td id="sum2"></td>
        </tr>
        </tfoot>
    </table>
    <?php ActiveForm::end(); ?>
</div>
