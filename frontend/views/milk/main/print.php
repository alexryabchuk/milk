<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\widgets\LinkPager;

$this->title = 'Портал Літинського мочного заводу';
?>
<div class="site-index">
    <table class="table table-striped table-condensed">
        <thead>
        <tr>
            <th>Прізвище, імя, по батькові</th>
            <th>Ід.код</th>
            <th>Натуральне</th>
            <th>Жир</th>
            <th>Базисне</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($statementItem as $Item) {
            echo "<tr>";
            echo "<td>" . $Item->fullname . "</td>";
            echo "<td>" . $Item->icode . "</td>";
            echo "<td>" . $Item->natur . "</td>";
            echo "<td>" . $Item->zhir . "</td>";
            echo "<td>" . round($Item->zhir * $Item->natur / 3.4) . "</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
        <tfoot>
        <?php
        echo "<tr>";
        echo "<td>Всього</td>";
        echo "<td></td>";
        echo "<td>" . $statement->natur2 . "</td>";
        echo "<td></td>";
        echo "<td>" . $statement->basis2 . "</td>";
        echo "</tr>";
        ?>
        </tfoot>
    </table>
</div>
