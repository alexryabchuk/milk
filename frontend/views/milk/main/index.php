<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\widgets\LinkPager;

$this->title = 'Портал Літинського мочного заводу';
?>
    <div class="site-index">
        <table class="table table-striped table-condensed">
            <thead>
            <tr>
                <th>№</th>
                <th>Період</th>
                <th>Населений пункт</th>
                <th>Натур</th>
                <th>Базис</th>
                <th>Натур</th>
                <th>Базис</th>
                <th>Дії</th>

            </tr>
            </thead>
            ";
            <?php
            foreach ($statementList as $statementItem) {
                if ($statementItem->status == 0)
                    echo "<tr style='color:black'>";
                if ($statementItem->status == 1)
                    echo "<tr style='color:blue'>";
                if ($statementItem->status == 2)
                    echo "<tr style='color:blue;font-weight: bold'>";
                echo "<td>" . $statementItem->nomerdok . "</td>";
                echo "<td>" . $statementItem->period . "</td>";
                echo "<td>" . $statementItem->naspunct . "</td>";
                echo "<td>" . $statementItem->natur1 . "</td>";
                echo "<td>" . $statementItem->basis1 . "</td>";
                echo "<td>" . $statementItem->natur2 . "</td>";
                echo "<td>" . $statementItem->basis2 . "</td>";
                echo "<td>";
                if ($statementItem->status == 0)
                    echo Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', yii\helpers\Url::to(['/milk/main/edit', 'id' => $statementItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Редагувати'));
                echo Html::a('<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>', yii\helpers\Url::to(['/milk/main/view', 'id' => $statementItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Переглянути'));
                if ($statementItem->status == 0)
                    echo Html::a('<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>', yii\helpers\Url::to(['/milk/main/send', 'id' => $statementItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Відіслати'));
                echo Html::a('<span class="glyphicon glyphicon-print" aria-hidden="true"></span>', yii\helpers\Url::to(['/milk/main/print', 'id' => $statementItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Друкувати'));
                echo "</td>";
                echo "</tr>";
            }
            ?>
        </table>
    </div>
<?php
echo LinkPager::widget([
    'pagination' => $pages,
]);
?>