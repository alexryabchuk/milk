<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use common\models\Bscountry;
use common\models\Bsproductlist1;
use backend\models\Bsparam;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cпецифікація №:' . $spec->num;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bsproductlist1-index">

    <h3>Cпецифікація №:<?= Html::encode($spec->num) ?></h3>

    <h3>Дата <?= yii::$app->formatter->asdate($spec->specdate) ?> року</h3>
    <h3>До накладної №____ від ___ _____________ 20___ року</h3>
    <h3>Посвідчення про якість _______________________ </h3>
    <h3>Назва продукції: <?= $spec->p->productname ?></h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th rowspan="2">
                Дата виготовлення
            </th>
            <th rowspan="2">
                № варки
            </th>
            <th rowspan="2">
                № місця
            </th>
            <th colspan="2">
                Кількість, шт
            </th>
            <th colspan="2">
                Маса, кг
            </th>
            <th rowspan="2">
                Сорт
            </th>
        </tr>
        <tr>
            <th>
                Ящиків
            </th>
            <th>
                Головок
            </th>
            <th>
                Нетто
            </th>
            <th>
                Брутто
            </th>
        </tr>
        </thead>
        <tbody>
        <?php $mn = 0;
        $mb = 0;
        $cy = 0;
        $cg = 0 ?>
        <?php foreach ($specItem as $item): ?>
            <?php
            $mn = $mn + $item['massanetto'];
            $mb = $mb + $item['massanetto'] + $spec->p->mtara;
            $cy = $cy + 1;
            $cg = $cg + $item['countHeads'];
            ?>
            <tr>
                <td>
                    <?= yii::$app->formatter->asDate($spec->madedate) ?>
                </td>
                <td>
                    <?= $spec->varka ?>
                </td>
                <td>
                    <?= $item['yasch'] ?>
                </td>
                <td> 1</td>
                <td>
                    <?= $item['countHeads'] ?>
                </td>
                <td>
                    <?= $item['massanetto'] / 1000 ?>
                </td>
                <td>
                    <?= ($item['massanetto'] + $spec->p->mtara) / 1000 ?>
                </td>
                <td>

                </td>
            </tr>
        <?php endforeach; ?>
        <tfoot>
        <tr>
            <td>
                Всього
            </td>
            <td>

            </td>
            <td>
                <?= $cy ?>
            </td>
            <td> 1</td>
            <td>
                <?= $cg ?>
            </td>
            <td>
                <?= $mn / 1000 ?>
            </td>
            <td>
                <?= $mb / 1000 ?>
            </td>
            <td>

            </td>
        </tr>
        </tbody>
    </table>
</div>