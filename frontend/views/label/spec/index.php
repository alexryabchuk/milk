<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use common\models\Bscountry;
use common\models\Bsproductlist1;
use backend\models\Bsparam;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список специфікацій';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bsproductlist1-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if ($model->used == 0) {
                return ['style' => 'color:red'];
            }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'num',
            'specdate',
            [
                'attribute' => 'pid',
                'value' => function ($model) {
                    $countryList = Bsproductlist1::getProductList();
                    return $countryList[$model->pid];
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'ceh_id',
                'value' => function ($model) {
                    $countryList = Bsparam::getPidrozdil();
                    return $countryList[$model->ceh_id];
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'country_id',
                'value' => function ($model) {
                    $countryList = Bscountry::getCountryList();
                    return $countryList[$model->country_id];
                },
                'format' => 'raw',
//                        'filter' => Select2::widget([
//                                'name' => 'country_id',
//                                'value' => $searchModel->country_id,
//                                'data' => Bscountry::getCountryList(),
//                                'hideSearch' => true,
//                                'pluginOptions' => [
//                                    'escapeMarkup' => new JsExpression("function(m) { return m; }"),
//                                    'allowClear' => true
//                                ],
//                        ]),
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>
</div>