<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Bsproductlist1 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Список продукції', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bsproductlist1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'productname',
            'producttype',
            'barscale1',
            'barscale2',
            'term1',
            'term2',
            'countg',
            'mtara',
            'usush',
            'et1',
            'et2',
            'used',
            'minmassa',
            'maxmassa',
            'code1C',
            'country_id',
            'param1',
            'param2',
            'param3',
            'param4',
            'param5',
            'param6',
        ],
    ]) ?>

</div>