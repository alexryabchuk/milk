<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\Select2;
use common\models\Bscountry;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список продукції';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bsproductlist1-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            if ($model->used == 0) {
                return ['style' => 'color:red'];
            }

        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'productname',
            'producttype',
            'barscale1',
            [
                'attribute' => 'country_id',
                'value' => function ($model) {
                    $countryList = Bscountry::getCountryList();
                    return $countryList[$model->country_id];
                },
                'format' => 'raw',
                'filter' => Select2::widget([
                    'name' => 'country_id',
                    'value' => $searchModel->country_id,
                    'data' => Bscountry::getCountryList(),
                    'hideSearch' => true,
                    'pluginOptions' => [
                        'escapeMarkup' => new JsExpression("function(m) { return m; }"),
                        'allowClear' => true
                    ],
                ]),
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {label1} {label2}',
                'buttons' => [
                    'label1' => function ($model, $key, $index) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-picture">',
                            Url::to(['label/label/pic1', 'id' => $key->id])
                        );
                    },
                    'label2' => function ($model, $key, $index) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-picture">',
                            Url::to(['label/label/pic2', 'id' => $key->id])
                        );
                    }
                ]
            ],
        ],
    ]); ?>
</div>