<?php

/* @var $this yii\web\View */
/* @var $model common\models\DovNaspunct */

$this->title = 'Створити населений пункт';
$this->params['breadcrumbs'][] = ['label' => 'Довідник населених пунктів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dov-naspunct-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
