<?php

use common\models\DovRegion;
use yii\helpers\Url;
use common\models\DovOblast;

return [
    ['class' => 'yii\grid\SerialColumn'],

    ['attribute' => 'code',
        'filter' => false,
    ],
    [
        'attribute' => 'oblast_code',
        'value' => function ($data) {
            return DovOblast::getOblastByCode($data->oblast_code);
        },
        'filter' => DovOblast::getOblastByCode(),
    ],
    [
        'attribute' => 'region_code',
        'value' => function ($data) {
            return DovRegion::getRegionByCode(0, $data->region_code);
        },
        'filter' => false,

    ],

    'name_naspunct',

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'width' => '120px',
        'template' => '{view}{update}{delete}',
        'viewOptions' => ['label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', 'role' => 'modal-remote'],
        'updateOptions' => ['label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', 'role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверенны?',
            'data-confirm-message' => 'Вы действительно хотите удалить запись'],
    ],

];