<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\DovOblast;
use common\models\DovRegion;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model common\models\DovNaspunct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dov-naspunct-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'oblast_code')->dropDownList(DovOblast::getOblastByCode(),
        [
            'prompt' => 'Виберіть область...',
            'onchange' => '
                   $.post(
                    "' . Url::toRoute('consignment/dicnaspunct/regionlist') . '",
                    {oblast_id : $(this).val()},
                    function(data){
                        $("select#regions").html(data).attr("disabled", false)
                    }
                   )
                '
        ]) ?>

    <?= $form->field($model, 'region_code')->dropDownList(DovRegion::getRegionByCode($model->oblast_code),
        [
            'prompt' => 'Выбрать регион...',
            'id' => 'regions',
            'disabled' => $model->isNewRecord ? 'disabled' : false
        ]) ?>
    <?= $form->field($model, 'name_naspunct')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
    <?php ActiveForm::end(); ?>

</div>
