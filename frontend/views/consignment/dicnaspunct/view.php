<?php

use common\models\DovOblast;
use common\models\DovRegion;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DovNaspunct */

$this->title = 'Update Dov Naspunct: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Dov Naspuncts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dov-naspunct-update">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'attribute' => 'oblast_code',
                'value' => function ($data) {
                    return DovOblast::getOblastByCode($data->oblast_code);
                },
            ],
            [
                'attribute' => 'region_code',
                'value' => function ($data) {
                    return DovRegion::getRegionByCode(0, $data->region_code);
                },
            ],
            'name_naspunct',
        ],
    ]) ?>

</div>
