<?php

/* @var $this yii\web\View */
/* @var $model common\models\DovNaspunct */

$this->title = 'Змінити населений пункт: ' . $model->name_naspunct;
$this->params['breadcrumbs'][] = ['label' => 'Довідник населених пунктів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_naspunct, 'url' => ['view', 'id' => $model->code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dov-naspunct-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
