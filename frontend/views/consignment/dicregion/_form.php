<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\DovOblast;
use kartik\widgets\Select2;


/* @var $this yii\web\View */
/* @var $model common\models\DovRegion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dov-region-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'oblast_code')->widget(Select2::classname(), [
        'data' => DovOblast::getOblastByCode(),
        'options' => ['placeholder' => 'Виберіть область ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'name_region')->textInput(['maxlength' => true]) ?>

    <?php ActiveForm::end(); ?>

</div>
