<?php

/* @var $this yii\web\View */
/* @var $model common\models\DovRegion */

$this->title = 'Змінити район:' . $model->name_region;
$this->params['breadcrumbs'][] = ['label' => 'Довідник районів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_region, 'url' => ['view', 'id' => $model->code]];
$this->params['breadcrumbs'][] = 'Змінити';
?>
<div class="dov-region-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
