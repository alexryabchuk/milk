<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\DovOblast;

/* @var $this yii\web\View */
/* @var $model common\models\DovRegion */

$this->title = $model->name_region;
$this->params['breadcrumbs'][] = ['label' => 'Довідник районів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dov-region-view">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            [
                'attribute' => 'oblast_code',
                'value' => function ($data) {
                    return DovOblast::getOblastByCode($data->oblast_code);
                },
            ],
            'name_region',
        ],
    ]) ?>

</div>
