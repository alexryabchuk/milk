<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\DovRegion */

$this->title = 'Створити район';
$this->params['breadcrumbs'][] = ['label' => 'Довідник районів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dov-region-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
