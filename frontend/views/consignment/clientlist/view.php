<?php

use yii\widgets\DetailView;
use common\models\DovOblast;
use common\models\DovRegion;
use common\models\DovNaspunct;

/* @var $this yii\web\View */
/* @var $model frontend\models\consignment\Clientlist */
?>
<div class="clientlist-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'firm',
            'deleted',
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'address',
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'naspunct',
                'value' => function ($data) {
                    return $data->naspunct ? DovNaspunct::getNaspunctName($data->naspunct) : null;
                },
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'region',
                'value' => function ($data) {
                    return $data->region ? DovRegion::getRegionByCode($data->oblast, $data->region) : null;
                },
            ],
            [
                'class' => '\kartik\grid\DataColumn',
                'attribute' => 'oblast',
                'value' => function ($data) {
                    return $data->oblast ? DovOblast::getOblastByCode($data->oblast) : null;
                },
            ],
        ],
    ]) ?>

</div>