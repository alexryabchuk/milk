<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use common\models\DovNaspunct;


/* @var $this yii\web\View */
/* @var $model frontend\models\consignment\Clientlist */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .modal-dialog {
        margin: 2px auto;
        z-index: 1100 !important;
    }

    .select2-close-mask {
        z-index: 2099;
    }

    .select2-dropdown {
        z-index: 3051;
    }

    .select2-container {
        z-index: 99999;
    }
</style>


<div class="clientlist-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'firm')->dropDownList(['1' => 'Фірмова', '2' => 'Київ', '3' => 'Дальні рейси', '4' => 'Інші']) ?>

    <?= $form->field($model, 'naspunct')->widget(Select2::classname(), [
        'initValueText' => DovNaspunct::getNaspunctName($model->naspunct),

        'options' => ['placeholder' => 'Знайти населений пункт...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'language' => 'ru',
            'ajax' => [
                'url' => \yii\helpers\Url::to(['consignment/clientlist/citylist']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(naspunct) { return naspunct.text; }'),
            'templateSelection' => new JsExpression('function (naspunct) { return naspunct.text; }'),
        ],
    ]);
    ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
