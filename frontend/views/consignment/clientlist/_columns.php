<?php

use yii\helpers\Url;
use common\models\DovOblast;
use common\models\DovRegion;
use common\models\DovNaspunct;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'firm',
        'filter' => ['1' => 'Фірмова', '2' => 'Київ', '3' => 'Дальні рейси', '4' => 'Інші'],
        'value' => function ($data) {
            $arr = ['1' => 'Фірмова', '2' => 'Київ', '3' => 'Дальні рейси', '4' => 'Інші'];
            return $arr[$data->firm];
        },
        //  'filter'
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'deleted',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'address',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'naspunct',
        'value' => function ($data) {
            return $data->naspunct ? DovNaspunct::getNaspunctName($data->naspunct) : null;
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'region',
        'value' => function ($data) {
            return $data->region ? DovRegion::getRegionByCode($data->oblast, $data->region) : null;
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'oblast',
        'filter' => DovOblast::getOblastByCode(),
        'value' => function ($data) {
            return $data->oblast ? DovOblast::getOblastByCode($data->oblast) : null;
        },
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'width' => '120px',
        'template' => '{view}{update}{delete}',
        'viewOptions' => ['label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', 'role' => 'modal-remote'],
        'updateOptions' => ['label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', 'role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверенны?',
            'data-confirm-message' => 'Вы действительно хотите удалить запись'],
    ],

];