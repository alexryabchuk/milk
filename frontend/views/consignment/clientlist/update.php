<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\consignment\Clientlist */

$this->title = 'Редагування: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список клієнтів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clientlist-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
