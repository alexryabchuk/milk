<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\consignment\Clientlist */

?>
<div class="clientlist-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
