<?php

/* @var $this yii\web\View */

use frontend\models\consignment\Clientlist;
use frontend\models\consignment\Productlist2;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;

$this->title = 'Портал Літинського мочного заводу';
?>
<style>
    .form-group-sm select.form-control {
        height: 30px;
        line-height: 18px;
    }
</style>
<div class="site-index">
    <div>
        <?php
        $form = ActiveForm::begin(['id' => 'n-form']);
        echo "<div class='row'><div class='col-lg-1 col-lg-offset-11'>" . Html::submitButton("Зберегти", ["class" => "btn btn-primary", "name" => "save"]) . "</div></div>";
        echo "<div class='row'>";
        echo $form->field($model, 'client_id')->widget(Select2::classname(), [
            'data' => clientList::getClientList(),
            'options' => ['placeholder' => 'Виберіть клієнта ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        echo "</div>";
        echo "<div class='row'>";
        echo $form->field($model, 'ndate')->input('date');
        echo "</div>";
        echo "<div class='row'>";
        echo "<table class='table'><thead><tr><th class='col-lg-8'>Назва</th><th class='col-lg-2'>Кількість</th><th class='col-lg-2'>Сума</th></tr></thead>";
        echo "<tbody id='productlist'>";
        foreach ($invoiceItem as $index => $item) {
            echo "<tr>";

            echo "<div class='row'>";
            echo '<td class="col-lg-8 form-group form-group-sm">' . $form->field($item, "[$index]product_id")->dropDownList(productList2::getProduct(), ['disabled' => true]) . "</td>";
            echo '<td class="col-lg-2 form-group form-group-sm">' . $form->field($item, "[$index]col")->textInput(['disabled' => true]) . "</td>";
            echo '<td class="col-lg-2 form-group form-group-sm">' . $form->field($item, "[$index]cena") . "</td>";
            echo "</tr>";
        }

        echo "</tbody></table>";
        echo "</div>";
        ActiveForm::end();
        ?>
    </div>
</div>
