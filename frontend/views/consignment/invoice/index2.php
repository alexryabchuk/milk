<?php

foreach ($invoiceList as $invoiceItem) {

    if ((\Yii::$app->user->can('consignment_invoice_editlab')) && ($invoiceItem->closedlab == 0) && ($invoiceItem->closeguard == 0)) {
        echo Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/invoice/editlab', 'id' => $invoiceItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Редагувати'));
    }
    if (\Yii::$app->user->can('consignment_invoice_edit') && $invoiceItem->closedlab == 1 && $invoiceItem->closeguard == 0) {
        echo Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/invoice/edit', 'id' => $invoiceItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Редагувати'));
    }
    if (\Yii::$app->user->can('consignment_invoice_delete') && $invoiceItem->closedlab == 0 && $invoiceItem->closeguard == 0) {
        echo Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/invoice/delete', 'id' => $invoiceItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Видалити', 'onClick' => 'return confirm("Знищити?")'));
    }
    if (\Yii::$app->user->can('consignment_invoice_activate') && $invoiceItem->closedlab == 0 && $invoiceItem->closeguard == 0) {
        echo Html::a('<span class="glyphicon glyphicon-check" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/invoice/activate', 'id' => $invoiceItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Підтвердити', 'onClick' => 'return confirm("Підтвердити?")'));
    }
    if (\Yii::$app->user->can('consignment_invoice_deactivate') && $invoiceItem->closedlab == 1 && $invoiceItem->closeguard == 0) {
        echo Html::a('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/invoice/deactivate', 'id' => $invoiceItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Зняти підтвердження', 'onClick' => 'return confirm("Зняти підтвердження?")'));
    }
    if (\Yii::$app->user->can('consignment_invoice_activate_guard') && $invoiceItem->closedlab == 1 && $invoiceItem->closeguard == 0) {
        echo Html::a('<span class="glyphicon glyphicon-check" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/invoice/activateguard', 'id' => $invoiceItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Підтвердити', 'onClick' => 'return confirm("Підтвердити?")'));
    }
    if (\Yii::$app->user->can('consignment_invoice_deactivate_guard') && $invoiceItem->closeguard == 1) {
        echo Html::a('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/invoice/deactivateguard', 'id' => $invoiceItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Зняти підтвердження', 'onClick' => 'return confirm("Зняти підтвердження?")'));
    }
    echo Html::a('<span class="glyphicon glyphicon-print" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/invoice/view', 'id' => $invoiceItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Переглянути'));
    echo "</td>";
    echo "</tr>";
}
?>
