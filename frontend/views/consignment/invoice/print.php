<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Портал Літинського мочного заводу';
?>
<div class="site-index">
    <p>ПрАТ "Літинський молочний завод</p>
    <p><?= $clientList[$invoice->client_id] ?></p>
    <p>Прізвище І.П. відповідальної особи _______________________</p>
    <p>_______________________</p>
    <p>Підпис</p>
    <h1>Рекламація № <?= $invoice->nnum ?> від <?= date("d.m.y", strtotime($invoice->ndate)) ?> року</h1>
    <table class="table table-striped table-bordered">
        <thead>
        <tr class='info' style="text-align: center">
            <td>№п/п</td>
            <td>Найменування продукту</td>
            <td>Вага</td>
            <td>Дата виробництва</td>
            <td>Дата закінчення</td>
            <td>Промперебка</td>
            <td>Перепаковка</td>
            <td>Причина повернення</td>
            <td>№ якісного посвідчення</td>
        </tr>
        </thead>
        <tbody id='productlist'>
        <?php $cehlist = array('Маслоцех', '1 цех', '2 цех', '3 цех', 'цех СЗМ') ?>
        <?php foreach ($invoiceItem as $num => $pr): ?>
            <tr>
                <td><?= $num + 1 ?></td>
                <td><?= $productList[$pr['product_id']] ?></td>
                <td><?= $pr['col'] ?></td>
                <td><?= date("d.m.y", strtotime($pr['dozr'])) ?></td>
                <td><?= date("d.m.y", strtotime($pr['realiz'])) ?></td>
                <td><?= $pr['col1'] ?>кг<br><?= $cehlist[$pr['napravleno1']] ?></td>
                <td><?= $pr['col2'] ?>кг<br><?= $cehlist[$pr['napravleno2']] ?></td>
                <td><?= $pr['prichina'] ?></td>
                <td>№ <?= $pr['nnumn'] ?> Дата:<?= date("d.m.y", strtotime($pr['ndaten'])) ?></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>
