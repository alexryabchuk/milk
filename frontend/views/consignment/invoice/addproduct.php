<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$index = rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9);
$cehlist = array('Маслоцех', '1 цех', '2 цех', '3 цех', 'цех СЗМ');
$form = ActiveForm::begin(['id' => 'n-form']);
?>
<tr>
    <td class="col-lg-4">
        <div class="row">
            <div class="col-lg-12">
                <?= $form->field($item, "[$index]product_id")->dropDownList($productList) ?>
            </div>
        </div>
        <div class='row'>
            <div class="col-lg-2">
                <?= $form->field($item, "[$index]col") ?>
            </div>
            <div class="col-lg-5">
                <?= $form->field($item, "[$index]dozr")->input('date') ?>
            </div>
            <div class="col-lg-5">
                <?= $form->field($item, "[$index]realiz")->input('date') ?>
            </div>
        </div>
    </td>
    <td class="col-lg-1">
        <div class="row">
            <div class="form-group form-group-sm">
                <?= $form->field($item, "[$index]col1") ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group form-group-sm">
                <?= $form->field($item, "[$index]napravleno1")->dropDownList($cehlist) ?>
            </div>
        </div>
    </td>
    <td class="col-lg-1">
        <div class="row">
            <div class="form-group form-group-sm">
                <?= $form->field($item, "[$index]col2") ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group form-group-sm">
                <?= $form->field($item, "[$index]napravleno2")->dropDownList($cehlist) ?>
            </div>
        </div>
    </td>
    <td class="col-lg-3">
        <div class="row">
            <div class="form-group form-group-sm">
                <?= $form->field($item, "[$index]prichina")->textarea(['rows' => 4])->label('&nbsp') ?>
            </div>
        </div>
    </td>
    <td class="col-lg-2">
        <div class="row">
            <div class="form-group form-group-sm">
                <?= $form->field($item, "[$index]nnumn") ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group form-group-sm">
                <?= $form->field($item, "[$index]ndaten")->input('date') ?>
            </div>
        </div>
    </td>
    <td>
        <?= Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', "#", array('class' => 'btn btn-danger btn-xs', 'title' => 'Редагувати', 'onClick' => '$(this).parent().parent().remove()')) ?>
    </td>
</tr>
