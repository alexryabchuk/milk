<?php

/* @var $this yii\web\View */

use frontend\models\consignment\Clientlist;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;

$this->title = 'Портал Літинського мочного заводу';
?>
<div class="site-index">
    <div>
        <?php $form = ActiveForm::begin(['id' => 'n-form']); ?>
        <div class='row'>
            <div class='col-lg-6'>
                <?= $form->field($model, 'ndate')->input('date') ?>
            </div>
        </div>
        <div class='row'>
            <div class='col-lg-12'>
                <?= $form->field($model, 'client_id')->widget(Select2::classname(), [
                    'data' => clientList::getClientList(),
                    'options' => ['placeholder' => 'Виберіть клієнта ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
