<?php

use frontend\models\consignment\Clientlist;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    ['class' => 'yii\grid\SerialColumn'],


    ['attribute' => 'nnum',
        'filter' => false,
    ],
    ['attribute' => 'ndate',
        'value' => function ($data) {
            return Yii::$app->formatter->asDate($data->ndate, "dd.MM.yyyy");
        },
        'filter' => false,
    ],
    ['attribute' => 'client_id',
        'value' => function ($data) {
            return isset(Clientlist::getClientList()[$data->client_id]) ? Clientlist::getClientList()[$data->client_id] : 'Не визначений клієнт';
        },
        'filter' => false,
    ],

    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{editlab}{update}{delete}{activate}{deactivate}{activateguard}{deactivateguard}{view}',
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'width' => '120px',
        'visibleButtons' => [
            'editlab' => function ($model) {
                return ((\Yii::$app->user->can('consignment_invoice_editlab')) && ($model->closedlab == 0) && ($model->closeguard == 0));
            },
            'update' => function ($model) {
                return (\Yii::$app->user->can('consignment_invoice_edit') && $model->closedlab == 1 && $model->closeguard == 0);
            },
            'activate' => function ($model) {
                return (\Yii::$app->user->can('consignment_invoice_activate') && $model->closedlab == 0 && $model->closeguard == 0);
            },
            'deactivate' => function ($model) {
                return (\Yii::$app->user->can('consignment_invoice_deactivate') && $model->closedlab == 1 && $model->closeguard == 0);
            },
            'activateguard' => function ($model) {
                return (\Yii::$app->user->can('consignment_invoice_activate_guard') && $model->closedlab == 1 && $model->closeguard == 0);
            },
            'deactivateguard' => function ($model) {
                return (\Yii::$app->user->can('consignment_invoice_deactivate_guard') && $model->closeguard == 1);
            },
            'delete' => function ($model) {
                return (\Yii::$app->user->can('consignment_invoice_delete') && $model->closedlab == 0 && $model->closeguard == 0);
            },
        ],
        'buttons' => [
            'editlab' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', $url,
                    ['class' => 'btn btn-warning btn-xs', 'role' => 'modal-remote', 'data-toggle' => 'tooltip', 'title' => 'Редагувати']);
            },
            'activate' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-check" aria-hidden="true"></span>', $url,
                    ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote', 'data-toggle' => 'tooltip',
                        'title' => 'Підтвердити', 'data-confirm-title' => 'Підтвердження лабораторія', 'data-confirm-message' => "Підтвердити?"]);
            },
            'deactivate' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>', $url,
                    ['class' => 'btn btn-danger btn-xs', 'role' => 'modal-remote', 'data-toggle' => 'tooltip',
                        'title' => 'Зняти підтвердження', 'data-confirm-title' => 'Підтвердження лабораторія', 'data-confirm-message' => "Зняти підтвердження?"]);
            },
            'activateguard' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-check" aria-hidden="true"></span>', $url,
                    ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote', 'data-toggle' => 'tooltip',
                        'title' => 'Підтвердити', 'data-confirm-title' => 'Підтвердження охорона', 'data-confirm-message' => "Підтвердити?"]);
            },
            'deactivateguard' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-minus" aria-hidden="true"></span>', $url,
                    ['class' => 'btn btn-danger btn-xs', 'role' => 'modal-remote', 'data-toggle' => 'tooltip',
                        'title' => 'Зняти підтвердження', 'data-confirm-title' => 'Підтвердження охорона', 'data-confirm-message' => "Зняти підтвердження?"]);
            },

        ],
        'viewOptions' => ['label' => '<button class="btn btn-info btn-xs"><span class="glyphicon glyphicon-eye-open"></span></button>', 'target' => '_blank', 'data-toggle' => 'tooltip',],
        'updateOptions' => ['label' => '<button class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span></button>', 'role' => 'modal-remote', 'data-toggle' => 'tooltip', 'title' => 'Редагувати', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Видалити',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'label' => '<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверенны?',
            'data-confirm-message' => 'Вы действительно хотите удалить запись'],
    ],

];