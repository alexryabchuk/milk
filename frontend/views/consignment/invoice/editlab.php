<?php

/* @var $this yii\web\View */

use common\models\Productlist;
use frontend\models\consignment\Clientlist;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Портал Літинського мочного заводу';
?>
<style>
    .modal-dialog {
        width: 80%;
    }

    .form-control {
        padding: 1px 6px;
    }

    .form-group-sm select.form-control {
        height: 30px;
        line-height: 18px;
    }

    .row [class^='col-lg-'] {
        min-height: 1px;
        padding-left: 2px;
        padding-right: 2px;
    }
</style>
<script>

    function addproduct() {
        $.ajax({
            url: "<?php echo yii\helpers\Url::to(['/consignment/invoice/addproduct'])?>",
            type: "GET",
            data: {},
            success: function (data) {
                $("#productlist").append(data.td);
            }
        });
    }
</script>
<div class="invoice-editlab">
    <?php var_dump(Yii::$app->request->post()); ?>
    <div>
        <?php
        $cehlist = array('Маслоцех', '1 цех', '2 цех', '3 цех', 'цех СЗМ');
        ?>
        <?php $form = ActiveForm::begin(['id' => 'n-form']); ?>
        <div class='row'>
            <div class='col-lg-6'>
                <?= $form->field($model, 'client_id')->dropDownList(clientList::getClientList(), ['disabled' => true]) ?>
            </div>
            <div class='col-lg-6'>
                <?= $form->field($model, 'ndate')->input('date', ['readOnly' => true]) ?>
            </div>
        </div>
        <div class='row'>
            <table class='table'>
                <thead>
                <tr>
                    <th class='col-lg-5'>Назва</th>
                    <th class='col-lg-1'>Промпереробка</th>
                    <th class='col-lg-1'>Перепаковка</th>
                    <th class='col-lg-3'>Причина повернення</th>
                    <th class='col-lg-2'>Накладна</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id='productlist'>
                <?php foreach ($invoiceItem as $index => $item): ?>
                    <tr>
                        <td class="col-lg-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($item, "[$index]product_id")->dropDownList(\frontend\models\consignment\Productlist2::getProduct()) ?>
                                </div>
                            </div>
                            <div class='row'>
                                <div class="col-lg-2">
                                    <?= $form->field($item, "[$index]col") ?>
                                </div>
                                <div class="col-lg-5">
                                    <?= $form->field($item, "[$index]dozr")->input('date') ?>
                                </div>
                                <div class="col-lg-5">
                                    <?= $form->field($item, "[$index]realiz")->input('date') ?>
                                </div>
                            </div>
                        </td>
                        <td class="col-lg-1">
                            <div class="row">
                                <div class="form-group form-group-sm">
                                    <?= $form->field($item, "[$index]col1") ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group form-group-sm">
                                    <?= $form->field($item, "[$index]napravleno1")->dropDownList($cehlist) ?>
                                </div>
                            </div>
                        </td>
                        <td class="col-lg-1">
                            <div class="row">
                                <div class="form-group form-group-sm">
                                    <?= $form->field($item, "[$index]col2") ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group form-group-sm">
                                    <?= $form->field($item, "[$index]napravleno2")->dropDownList($cehlist) ?>
                                </div>
                            </div>
                        </td>
                        <td class="col-lg-3">
                            <div class="row">
                                <div class="form-group form-group-sm">
                                    <?= $form->field($item, "[$index]prichina")->textarea(['rows' => 4])->label('&nbsp') ?>
                                </div>
                            </div>
                        </td>
                        <td class="col-lg-2">
                            <div class="row">
                                <div class="form-group form-group-sm">
                                    <?= $form->field($item, "[$index]nnumn") ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group form-group-sm">
                                    <?= $form->field($item, "[$index]ndaten")->input('date') ?>
                                </div>
                            </div>
                        </td>
                        <td>
                            <?= Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', "#", array('class' => 'btn btn-danger btn-xs', 'title' => 'Редагувати', 'onClick' => '$(this).parent().parent().remove()')) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div>
            <?= Html::a('Додати продукт', "#", array('class' => 'btn btn-success', 'title' => 'Додати', 'onClick' => 'addproduct()')) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
