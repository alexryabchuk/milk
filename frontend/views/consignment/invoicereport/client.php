<?php
/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;

$this->title = 'Портал Літинського мочного заводу';
?>
<script>
    function changeCategory() {
        var category = $('#reportform-vidbir option:selected').val();
        if (category == 0) {
            $(".rep1_cat").show();
            $(".rep1_client").hide();
        }
        ;
        if (category == 1) {
            $(".rep1_cat").hide();
            $(".rep1_client").show();
        }
        ;
    }

</script>
<div class="site-index">
    <h1>Звіти: Повернення</h1>
    <div class="body-content">
        <?php $form = ActiveForm::begin(['options' => ['target' => '_blank']]); ?>
        <div class='row'>
            <div class='col-lg-1 col-lg-offset-11'>
                <?= Html::submitButton("Зформувати", ["class" => "btn btn-primary", "name" => "save"]) ?>
            </div>
        </div>
        <div class="row">
            <div class='col-lg-2'>
                <?= $form->field($model, 'startdate')->input('date'); ?>
            </div>
            <div class='col-lg-2'>
                <?= $form->field($model, 'enddate')->input('date'); ?>
            </div>
            <div class="col-lg-1">
                <label>Вибрати всіх</label>
                <br/>
                <?= $form->field($model, 'all')->checkbox(); ?>
            </div>

            <div class="col-lg-7 rep1_client">
                <?= $form->field($model, 'client')->widget(Select2::classname(), [
                    'data' => $clientList,
                    'options' => ['placeholder' => 'Виберіть клієнта ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
