<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Портал Літинського мочного заводу';
?>
<div class="site-index">
    <h1>Звіти: Повернення</h1>
    <div class="body-content">
        <?php $form = ActiveForm::begin(['options' => ['target' => '_blank']]); ?>
        <div class='row'>
            <div class='col-lg-1 col-lg-offset-11'>
                <?= Html::submitButton("Зформувати", ["class" => "btn btn-primary", "name" => "save"]) ?>
            </div>
        </div>
        <div class="row">
            <div class='col-lg-2'>
                <?= $form->field($model, 'startdate')->input('date'); ?>
            </div>
            <div class='col-lg-2'>
                <?= $form->field($model, 'enddate')->input('date'); ?>
            </div>
            <div class="col-lg-8 rep1_cat">
                <?= $form->field($model, "categoryList")->inline(true)->checkboxList($categoryList) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
