<h1> Звіт </h1>
<h2> про повернення продукції </h2>
<h2> з <?= date("d.m.y", strtotime($startdate)) ?> по <?= date("d.m.y", strtotime($enddate)) ?> року </h2>
<table cellpadding="0" cellspacing="0">
    <thead>
    <tr class="tabhead">
        <th> Продукт</th>
        <th> Повернено</th>
        <th> Пром переробка</th>
        <th> Перепаковано</th>
        <?php foreach ($catlist as $cat): ?>
            <th>
                <?= $Category_list[$cat] ?>
            </th>
        <?php endforeach ?>
    </tr>
    </thead>
    <?php $allsum = 0;
    $allsum1 = 0;
    $allsum2 = 0;
    foreach ($catlist as $cat) {
        $allsumcat[$cat] = 0;
    }
    ?>
    <tbody>
    <?php foreach ($Rep_data as $Client_name => $block): ?>
        <tr>
            <?php $countcat = count($catlist) + 4 ?>
            <td colspan="<?= $countcat ?>" class="categoryname">
                <?php if (isset($Category_product[$Client_name])) {
                    echo $Category_product[$Client_name];
                }
                ?>
            </td>
        </tr>

        <?php $sum = 0;
        $sum1 = 0;
        $sum2 = 0;
        foreach ($catlist as $cat) {
            $sumcat[$cat] = 0;
        }
        ?>


        <?php foreach ($block as $Product_name => $row): ?>

            <tr>
                <?php
                $sum = $sum + $row['allsum'];
                $sum1 = $sum1 + $row['sum1'];
                $sum2 = $sum2 + (float)$row['sum2']
                ?>

                <td> <?= $Product_name ?> </td>
                <td> <?= $row['allsum'] ?> </td>
                <td> <?= $row['sum1'] ?> </td>
                <td> <?= $row['sum2'] ?> </td>

                <?php foreach ($catlist as $cat): ?>
                    <td>
                        <?= $row['cat' . $cat] ?>
                        <?php $sumcat[$cat] = $sumcat[$cat] + $row['cat' . $cat] ?>
                    </td>
                <?php endforeach ?>

            </tr>

        <?php endforeach ?>
        <tr class="allsumma">
            <td>
                Всього:
            </td>
            <td>
                <?= $sum ?>
            </td>
            <td>
                <?= $sum1 ?>
            </td>
            <td>
                <?= $sum2 ?>
            </td>
            <?php foreach ($catlist as $cat): ?>
                <td>
                    <?= $sumcat[$cat] ?>
                </td>
            <?php endforeach ?>
        </tr>

        <?php $allsum = $allsum + $sum ?>
        <?php $allsum1 = $allsum1 + $sum1 ?>
        <?php $allsum2 = $allsum2 + $sum2 ?>
        <?php foreach ($catlist as $cat) {
            $allsumcat[$cat] = $allsumcat[$cat] + $sumcat[$cat];
        }
        ?>

    <?php endforeach ?>
    <tr class="allsumma">
        <td>
            Загалом:
        </td>
        <td>
            <?= $allsum ?>
        </td>
        <td>
            <?= $allsum1 ?>
        </td>
        <td>
            <?= $allsum2 ?>
        </td>
        <?php foreach ($catlist as $cat): ?>
            <td>
                <?= $allsumcat[$cat] ?>
            </td>
        <?php endforeach ?>
    </tr>
    </tbody>
</table>