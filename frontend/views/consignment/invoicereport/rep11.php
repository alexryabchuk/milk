<?php

use kartik\grid\GridView;
use kartik\helpers\Html;

$gridColumns = [
    [
        'attribute' => 'ndate',
        'width' => '36px',
        'value' => function ($model, $key, $index, $column) {
            return $key;
        },

    ],
    [
        'class' => 'kartik\grid\ExpandRowColumn',
        'width' => '50px',
        'value' => function ($model, $key, $index, $column) {
            return GridView::ROW_COLLAPSED;
        },
        'detail' => function ($model, $key, $index, $column) {
            return Yii::$app->controller->renderPartial('_expand-row-details', ['model' => $model]);
        },
        'headerOptions' => ['class' => 'kartik-sheet-style'],
        'expandOneOnly' => true
    ],
    [
        'class' => 'kartik\grid\ActionColumn',

        'urlCreator' => function ($action, $model, $key, $index) {
            return '#';
        },
        'viewOptions' => ['title' => 'This will launch the book details page. Disabled for this demo!', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['title' => 'This will launch the book update page. Disabled for this demo!', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['title' => 'This will launch the book delete action. Disabled for this demo!', 'data-toggle' => 'tooltip'],
        'headerOptions' => ['class' => 'kartik-sheet-style'],
    ],
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'headerOptions' => ['class' => 'kartik-sheet-style'],
    ],
];

echo GridView::widget([
    'id' => 'kv-grid-demo',
    'dataProvider' => $Rep_data,
    'columns' => $gridColumns, // check the configuration for grid columns by clicking button above
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => true, // pjax is set to always true for this demo
    // set your toolbar
    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    // set export properties
    'export' => [
        'fontAwesome' => true
    ],
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 10],
    'itemLabelSingle' => 'book',
    'itemLabelPlural' => 'books'
]);

?>


<h1>
    Звіт
</h1>
<h2>
    про повернення продукції
</h2>
<h2>
    з <?= date("d.m.y", strtotime($startdate)) ?> по <?= date("d.m.y", strtotime($enddate)) ?> року
</h2>
<table cellpadding="0" cellspacing="0">
    <tr>
        <th>
            Контрагент
        </th>
        <th>
            Продукт
        </th>
        <th>
            К-сть
        </th>
        <th>
            Дата виг.
        </th>
        <th>
            Дата зак. <br>терм.реаліз.
        </th>

        <th>
            Дата одерж.
        </th>
        <th>
            Причина повернення
        </th>

    </tr>

</table>