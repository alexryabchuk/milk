<h1>Звіт</h1>
<h2>про повернення продукції</h2>
<h2> з <?= date("d.m.y", strtotime($startdate)) ?> по <?= date("d.m.y", strtotime($enddate)) ?> року</h2>
<table cellpadding="0" cellspacing="0">
    <tr>
        <th>Контрагент</th>
        <th>Продукт</th>
        <th>К-сть</th>
        <th>Дата виг.</th>
        <th>Дата зак. <br>терм.реаліз.</th>
        <th>Дата одерж.</th>
        <th>Причина повернення</th>
    </tr>
    <?php foreach ($Rep_data as $ndate => $block): ?>
        <tr>
            <td class="categoryname" colspan="7"><?= date("d.m.Y", strtotime($ndate)) ?></td>
        </tr>
        <?php foreach ($block as $row): ?>
            <tr>
                <td><?= $row['clientname'] ?></td>
                <td><?= $row['name'] ?> </td>
                <td><?= $row['col'] ?> </td>
                <td><?= date("d.m.y", strtotime($row['dozr'])) ?></td>
                <td><?= date("d.m.y", strtotime($row['realiz'])) ?></td>
                <td><?= date("d.m.y", strtotime($row['ndaten'])) ?></td>
                <td><?= $row['prichina'] ?></td>
            </tr>
        <?php endforeach ?>

    <?php endforeach ?>
</table>