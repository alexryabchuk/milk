<?php


use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

$provider = new ArrayDataProvider([
    'allModels' => $model,
    'pagination' => false,
    'sort' => [
        'attributes' => ['id', 'name'],
    ],
]);
echo GridView::widget([
    'id' => 'kv-grid-demo',
    'dataProvider' => $provider,
    'columns' =>
        [
            ['attribute' => 'name'],
            ['attribute' => 'col',
                'pageSummary' => true],
        ],

    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => true, // pjax is set to always true for this demo
    // set your toolbar
    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
    // set export properties
    'export' => [
        'fontAwesome' => true
    ],
    'showPageSummary' => true,
    'persistResize' => false,
    'toggleDataOptions' => ['minCount' => 10],

]);

?>
