<h1> Звіт </h1>
<h2> про повернення продукції </h2>
<h2> з <?= date("d.m.y", strtotime($startdate)) ?> по <?= date("d.m.y", strtotime($enddate)) ?> року </h2>
<table cellpadding="0" cellspacing="0">
    <thead>
    <tr class="tabhead">
        <th> Категорія</th>
        <th> Повернено</th>
        <th> Пром переробка</th>
        <th> Перепаковано</th>
    </tr>
    </thead>
    <?php $allsum = 0;
    $allsum1 = 0;
    $allsum2 = 0;
    ?>
    <tbody>
    <?php foreach ($Rep_data as $day => $block): ?>
        <tr>

            <td colspan="4" class="categoryname">
                <?= $day ?>
            </td>
        </tr>

        <?php $sum = 0;
        $sum1 = 0;
        $sum2 = 0;
        ?>


        <?php foreach ($block as $categoryName => $row): ?>

            <tr>
                <?php
                $sum = $sum + $row['col'];
                $sum1 = $sum1 + $row['col1'];
                $sum2 = $sum2 + (float)$row['col2']
                ?>

                <td> <?= $CategoryList[$row['category']] ?> </td>
                <td> <?= $row['col'] ?> </td>
                <td> <?= $row['col1'] ?> </td>
                <td> <?= $row['col2'] ?> </td>

            </tr>

        <?php endforeach ?>
        <tr class="allsumma">
            <td>
                Всього:
            </td>
            <td>
                <?= $sum ?>
            </td>
            <td>
                <?= $sum1 ?>
            </td>
            <td>
                <?= $sum2 ?>
            </td>
        </tr>

        <?php $allsum = $allsum + $sum ?>
        <?php $allsum1 = $allsum1 + $sum1 ?>
        <?php $allsum2 = $allsum2 + $sum2 ?>
    <?php endforeach ?>
    <tr class="allsumma">
        <td>
            Загалом:
        </td>
        <td>
            <?= $allsum ?>
        </td>
        <td>
            <?= $allsum1 ?>
        </td>
        <td>
            <?= $allsum2 ?>
        </td>
    </tr>
    </tbody>
</table>