<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DovOblast */

$this->title = $model->name_oblast;
$this->params['breadcrumbs'][] = ['label' => 'Довідник областей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dov-oblast-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'code',
            'name_oblast',
        ],
    ]) ?>

</div>
