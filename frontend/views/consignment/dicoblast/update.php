<?php

/* @var $this yii\web\View */
/* @var $model common\models\DovOblast */

$this->title = 'Змінити область: ' . $model->name_oblast;
$this->params['breadcrumbs'][] = ['label' => 'Довідник областей', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_oblast, 'url' => ['view', 'id' => $model->code]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dov-oblast-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
