<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DovOblast */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dov-oblast-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name_oblast')->textInput(['maxlength' => true]) ?>
    <?php ActiveForm::end(); ?>

</div>
