<?php

/* @var $this yii\web\View */
/* @var $model common\models\DovOblast */

$this->title = 'Створити область';
$this->params['breadcrumbs'][] = ['label' => 'Довідник областей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dov-oblast-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
