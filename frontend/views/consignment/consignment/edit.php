<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Портал Літинського мочного заводу';
?>
<script>
    function addproduct() {
        $.ajax({
            url: "<?php echo yii\helpers\Url::to(['/consignment/invoice/addproduct'])?>",
            type: "GET",
            data: {},
            success: function (data) {
                $("#productlist").append(data.td);
            }
        });
    }
</script>
<div class="site-index">
    <div>
        <?php
        $cehlist = array('Маслоцех', '1 цех', '2 цех', '3 цех', 'цех СЗМ');
        $form = ActiveForm::begin(['id' => 'n-form']);
        echo "<div class='row'><div class='col-lg-1 col-lg-offset-11'>" . Html::submitButton("Зберегти", ["class" => "btn btn-primary", "name" => "save"]) . "</div></div>";
        echo "<div class='row'>";
        echo "<div class='col-lg-6'>";
        echo $form->field($consignment, 'client_id')->dropDownList($clientList);
        echo "</div>";
        echo "<div class='col-lg-3'>";
        echo $form->field($consignment, 'nnum');
        echo "</div>";
        echo "<div class='col-lg-3'>";
        echo $form->field($consignment, 'ndate')->input('date');
        echo "</div>";
        echo "</div>";
        echo "<div class='row'>";
        echo "<table class='table'><thead><tr><th class='col-lg-8'>Назва</th><th class='col-lg-2'>Залишок</th><th class='col-lg-2'>Кількістт</th></tr></thead>";
        echo "<tbody id='productlist'>";
        foreach ($consignmentItem as $index => $item) {
            echo "<tr>";
            echo '<td class="col-lg-8">';
            echo '<div class"row"><div class="form-group form-group-sm">' . $form->field($item, "[$index]product_id")->dropDownList($productList)->label(false) . "</div></div></td>";
            //echo '<td class="col-lg-2"><div class"row"><div class="form-group form-group-sm">'.$form->field($item, "[$index]ost")->input()->label(false)."</div></div></td>";
            echo '<td class="col-lg-2"><div class"row"><div class="form-group form-group-sm">' . $form->field($item, "[$index]col")->input()->label(false) . "</div></div></td>";
            echo '<td>' . Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', "#", array('class' => 'btn btn-success btn-xs', 'title' => 'Редагувати', 'onClick' => '$(this).parent().parent().remove()')) . '</td>';
            echo "</tr>";
        }
        echo "</table>";
        echo "</div>";
        echo '<div>' . Html::a('Додати продукт', "#", array('class' => 'btn btn-success', 'title' => 'Додати', 'onClick' => 'addproduct()')) . '</td>';
        ActiveForm::end();
        ?>
    </div>
</div>
