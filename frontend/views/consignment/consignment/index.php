<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\widgets\LinkPager;

$this->title = 'Портал Літинського мочного заводу';
?>

    <div class="site-index">
        <?php
        if (\Yii::$app->user->can('consignment_invoice_add')) {
            echo Html::a('Додати', yii\helpers\Url::to(['/consignment/consignment/add']), array('class' => 'btn btn-success', 'title' => 'Додати'));
        }
        ?>
        <table class="table table-striped table-condensed">
            <thead>
            <tr class='info' style="text-align: center">
                <td class="col-lg-1">№</td>
                <td class="col-lg-2">Дата</td>
                <td class="col-lg-8">Контрагент</td>
                <td class="col-lg-1">Дії</td>
            </tr>
            </thead>
            <?php
            foreach ($consignmentList as $consignmentItem) {
                if ($consignmentItem->closed == 1) {
                    echo '<tr style="font-weight: bold">';
                } else {
                    echo '<tr>';
                }
                echo "<td>" . $consignmentItem->nnum . "</td>";
                echo "<td>" . Yii::$app->formatter->asDate($consignmentItem->ndate, "dd.MM.yyyy") . "</td>";
                if (isset($clientList[$consignmentItem->client_id])) {
                    echo "<td>" . $clientList[$consignmentItem->client_id] . "</td>";
                } else {
                    echo "<td>Не визначений клієнт: Код " . $consignmentItem->client_id . "</td>";
                }
                echo "<td>";
                if (\Yii::$app->user->can('consignment_consignment_edit')) {
                    echo Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/consignment/edit', 'id' => $consignmentItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Редагувати'));
                }
                if (\Yii::$app->user->can('consignment_consignment_delete')) {
                    echo Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', yii\helpers\Url::to(['/consignment/consignment/delete', 'id' => $consignmentItem->id]), array('class' => 'btn btn-success btn-xs', 'title' => 'Видалити', 'onClick' => 'return confirm("Знищити?")'));
                }
                echo "</td>";
                echo "</tr>";
            }
            ?>
        </table>
    </div>
<?php
echo LinkPager::widget([
    'pagination' => $pages,
]);
?>