<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Портал Літинського мочного заводу';
?>
<script>
    function addproduct() {
        $.ajax({
            url: "<?php echo yii\helpers\Url::to(['/consignment/consignment/addproduct'])?>",
            type: "GET",
            data: {},
            success: function (data) {
                $("#productlist").append(data.td);
            }
        });
    }

    function getOstat(item1, item2) {
        product_id = item1.value;
        client_id = $("#nakladnalist2-client_id option:selected").val();
        $.ajax({
            url: "<?php echo yii\helpers\Url::to(['/consignment/consignment/ostat'])?>",
            type: "GET",
            data: {product: product_id, client: client_id},
            success: function (data) {
                item2.val(data.ost);
            }
        });
    }
</script>
<div class="site-index">
    <div>
        <?php
        $cehlist = array('Маслоцех', '1 цех', '2 цех', '3 цех', 'цех СЗМ');
        $form = ActiveForm::begin(['id' => 'n-form']);
        echo "<div class='row'><div class='col-lg-1 col-lg-offset-11'>" . Html::submitButton("Зберегти", ["class" => "btn btn-primary", "name" => "save"]) . "</div></div>";
        echo "<div class='row'>";
        echo "<div class='col-lg-6'>";
        echo $form->field($consignment, 'client_id')->dropDownList($clientList);
        echo "</div>";
        echo "<div class='col-lg-3'>";
        echo $form->field($consignment, 'nnum');
        echo "</div>";
        echo "<div class='col-lg-3'>";
        echo $form->field($consignment, 'ndate')->input('date');
        echo "</div>";
        echo "</div>";
        echo "<div class='row'>";
        echo "<table class='table'><thead><tr><th class='col-lg-8'>Назва</th><th class='col-lg-2'>Залишок</th><th class='col-lg-2'>Кількість</th><th></th></tr></thead>";
        echo "<tbody id='productlist'>";
        foreach ($consignmentItem as $index => $item) {
            echo "<tr>";
            echo '<td class="col-lg-8">' . $form->field($item, "[$index]product_id")->dropDownList($productList, ['onClick' => 'getOstat(this,$(this).parent().parent().parent().children(".ost1").children(".form-group").children(".ost"))'])->label(false) . "</td>";
            echo '<td class="col-lg-2 ost1">' . $form->field($item, "[$index]ost")->textInput(['disabled' => true, 'class' => 'form-control ost'])->label(false) . "</td>";
            echo '<td class="col-lg-2">' . $form->field($item, "[$index]col")->label(false) . "</td>";
            echo '<td>' . Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', "#", array('class' => 'btn btn-success', 'title' => 'Вилучити', 'onClick' => '$(this).parent().parent().remove()')) . '</td>';
            echo "</tr>";
        }
        echo "</table>";
        echo "</div>";
        echo '<div>' . Html::a('Додати продукт', "#", array('class' => 'btn btn-success', 'title' => 'Додати', 'onClick' => 'addproduct()')) . '</td>';
        ActiveForm::end();
        ?>
    </div>
</div>
