<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$index = rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9) . rand(1, 9);
$form = ActiveForm::begin(['id' => 'n-form']);
echo "<tr>";
echo '<td class="col-lg-8">' . $form->field($item, "[$index]product_id")->dropDownList($productList, ['onClick' => 'getOstat(this,$(this).parent().parent().parent().children(".ost1").children(".form-group").children(".ost"))'])->label(false) . "</td>";
echo '<td class="col-lg-2 ost1">' . $form->field($item, "[$index]ost")->textInput(['disabled' => true, 'class' => 'form-control ost'])->label(false) . "</td>";
echo '<td class="col-lg-2">' . $form->field($item, "[$index]col")->label(false) . "</td>";
echo '<td>' . Html::a('<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>', "#", array('class' => 'btn btn-success btn-xs', 'title' => 'Вилучити', 'onClick' => '$(this).parent().parent().remove()')) . '</td>';
echo "</tr>";
?>