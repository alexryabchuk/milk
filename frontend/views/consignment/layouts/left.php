<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;


$path = 'http://' . $_SERVER['SERVER_NAME'] . '/assets/images/users/avatar.jpg';


?>

<!-- START PAGE SIDEBAR -->
<div class="page-sidebar page-sidebar-fixed scroll">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation ">
        <li class="xn-logo">
            <a href="<?= Url::toRoute([Yii::$app->homeUrl]) ?>">Накладні</a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="<?= $path ?>" alt="John Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="<?= $path ?>" alt="John Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php if (!Yii::$app->user->isGuest) echo Yii::$app->user->identity->username ?></div>
                    <div class="profile-data-title"><?php if (!Yii::$app->user->isGuest) echo 'getCurrentRole()' ?></div>
                </div>
                <div class="profile-controls">
                    <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title' => 'Профиль', 'class' => 'profile-control-left']); ?>

                    <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title' => 'Профиль', 'class' => 'profile-control-right']); ?>
                </div>
            </div>
        </li>

        <li class="xn-title">Menu</li>
        <?= Yii::$app->controller->id ?>
        <li <?php if (Yii::$app->controller->id == 'consignment/main') {
            echo ' class = "active" ';
        } ?>>
            <?= Html::a('<span class="fa fa-dashboard"></span> <span class="xn-text">Показники</span>', ['consignment/main/index'], []); ?>
        </li>
        <li <?php if (Yii::$app->controller->id == 'consignment/invoice') {
            echo ' class = "active" ';
        } ?>>
            <?= Html::a('<span class="fa fa-dashboard"></span> <span class="xn-text">Накладні</span>', ['consignment/invoice/index'], []); ?>
        </li>
        <li class="xn-openable
                    <?php if (Yii::$app->controller->id == 'consignment/clientlist') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'consignment/dicoblast') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'consignment/dicregion') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'consignment/dicnaspunct') {
            echo " active";
        } ?>
                    "><?= Html::a('<span class="fa fa-wrench"></span> <span class="xn-text">Довідники</span>', ['#'], []); ?>
            <ul>
                <li <?php if (Yii::$app->controller->id == 'consignment/clientlist') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >Клієнти</span>', ['/consignment/clientlist/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'consignment/dicoblast') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-cutlery"></span> <span >Області</span>', ['/consignment/dicoblast/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'consignment/dicregion') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-ice-lolly-tasted"></span> <span >Райони</span>', ['/consignment/dicregion/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'consignment/dicnaspunct') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-ice-lolly-tasted"></span> <span >Населений пункт</span>', ['/consignment/dicnaspunct/index'], []); ?>
                </li>
            </ul>
        </li>
        <li class="xn-openable <?php if (Yii::$app->controller->id == 'consignment/invoicereport') {
            echo " active";
        } ?> ">
            <?= Html::a('<span class="fa fa-wrench"></span> <span class="xn-text">Звіти</span>', ['#'], []); ?>
            <ul>
                <li <?php if (Yii::$app->controller->action->id == 'client') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >По контрагентах</span>', ['/consignment/invoicereport/client'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->action->id == 'product') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-ice-lolly-tasted"></span> <span >По продукції</span>', ['/consignment/invoicereport/product'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->action->id == 'days') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >По днях</span>', ['/consignment/invoicereport/days'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->action->id == 'category') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >По категоріях</span>', ['/consignment/invoicereport/client'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->action->id == 'mounth') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >По місяцях</span>', ['/consignment/invoicereport/client'], []); ?>
                </li>
            </ul>
        </li>


    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->