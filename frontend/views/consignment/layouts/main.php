<?php

use frontend\assets\AppAsset;
use yii\helpers\Html;


AppAsset::register($this);


?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="">
<?php $this->beginBody() ?>
<div class="page-container">

    <?= $this->render('left.php', []) ?>
    <!-- PAGE CONTENT -->
    <div class="page-content">
        <?= $this->render('header.php', []) ?>
        <?= $this->render('content.php', ['content' => $content,]) ?>
    </div>
    <!-- END PAGE CONTENT -->
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
