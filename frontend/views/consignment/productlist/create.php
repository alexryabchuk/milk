<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\consignment\Productlist2 */

$this->title = 'Створення: Список продуктів';
$this->params['breadcrumbs'][] = ['label' => 'Список продуктів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productlist2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
