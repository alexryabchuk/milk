<?php

namespace backend\controllers;

use Yii;
use app\models\User;
use app\models\Usersearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use backend\models\UserPermision;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * UserController implements the CRUD actions for User model.
 */
class GrantController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//             
//                ],
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $Model = User::find();
        $countModel = clone $Model;
        $pages = new Pagination(['totalCount' => $countModel->count(), 'pageSize' => 5]);
        $userList = $Model->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $auth = Yii::$app->authManager;
//        $auth->removeAll();
//        $cash_zvitzorder_client = $auth->createPermission('p_cash_zvitzorder_client');
//        $cash_zvitzorder_client->description='Касові апарати: Звіт по коштах по контрагенту';
//        $auth->add($cash_zvitzorder_client);
//        $rcash_zvitzorder_client = $auth->createRole('cash_zvitzorder_client');
//        $rcash_zvitzorder_client->description='Касові апарати: Звіт по коштах по контрагенту';
//        $auth->add($rcash_zvitzorder_client);
//        $auth->addChild($rcash_zvitzorder_client,$cash_zvitzorder_client);
//        
//        $cash_workcheck_lostpackage = $auth->createPermission('p_cash_workcheck_lostpackage');
//        $cash_workcheck_lostpackage->description='Касові апарати: Список пропущених';
//        $auth->add($cash_workcheck_lostpackage);
//        $rcash_workcheck_lostpackage = $auth->createRole('cash_cash_workcheck_lostpackage');
//        $rcash_workcheck_lostpackage->description='Касові апарати: Список пропущених';
//        $auth->add($rcash_workcheck_lostpackage);
//        $auth->addChild($rcash_workcheck_lostpackage,$cash_workcheck_lostpackage);
//        
//        $cash_zvitcash_cashall = $auth->createPermission('p_cash_zvitcash_cashall');
//        $cash_zvitcash_cashall->description='Касові апарати: Звіт продукція загальний';
//        $auth->add($cash_zvitcash_cashall);
//        $rcash_zvitcash_cashall = $auth->createRole('cash_zvitcash_cashall');
//        $rcash_zvitcash_cashall->description='Касові апарати: Звіт продукція загальний';
//        $auth->add($rcash_zvitcash_cashall);
//        $auth->addChild($rcash_zvitcash_cashall,$cash_zvitcash_cashall);
//        
//        $cash_zvitcash_cashclient = $auth->createPermission('p_cash_zvitcash_cashclient');
//        $cash_zvitcash_cashclient->description='Касові апарати: Звіт продукція по контрагенту';
//        $auth->add($cash_zvitcash_cashclient);
//        $rcash_zvitcash_cashclient = $auth->createRole('cash_zvitcash_cashclient');
//        $rcash_zvitcash_cashclient->description='Касові апарати: Звіт продукція по контрагенту';
//        $auth->add($rcash_zvitcash_cashclient);
//        $auth->addChild($rcash_zvitcash_cashclient,$cash_zvitcash_cashclient);
//        
//        $cash_zvitcash_zorder = $auth->createPermission('p_cash_zvitcash_zorder');
//        $cash_zvitcash_zorder->description='Касові апарати: Звіт продукція і z-звіти';
//        $auth->add($cash_zvitcash_zorder);
//        $rcash_zvitcash_zorder = $auth->createRole('cash_zvitcash_zorder');
//        $rcash_zvitcash_zorder->description='Касові апарати: Звіт продукція і z-звіти';
//        $auth->add($rcash_zvitcash_zorder);
//        $auth->addChild($rcash_zvitcash_zorder,$cash_zvitcash_zorder);

//        $cash_zvitlending_making = $auth->createPermission('p_cash_zvitlending_lending');
//        $cash_zvitlending_making->description='Касові апарати: Службова видача коштів по контрагенту';
//        $auth->add($cash_zvitlending_making);
//        $rcash_zvitlending_making = $auth->createRole('cash_zvitlending_lending');
//        $rcash_zvitlending_making->description='Касові апарати: Службова видача коштів по контрагенту';
//        $auth->add($rcash_zvitlending_making);
//        $auth->addChild($rcash_zvitlending_making,$cash_zvitlending_making);
        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        return $this->render('index', [
            'userList' => $userList,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionEdit($id)
    {
        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');
        $user_permit = array_keys(Yii::$app->authManager->getRolesByUser($id));
        $user = $this->findUser($id);
        if (Yii::$app->request->post('roles')) {
            Yii::$app->authManager->revokeAll($user->getId());
            foreach (Yii::$app->request->post('roles') as $role) {
                $new_role = Yii::$app->authManager->getRole($role);
                Yii::$app->authManager->assign($new_role, $user->getId());
            }
            return $this->redirect(Url::to(["index"]));
        }
        return $this->render('edit', ['user' => $user, 'roles' => $roles, 'user_permit' => $user_permit]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $user = $this->findUser($id);
        Yii::$app->authManager->revokeAll($user->getId());
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function findUser($id)
    {

        $user = \common\models\User::findIdentity($id);
        if (empty($user)) {
            throw new NotFoundHttpException(Yii::t('db_rbac', 'Пользователь не найден'));
        } else {
            return $user;
        }
    }
}
