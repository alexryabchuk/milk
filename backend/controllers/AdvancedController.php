<?php

namespace backend\controllers;

use app\models\forms\Cleardata;
use components\widgets\SelectCash;
use frontend\models\Cashproductclient;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\SelectDate;
use yii\web\Response;
use common\models\Advanced;
use app\models\forms\SettingForm;


/**
 * Site controller
 */
class AdvancedController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'delidentical', 'alarmsetting', 'nameproductcash', 'cleardata', 'deletedata', 'priceproductcash', 'price-chart'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['searchidentical'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionSearchidentical()
    {
        $model = new SelectDate();
        return $this->render('searchidentical', ['model' => $model]);
    }


    /**
     * Login action.
     *
     * @return string
     */
    public function actionDeletedata()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $date = date("Y-m-d 0:0:0", Yii::$app->request->post('date') / 1000);
            $password = Yii::$app->request->post('password');
            if ($password == 'KlopM,./') {
                $rows0 = Yii::$app->db->createCommand('delete  FROM `check0_p` WHERE (`package_date` < "' . $date . '") ')->execute();
                $rows1 = Yii::$app->db->createCommand('delete  FROM `check0` WHERE (`package_date` < "' . $date . '") ')->execute();
                $rows2 = Yii::$app->db->createCommand('delete  FROM `check1` WHERE (`package_date` < "' . $date . '") ')->execute();
                $rows3 = Yii::$app->db->createCommand('delete  FROM `check_service` WHERE (`package_date` < "' . $date . '") ')->execute();
                $rows4 = Yii::$app->db->createCommand('delete  FROM `zorder` WHERE (`package_date` < "' . $date . '") ')->execute();

                $flag = 'Знищено чеків: ' . $rows1 . "<br>";
                $flag .= 'Знищено чеків повернення: ' . $rows2 . "<br>";
                $flag .= 'Знищено службових чеків: ' . $rows3 . "<br>";
                $flag .= 'Знищено Z-звітів: ' . $rows4 . "<br>";
            } else {
                $flag = 'Не вірний пароль';
            }

            return ['flag' => $flag];// $this->render('searchidentical',['model'=>$model]);
        }
    }

    public function actionDelidentical()
    {
        if (Yii::$app->request->isAjax) {
            //var_dump(Yii::$app->request->post('date')/1000);
            Yii::$app->response->format = Response::FORMAT_JSON;
            $date = Yii::$app->request->post('date') / 1000;
//            $date = strtotime('03.01.2019');
            $flag = Advanced::searchIdenticalCheckDate($date);
            return ['flag' => $flag];// $this->render('searchidentical',['model'=>$model]);
//        } else {
//            var_dump(Yii::$app->request->post('date')/1000);
        }
    }

    public function actionAlarmsetting()
    {
        $model = \common\models\Settings::find()->one();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
        }
        return $this->render('settings', ['model' => $model]);

    }

    public function actionNameproductcash()
    {
        $model = new Cashproductclient();
        $data = [];
        if ($model->load(Yii::$app->request->post())) {
            $data = yii::$app->db->createCommand('SELECT DISTINCT nm, cash_id FROM check0_p WHERE c=' . $model->clientid)->queryAll();
        }
        return $this->render('nameproductcash', ['zvtdata' => $data, 'model' => $model]);


    }

    public function actionPriceproductcash()
    {
        $model = new Cashproductclient();
        $data = [];
        if ($model->load(Yii::$app->request->post())) {
            $data = yii::$app->db->createCommand('SELECT `cash_id`,`prc`, max(`package_date`) as `pdate`, min(`package_date`) as `mdate` FROM `check0_p` WHERE (`package_date` between "' . $model->cashdatestart . ' 00:00:00" AND "' .
                $model->cashdateend . ' 23:59:59") AND (`c` = ' . $model->clientid . ') GROUP BY `cash_id`,`prc` order by `cash_id`,`mdate`')->queryAll();
        }
        return $this->render('priceproductcash', ['zvtdata' => $data, 'model' => $model]);


    }

    public function actionCleardata()
    {
        $model = new Cleardata();
        return $this->render('deletedata', ['model' => $model]);


    }

    public function actionPriceChart()
    {
//        if (Yii::$app->request->isAjax) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $cashId = 4101234626;// Yii::$app->request->post('cashId');
        $productId = 2;// Yii::$app->request->post('productId');
        $data = yii::$app->db->createCommand('SELECT `prc`, `package_date` FROM `check0_p` WHERE (`c` = ' . $productId . ') and (`cash_id` = ' . $cashId . ') order by `package_date`')->queryAll();
        $lastPrc = null;
        $lastDate = null;
        $e = [];
        foreach ($data as $d) {
            if (!$lastPrc) {
                $e[] = ['prc' => $d['prc'], 'package_date' => $d['package_date']];
                $lastPrc = $d['prc'];

            } else {
                if ($lastPrc != $d['prc']) {
                    $e[] = ['prc' => $lastPrc, 'package_date' => $lastDate];
                    $e[] = ['prc' => $d['prc'], 'package_date' => $d['package_date']];
                    $lastPrc = $d['prc'];
                }
                $lastDate = $d['package_date'];
            }
        }
        return $e;
//        }
    }
}
