<?php

namespace backend\controllers;

use Yii;
use common\models\Productlist;
use common\models\search\ProductlistSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ProductlistController implements the CRUD actions for Productlist model.
 */
class ProductlistController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProductlistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Перегляд  :" . $model->name,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Змінити', ['update', 'id' => $id], ['class' => 'btn btn-warning', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Productlist();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Створити новий Productlist",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $this->clearCash();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Створити новий Productlist",
                    'content' => '<span class="text-success">Create Productlist success</span>',
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Створити ще', ['create'], ['class' => 'btn btn-info', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Створити новий Productlist",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                $this->clearCash();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Змінити: " . $model->name,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $this->clearCash();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => $model->name,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Змінити', ['update', 'id' => $id], ['class' => 'btn btn-warning', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Змінити: " . $model->name,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрити', ['class' => 'btn btn-primary pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Зберегти', ['class' => 'btn btn-info', 'type' => "submit"])
                ];
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                $this->clearCash();
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if ($model->deleted == 1) {
            $model->deleted = 0;
        } else {
            $model->deleted = 1;
        }
        $model->save();
        $this->clearCash();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }
    }

    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }
        $this->clearCash();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }

    }

    protected function findModel($id)
    {
        if (($model = Productlist::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function clearCash()
    {
        $productList = Productlist::find()->indexBy('id')->asarray()->all();
        Yii::$app->cache->set('ProductList', $productList);

        $productList = Productlist::find()->asarray()->all();
        foreach ($productList as $item) {
            $productListName[$item['id']] = $item['name'];
        }
        Yii::$app->cache->set('ProductListName', $productListName);
    }
}


