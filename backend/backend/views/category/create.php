<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Categorylist */

?>
<div class="categorylist-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
