<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Categorylist */
?>
<div class="categorylist-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'deleted',
        ],
    ]) ?>

</div>
