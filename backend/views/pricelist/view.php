<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pricelist */
?>
<div class="pricelist-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'product_id',
            'price_date',
            'price',
        ],
    ]) ?>

</div>
