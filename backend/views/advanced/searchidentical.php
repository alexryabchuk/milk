<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
    <div class="site-index">
        <?php
        $form = ActiveForm::begin(['id' => 'n-form']);
        echo "<div class='row'><div class='col-lg-6'>" . $form->field($model, 'cashdatestart')->input('date') . "</div>";
        echo "<div class='col-lg-6'>" . $form->field($model, 'cashdateend')->input('date') . "</div></div>";
        echo '<div class="form-group">';
        echo Html::Button("Знайти", ['id' => 'javatest', "class" => "btn btn-primary"]);
        echo '</div>';
        ActiveForm::end();

        ?>

        <?php
        Modal::begin([
            'header' => '<h2>Пошук даних. Зачекайте...</h2>',
            'closeButton' => false,
        ]);
        ?>
        <p id="searchwork">Обробка даних за </p>
        <?php Modal::end(); ?>
        <div class="row">
            <div class="col-lg-12">
                <p class="info" id="searchResult"></p>
            </div>
        </div>
    </div>


<?php
$script = <<< JS
    $('#javatest').click(function() {
    var startdate = new Date($('#selectdate-cashdatestart').val());
    var enddate = new Date($('#selectdate-cashdateend').val());
    $('#searchwork').html('Обробка даних за:'+startdate.toLocaleDateString('ua')); 
    var d1 = startdate.valueOf();
    var d2 = enddate.valueOf();
        $('#w0').modal();
        var resultText = '';     //строка для вывода результатов  

        function send() 
            { 
                $.ajax({ 
                url: '/index.php?r=advanced%2Fdelidentical',
                type: "POST",
                timeout:0,
                data: {date:d1},
                success: function(data) { 
                    resultText += data.flag+"<br>";
                     if(d1>=d2) {
                        $('#w0').modal('hide');
                        $('#searchResult').html(resultText);             
                    }
                    else{
                        startdate.setDate(startdate.getDate()+1);
                        d1 = startdate.valueOf();
                        $('#searchwork').html('Обробка даних за:'+startdate.toLocaleDateString('ua'));             
                        send()     //выполняем рекурсивный запрос
                    }
                },
                error: function(jqXHR, status, e) {
                    if (status === "timeout") {  
                        alert("Время ожидания ответа истекло!");
                    } else {
                        alert(status); // Другая ошибка
                    }
                }
                }); 
            }
        send();
//if (d1>d2) {
//        alert ("Кінцева дата менша за початкову");
//} else
//{
//    do  {
//        alert( d1);
//        startdate.setDate(startdate.getDate()+1);
//        d1 = startdate.valueOf();
//        
//    }
//    while (d1<=d2);
//        
//        alert( enddate.getDate() );
//}        
});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>