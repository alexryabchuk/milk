<?php

use components\widgets\SelectCash;
use common\models\CashList;

$cashList = CashList::getCashList();

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?= SelectCash::widget(['model' => $model]) ?>
    <table class="table">
        <thead>
        <tr>
            <th>
                Назва продукту
            </th>
            <th>
                Назва касового
            </th>
            <th>
                Код касового
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($zvtdata as $item): ?>
            <tr>
                <td><?= $item['nm'] ?></td>
                <td><?= $cashList[$item['cash_id']] ?></td>
                <td><?= $item['cash_id'] ?></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>

</div>

