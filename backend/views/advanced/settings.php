<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6">
            <?php echo $form->field($model, 'cash_alarm_list')->widget(MultipleInput::className(), [
                'max' => 32,
                'allowEmptyList' => false,
                'columns' => [
                    ['name' => 'cash_alarm_list',
                        'options' => ['placeholder' => 'електронна адреса'],
                        'title' => 'Непрацюючі касові',]
                ],
                'min' => 1, // should be at least 2 rows
                'addButtonPosition' => [MultipleInput::POS_ROW]
            ])->label(false); ?>
        </div>
        <div class="col-lg-6">
            <?php echo $form->field($model, 'check_alarm_list')->widget(MultipleInput::className(), [
                'max' => 32,
                'allowEmptyList' => false,
                'columns' => [
                    ['name' => 'check_alarm_list',
                        'options' => ['placeholder' => 'електронна адреса'],
                        'title' => 'Пропущенні пакети',]
                ],
                'min' => 1, // should be at least 2 rows
                'addButtonPosition' => [MultipleInput::POS_ROW]
            ])->label(false); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Зберегти налаштування', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

