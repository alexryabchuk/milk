<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
    <div class="site-index">
        <?php $form = ActiveForm::begin(['id' => 'n-form']); ?>
        <div class='row'>
            <div class='col-lg-6'>
                <?= $form->field($model, 'startdate')->input('date') ?>
            </div>
            <div class='col-lg-6'>
                <?= $form->field($model, 'password')->textInput() ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::Button("Знайти", ['id' => 'javatest', "class" => "btn btn-primary"]); ?>
        </div>
        <? ActiveForm::end(); ?>
        <div class="row">
            <div class="col-lg-12">
                <p class="info" id="searchResult"></p>
            </div>
        </div>
    </div>


<?php
$script = <<< JS
    $('#javatest').click(function() {
    var startdate = new Date($('#cleardata-startdate').val());
    var pas = $('#cleardata-password').val();
    var d1 = startdate.valueOf();
        var resultText = '';     //строка для вывода результатов  

        $.ajax({ 
            url: '/index.php?r=advanced%2Fdeletedata',
            type: "POST",
            timeout:0,
            data: {date:d1,password:pas},
            success: function(data) { 
                resultText = data.flag;
                $('#searchResult').html(resultText);             
            },
            error: function(jqXHR, status, e) {
                if (status === "timeout") {  
                    alert("Время ожидания ответа истекло!");
                } else {
                    alert(status); // Другая ошибка
                }
            }
        }); 
});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>