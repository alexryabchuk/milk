<?php

use common\models\CashList;
use components\widgets\SelectPeriodProduct;
use dosamigos\chartjs\ChartJs;
use yii\helpers\ArrayHelper;

$cashList = CashList::getCashList();

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<style>
    .modal-backdrop {
        z-index: 1040 !important;
    }

    .modal-dialog {
        margin: 2px auto;
        z-index: 1100 !important;
    }
</style>
<div class="site-index">
    <?= SelectPeriodProduct::widget(['model' => $model]) ?>
    <table class="table">
        <thead>
        <tr>
            <th>
                Ціна
            </th>
            <th>
                Назва касового
            </th>
            <th>
                Код касового
            </th>
            <th>
                Дата першого продажу
            </th>
            <th>
                Дата останнього продажу
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($zvtdata as $item): ?>
            <tr>
                <td><?= $item['prc'] ?></td>
                <td><?= $cashList[$item['cash_id']] ?></td>
                <td><?= $item['cash_id'] ?></td>

                <td><?= date("d.m.Y H:i:s", strtotime($item['mdate'])) ?></td>
                <td><?= date("d.m.Y H:i:s", strtotime($item['pdate'])) ?></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>

</div>

<?php
yii\bootstrap\Modal::begin([
    'header' => 'Оформить заказ',
    'options' => ['tabindex' => ''],
    'id' => 'modal',
    'size' => 'modal-lg',
]);
?>
<div id='modal-content'>Загружаю...</div>
<?= ChartJs::widget([
    'type' => 'line',
    'id' => 'lines',
    'options' => [
        'class' => 'chartjs-render-monitor',
        'height' => 80,
        'width' => 300
    ],
    'data' => [
        'labels' => ArrayHelper::getColumn($zvtdata, 'cash_id'),
        'datasets' => [
            [
                'label' => "Количестро заказов",
                'backgroundColor' => "rgba(255,99,132,0.2)",
                'borderColor' => "rgba(255,99,132,1)",
                'pointBackgroundColor' => "rgba(255,99,132,1)",
                'pointBorderColor' => "#fff",
                'pointHoverBackgroundColor' => "#fff",
                'pointHoverBorderColor' => "rgba(255,99,132,1)",
                'data' => ArrayHelper::getColumn($zvtdata, 'prc'),
            ]
        ]
    ]
]);
?>
<?php yii\bootstrap\Modal::end(); ?>
<a id="modal-btn" data-target="<?php echo \yii\helpers\Url::to('/login') ?>">Вход</a>
<script>

</script>
<?php
$script = <<< JS

$('#modal-btn').on('click', function () {
        $('#modal').modal('show')
            .find('#modal-content')
            .load($(this).attr('data-target'));
    });

JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);
?>
