<?php

use common\models\Bscountry;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Bsproductlist1 */
?>
<div class="bsproductlist1-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'productname',
            'producttype',
            'barscale1',
            'barscale2',
            'term1',
            'term2',
            'countg',
            'mtara',
            'usush',
            'used',
            'minmassa',
            'maxmassa',
            'code1C',
            [
                'attribute' => 'country_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return Bscountry::getCountryList()[$model->country_id];
                }
            ],
            'param1',
            'param2',
            'param3',
            'param4',
            'param5',
            'param6',
        ],
    ]) ?>

</div>
