<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\Bscountry;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Bsproductlist1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bsproductlist1-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class='row'>
        <div class='col-lg-6'>
            <?= $form->field($model, 'country_id')->widget(Select2::classname(), [
                'name' => 'country_id',
                'value' => $model->country_id,
                'data' => Bscountry::getCountryList(),
                'hideSearch' => true,
                'pluginOptions' => [
                    'escapeMarkup' => new JsExpression("function(m) { return m; }"),
                    'allowClear' => true
                ]
            ]) ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-lg-6'>
            <?= $form->field($model, 'productname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-lg-6'>
            <?= $form->field($model, 'producttype')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-lg-5'>
            <?= $form->field($model, 'barscale1')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-lg-5'>
            <?= $form->field($model, 'barscale2')->textInput(['maxlength' => true]) ?>
        </div>
        <div class='col-lg-2'>
            <?= $form->field($model, 'code1C')->textInput() ?>
        </div>

    </div>
    <div class='row'>
        <div class='col-lg-3'>
            <?= $form->field($model, 'term1')->textInput() ?>
        </div>
        <div class='col-lg-3'>
            <?= $form->field($model, 'term2')->textInput() ?>
        </div>
        <div class='col-lg-3'>
            <?= $form->field($model, 'countg')->textInput() ?>
        </div>
        <div class='col-lg-3'>
            <?= $form->field($model, 'mtara')->textInput() ?>
        </div>
    </div>
    <div class='row'>
        <div class='col-lg-3'>
            <?= $form->field($model, 'usush')->textInput() ?>
        </div>
        <div class='col-lg-3'>
            <?= $form->field($model, 'minmassa')->textInput() ?>
        </div>
        <div class='col-lg-3'>
            <?= $form->field($model, 'maxmassa')->textInput() ?>
        </div>
        <div class='col-lg-3'>
            <?= $form->field($model, 'used')->checkbox() ?>
        </div>
    </div>
    <div class='row'>
        <?= $form->field($model, 'param1')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'param2')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'param3')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'param4')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'param5')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'param6')->textInput(['maxlength' => true]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>