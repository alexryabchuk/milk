<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Bsproductlist1 */

?>
<div class="bsproductlist1-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
