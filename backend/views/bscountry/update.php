<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Bscountry */
?>
<div class="bscountry-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
