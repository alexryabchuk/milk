<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;


$path = 'http://' . $_SERVER['SERVER_NAME'] . '/assets/images/users/avatar.jpg';


?>

<!-- START PAGE SIDEBAR -->
<div class="page-sidebar page-sidebar-fixed scroll">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation ">
        <li class="xn-logo">
            <a href="<?= Url::toRoute([Yii::$app->homeUrl]) ?>"><?= Yii::$app->name ?></a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
            <a href="#" class="profile-mini">
                <img src="<?= $path ?>" alt="John Doe"/>
            </a>
            <div class="profile">
                <div class="profile-image">
                    <img src="<?= $path ?>" alt="John Doe"/>
                </div>
                <div class="profile-data">
                    <div class="profile-data-name"><?php if (!Yii::$app->user->isGuest) echo Yii::$app->user->identity->username ?></div>
                    <div class="profile-data-title"><?php if (!Yii::$app->user->isGuest) echo 'getCurrentRole()' ?></div>
                </div>
                <div class="profile-controls">
                    <?= Html::a('<span class="fa fa-info"></span>', ['/users/profile'], ['title' => 'Профиль', 'class' => 'profile-control-left']); ?>

                    <?= Html::a('<span class="fa fa-envelope"></span>', ['/users/profile'], ['title' => 'Профиль', 'class' => 'profile-control-right']); ?>
                </div>
            </div>
        </li>

        <li class="xn-title">Menu</li>

        <li <?php if (Yii::$app->controller->id == 'site') {
            echo ' class = "active" ';
        } ?>>
            <?= Html::a('<span class="fa fa-dashboard"></span> <span class="xn-text">Показники</span>', ['/site/index'], []); ?>
        </li>
        <li class="xn-openable
                    <?php if (Yii::$app->controller->id == 'pidrozdil') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'categorylist') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'productlist') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'labellist') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'user') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'cashlist') {
            echo " active";
        } ?>
                    <?php if (Yii::$app->controller->id == 'pricelist') {
            echo " active";
        } ?>
                    "><?= Html::a('<span class="fa fa-wrench"></span> <span class="xn-text">Довідники</span>', ['#'], []); ?>
            <ul>
                <li <?php if (Yii::$app->controller->id == 'user') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >Користувачі</span>', ['/user/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'pricelist') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >Прайси</span>', ['/pricelist/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'pidrozdil') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-cutlery"></span> <span >Підрозділи</span>', ['/pidrozdil/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'categorylist') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-ice-lolly-tasted"></span> <span >Категорії продукції</span>', ['/categorylist/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'productlist') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-ice-lolly-tasted"></span> <span >Продукція</span>', ['/productlist/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'labellist') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="fa fa-users"></span> <span>Етикетки</span>', ['/labellist/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'cashlist') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="fa fa-wrench"></span> <span>Касові апарати</span>', ['/cashlist/index'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->id == 'bscountry') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="fa fa-wrench"></span> <span>Країни</span>', ['/bscountry/index'], []); ?>
                </li>
            </ul>
        </li>
        <li class="xn-openable <?php if (Yii::$app->controller->id == 'advanced') {
            echo " active";
        } ?> ">
            <?= Html::a('<span class="fa fa-wrench"></span> <span class="xn-text">Додаткові функції</span>', ['#'], []); ?>
            <ul>
                <li <?php if (Yii::$app->controller->action->id == 'searchidentical') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-globe"></span> <span >Пошук дублюючих чеків</span>', ['/advanced/searchidentical'], []); ?>
                </li>

                <li <?php if (Yii::$app->controller->action->id == 'nameproductcash') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-ice-lolly-tasted"></span> <span >Контроль назв продукції в касових</span>', ['/advanced/nameproductcash'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->action->id == 'priceproductcash') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-ice-lolly-tasted"></span> <span >Контроль цін продукції в касових</span>', ['/advanced/priceproductcash'], []); ?>
                </li>
                <li <?php if (Yii::$app->controller->action->id == 'cleardata') {
                    echo 'class="active"';
                } ?>>
                    <?= Html::a('<span class="glyphicon glyphicon-cutlery"></span> <span >Знищення данних</span>', ['/advanced/cleardata'], []); ?>
                </li>
            </ul>
        </li>
        <li <?php if (Yii::$app->controller->id == 'history-update') {
            echo 'class="active"';
        } ?>>
            <?= Html::a('<span class="glyphicon glyphicon-signal"></span> <span class="xn-text">Історя змін</span>', ['/history-update/index'], []); ?>
        </li>


    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->