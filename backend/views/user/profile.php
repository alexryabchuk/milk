<?php

/* @var $this yii\web\View */

use yii\bootstrap\Html;

use yii\widgets\ActiveForm;

$this->title = 'Портал Літинського мочного заводу';
?>
<div class="site-index">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($profile, 'lastname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($profile, 'firstname')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($profile, 'parentname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($profile, 'usercode')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton('Зберегти', ['class' => 'btn btn-info']) ?>
        </div>
    <?php } ?>
    <?php ActiveForm::end(); ?>
</div>
