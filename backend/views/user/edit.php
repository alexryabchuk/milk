<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = 'Update User: ';
$this->params['breadcrumbs'][] = ['label' => 'Користувачі', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Права';
?>
<div class="user-update">


    <?php $form = ActiveForm::begin(['action' => ["grants", 'id' => $user->getId()]]); ?>

    <?= Html::checkboxList('roles', $user_permit, $roles, ['separator' => '<br>']); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
