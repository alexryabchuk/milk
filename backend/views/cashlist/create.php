<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CashList */

?>
<div class="cash-list-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
