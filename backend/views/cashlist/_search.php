<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cashlistsearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cash-list-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'cash_number') ?>

    <?= $form->field($model, 'cash_madedate') ?>

    <?= $form->field($model, 'version_software') ?>

    <?= $form->field($model, 'fix_num') ?>

    <?php // echo $form->field($model, 'town') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'name') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
