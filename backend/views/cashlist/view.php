<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CashList */
?>
<div class="cash-list-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cash_number',
            'cash_madedate',
            'version_software',
            'fix_num',
            'town',
            'address',
            'name',
        ],
    ]) ?>

</div>
