<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Права користувачів';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <table class="table table-striped table-condensed">
        <thead>
        <tr>
            <th>Користувач</th>
            <th>Email</th>
            <th>Права</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($userList as $userItem) : ?>
            <tr>
                <td>
                    <?= $userItem->username ?>
                </td>
                <td>
                    <?= $userItem->email ?>
                </td>
                <td>
                    <?php echo HTML::a('Редагувати права', ['grant/edit', 'id' => $userItem->id]) ?>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>


</div>
