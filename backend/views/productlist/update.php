<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Productlist */
?>
<div class="productlist-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
