<?php

use common\models\Categorylist;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Productlist */
?>
<div class="productlist-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'category',
                'value' => function ($model) {
                    $categoryList = Categorylist::getAllCategoryList();
                    if (isset($categoryList[$model->category])) {
                        return $categoryList[$model->category];
                    }

                }
            ]

        ],
    ]) ?>

</div>
