<?php

use common\models\Categorylist;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Productlist */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productlist-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <?= $form->field($model, 'id')->textInput() ?>
        </div>
        <div class="col-sm-10">
            <?= $form->field($model, 'category')->dropDownList(Categorylist::getAllCategoryList()) ?>
        </div>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Створити' : 'Змінити', ['class' => $model->isNewRecord ? 'btn btn-info' : 'btn btn-warning']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
