<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Bsproductlist1 */

$this->title = 'Редагувати продукт: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Список підрозділів', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редагувати';
?>
<div class="bsproductlist1-update">
    <?= $this->render('_form', [
        'model' => $model,
        'labelList' => $labelList,
        'label' => $label,
    ]) ?>

</div>