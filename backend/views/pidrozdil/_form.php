<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Bsparam */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bsparam-form">


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <?= Html::checkboxList('label', $label, $labelList,
            [
                //'separator' => '<br>',
                'encode' => false,
                'item' => function ($index, $label, $name, $checked, $value) {
                    return '<div class="col-md-6">' . Html::checkbox($name, $checked, ['label' => $label, 'value' => $value]) . '</div>';
                }
            ]
        )
        ?>
    </div>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>