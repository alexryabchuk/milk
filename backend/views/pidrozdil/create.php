<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Bsparam */

$this->title = 'Створити підрозділ';
$this->params['breadcrumbs'][] = ['label' => 'Bsparams', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bsparam-create">

    <?= $this->render('_form', [
        'model' => $model,
        'label' => $label,
        'labelList' => $labelList,
    ]) ?>

</div>