<?php

use common\models\Bsproductlist1;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Bsproductlist1 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Список підрозділів', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bsproductlist1-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'productlist',
                'format' => 'raw',
                'value' => function ($model) {
                    $labelList = Bsproductlist1::getProductWithCountry();
                    $label = explode(',', $model->productlist);
                    $st = '';
                    foreach ($label as $id) {
                        $st .= $labelList[$id] . "<br>";
                    }
                    return $st;
                }
            ]
        ],
    ]) ?>

</div>