<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cash_list".
 *
 * @property integer $id
 * @property string $cash_number
 * @property string $cash_madedate
 * @property string $version_software
 * @property string $fix_num
 * @property string $town
 * @property string $address
 * @property string $name
 */
class Version extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'versions';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'version' => '№ версії',
            'comment' => 'Опис',
        ];
    }
}
