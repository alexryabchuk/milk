<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bscountry".
 *
 * @property int $id
 * @property string $name
 * @property resource $flag
 *
 * @property Bsproductlist1[] $bsproductlist1s
 * @property Bsproductlist2[] $bsproductlist2s
 */
class Bscountry extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bscountry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['flag'], 'string'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'flag' => 'Flag',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBsproductlist1s()
    {
        return $this->hasMany(Bsproductlist1::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBsproductlist2s()
    {
        return $this->hasMany(Bsproductlist2::className(), ['country_id' => 'id']);
    }
}
