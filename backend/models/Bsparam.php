<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bsparam".
 *
 * @property int $id
 * @property string $productlist
 * @property string $name
 */
class Bsparam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bsparam';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['productlist'], 'string', 'max' => 1024],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'productlist' => 'Дозволенна продукція',
            'name' => 'Назва',
        ];
    }

    public static function getPidrozdil()
    {
        return \yii\helpers\ArrayHelper::map(static::find()->asArray()->all(), 'id', 'name');
    }
}