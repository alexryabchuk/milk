<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productlist".
 *
 * @property integer $id
 * @property string $name
 * @property string $category
 */
class Productlist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productlist';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'category'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['category'], 'string', 'max' => 32],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код товару',
            'name' => 'Назва',
            'category' => 'Категорія',
        ];
    }
}
