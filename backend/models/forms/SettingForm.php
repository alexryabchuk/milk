<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "cash_list".
 *
 * @property integer $id
 * @property string $cash_number
 * @property string $cash_madedate
 * @property string $version_software
 * @property string $fix_num
 * @property string $town
 * @property string $address
 * @property string $name
 */
class SettingForm extends Model
{
    public $cash_alarm_list = array();

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'version' => '№ версії',
            'comment' => 'Опис',
        ];
    }

    public function rules()
    {
        return [
            [['cash_alarm_list'], 'safe'],
        ];
    }
}
