<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "cash_list".
 *
 * @property integer $id
 * @property string $cash_number
 * @property string $cash_madedate
 * @property string $version_software
 * @property string $fix_num
 * @property string $town
 * @property string $address
 * @property string $name
 */
class CountryForm extends Model
{
    public $country_id;


    public function rules()
    {
        return [
            [['country_id'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'country_id' => 'Країна',
        ];
    }
}
