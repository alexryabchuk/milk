<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "cash_list".
 *
 * @property integer $id
 * @property string $cash_number
 * @property string $cash_madedate
 * @property string $version_software
 * @property string $fix_num
 * @property string $town
 * @property string $address
 * @property string $name
 */
class Cleardata extends Model
{
    public $startdate;
    public $password;


    public function rules()
    {
        return [
            [['password', 'startdate'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'startdate' => 'До якої дати',
            'password' => 'Пароль',
        ];
    }
}
