<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pricelist".
 *
 * @property int $id
 * @property int $product_id Код продукту
 * @property string $price_date Дата прайсу
 * @property string $price
 *
 * @property Productlist $product
 */
class Pricelist extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pricelist';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['price_date'], 'safe'],
            [['price'], 'number'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Productlist::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Код продукту',
            'price_date' => 'Дата прайсу',
            'price' => 'Ціна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Productlist::className(), ['id' => 'product_id']);
    }
}
