<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bsproductlist1;

/**
 * Bsproductlist1Search represents the model behind the search form of `common\models\Bsproductlist1`.
 */
class Bsproductlist1Search extends Bsproductlist1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'term1', 'term2', 'countg', 'mtara', 'usush', 'used', 'minmassa', 'maxmassa', 'code1C', 'country_id'], 'integer'],
            [['productname', 'producttype', 'barscale1', 'barscale2', 'param1', 'param2', 'param3', 'param4', 'param5', 'param6'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bsproductlist1::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'term1' => $this->term1,
            'term2' => $this->term2,
            'countg' => $this->countg,
            'country_id' => $this->country_id,
        ]);

        $query->andFilterWhere(['like', 'productname', $this->productname])
            ->andFilterWhere(['like', 'producttype', $this->producttype])
            ->andFilterWhere(['like', 'barscale1', $this->barscale1]);


        return $dataProvider;
    }
}
