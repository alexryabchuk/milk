<?php
namespace console;

use Yii;

/**
 * Site controller
 */
class ParseCheck 
{
    public $_check= [];
    public $_checkService= [];
    public $_zorder= [];
    public $_package= [];
    public $_xml;
    
    private function _getP($XML) {
        $_p=array();
        $_pitem=  array();
        foreach ($XML as $X) {
            $_pitem['N']= (int) $X['N'];
            $_pitem['C']= (int) $X['C'];
            $_pitem['NM']= (string) $X['NM'];
            $_pitem['SM']= ((int) $X['SM'])/100;
            $_pitem['PRC']= ((int) $X['PRC'])/100;
            $_pitem['Q']= ((int) $X['Q'])/1000;
            $_pitem['W']= (string) $X['W'];
            $_p[]=$_pitem;
        }
        return $_p;
    }
    
    private function _getCP($XML) {
        $_cp=array();
        $_cpitem=  array();
        foreach ($XML as $X) {
            $_cpitem['N']= (int) $X['N'];
            $_cpitem['C']= (int) $X['С'];
            $_cpitem['SM']= (int) $X['SM'];
            $_cpitem['PRC']= (int) $X['PRC'];
            $_cpitem['CD']= (string) $X['CD'];
            $_cpitem['NM']= (string) $X['NM'];
            $_cpitem['TX']= (string) $X['TX'];
            $_cp[]=$_cpitem;
        }
        return $_cp;
    }
    
    private function _getI($XML) {
        $_i=array();
        $_iitem=  array();
        foreach ($XML as $X) {
            $_iitem['N']= (int) $X['N'];
            $_iitem['T']= (int) $X['T'];
            $_iitem['SM']= (float) $X['SM']/100;
            $_iitem['NM']= (string) $X['NM'];
            $_i[]=$_iitem;
        }
        if (count($_i)>1) {
            Yii::warning('В одному чеку два різних службових внесення: Касовий - '.$this->_package['cash_id'].' Дата - '.$this->_package['package_date'].' DI - '.$this->_package['di'], 'parse_m');
        }
        return $_iitem;
    }
    
    private function _getO($XML) {
        $_o=array();
        $_oitem=  array();
        foreach ($XML as $X) {
            $_oitem['N']= (int) $X['N'];
            $_oitem['T']= (int) $X['T'];
            $_oitem['SM']= (float) $X['SM']/100;
            $_oitem['NM']= (string) $X['NM'];
            $_o[]=$_oitem;
        }
        if (count($_o)>1) {
            Yii::warning('В одному чеку дві різні службові видачі: Касовий - '.$this->_package['cash_id'].' Дата - '.$this->_package['package_date'].' DI - '.$this->_package['di'], 'parse_m');
        }
        return $_oitem;
    }
    
    private function _getM($XML) {
        $_m=[]; $_mitem=[];
        foreach ($XML as $X) {
            $_mitem['T']= (int) $X['T'];
            $_mitem['SM']= ((int) $X['SM']-(float) $X['RM'])/100;
            $_mitem['NM']= (string) $X['NM'];
            if (!$_mitem['SM']==0) { $_m[]=$_mitem; }
        }
        $_m_single =$_mitem;
        $_m_single['SM'] =0;
        $mLast=-1;
        foreach ($_m as $_mitem) {
            $_m_single['SM'] = $_mitem['SM'];
            if ($mLast==-1) {
                $mLast=$_mitem['T'];
            } else {
                if (!$mLast==$_mitem['T']) { Yii::warning('В одному чеку два різних види оплати: Касовий - '.$this->_package['cash_id'].' Дата - '.$this->_package['package_date'].' DI - '.$this->_package['di'], 'parse_m');}
                $mLast=$_mitem['T'];
            }
        }
        return $_m_single;
    }
    
    private function _getVD($XML) {
        $_vd=[]; $_vditem=[];
        foreach ($XML as $X) {
            $XN=(int) $X['N'];
            if (isset($X->NI)) {
                $XML_NI=$X->NI;
                $_vditem1=  array();
                foreach ($XML_NI as $XNI) {
                    $_vditem1['N']= 0;
                    $_vditem1['NI']= (int) $XNI['NI'];
                    $_vd[]=$_vditem1;
                }
            } else {
                $_vditem['N']= $XN;
                $_vditem['NI']= (int)$X['NI'];
                $_vd[]=$_vditem;
            }
        }
        foreach ($_vd as $vd_item) {
            foreach ($this->_check['P'] as $id=>$p) {
                if ($p['N']==$vd_item['NI']) {
                    unset($this->_check['P'][$id]);
                } 
            }        
        }
        
    }
    
    private function _getE($XML) {
        $_e=array();
        $_eitem=  array();
        foreach ($XML as $X) {
            $_eitem['NO']= (int) $X['NO'];
            $_eitem['VD']= (int) $X['VD'];
            $_eitem['SM']= ((float) $X['SM'])/100;
            $_e[]=$_eitem;
        }
        if (count($_e)>1) {
            die('е більше 1');
        }
        return $_eitem;
    }
    
    private function _getZNC($XML) {
        $_znc=array();
        $_zncitem=  array();
        foreach ($XML as $X) {
            $_zncitem['NI']= (int) $X['NI'];
            $_zncitem['NO']= (int) $X['NO'];
            $_znc[]=$_zncitem;
        }
         if (count($_znc)>1) {
            Yii::warning('В одному чеку два теги про кількість z-звітів: Касовий - '.$this->_package['cash_id'].' Дата - '.$this->_package['package_date'].' DI - '.$this->_package['di'], 'parse_m');
        }
        return $_zncitem;
    }
    
    private function _getZIO($XML) {
        $_zio= []; $_zio['IO_SMI_0']=0;$_zio['IO_SMO_0']=0;$_zio['IO_SMI_1']=0;$_zio['IO_SMO_1']=0;
        foreach ($XML as $X) {
            if (((int) $X['T'])==0) {
                $_zio['IO_SMI_0']+= (int) $X['SMI']/100;
                $_zio['IO_SMO_0']+= (int) $X['SMO']/100;
            } else {
                $_zio['IO_SMI_1']+= (int) $X['SMI']/100;
                $_zio['IO_SMO_1']+= (int) $X['SMO']/100;
            }
        }
        return $_zio;
    }
    
    private function _getZM($XML) {
        $_zm=array();$_zm['M_SMI_0']=0;$_zm['M_SMO_0']=0;$_zm['M_SMI_1']=0;$_zm['M_SMO_1']=0;
        foreach ($XML as $X) {
            if (((int) $X['T'])==0) {
                $_zm['M_SMI_0']+= (int) $X['SMI']/100;
                $_zm['M_SMO_0']+= (int) $X['SMO']/100;
            } else {
                $_zm['M_SMI_1']+= (int) $X['SMI']/100;
                $_zm['M_SMO_1']+= (int) $X['SMO']/100;
            }
        }
        return $_zm;
    }
    
    private function getCheck($XML){
        if (isset($XML->P)) {
            $this->_check['P']=$this->_getP($XML->P);
        }
        if (isset($XML->M)) {
             $this->_check['M']=$this->_getM($XML->M);
        }
        if (isset($XML->VD)) {
            $this->_getVD($XML->VD);
        }
        if (isset($XML->E)) {
             $this->_check['E']=$this->_getE($XML->E);
        }
        $sumByProduct = 0;
        if (isset($this->_check['P'])) {
                foreach ($this->_check['P'] as $p) {
                $sumByProduct +=$p['SM'];
            }        
        }
        
        $this->_check['payProduct'] = $sumByProduct;
    }
    
    private function getCheckService($XML){
            if (isset($XML->I)) {
                $this->_checkService['I']= $this->_getI($XML->I);
            }
            if (isset($XML->O)) {
                $this->_checkService['O']=$this->_getO($XML->O);
            }
    }
    
    private function getZOrder ($XML) {
        if (isset($XML->Z['NO'])) {
            $this->_zorder['package_no']=(int) $XML->Z['NO'];
        }
        if (isset($XML->Z->NC)) {
            $this->_zorder['NC']=$this->_getZNC($XML->Z->NC);
        }
        if (isset($XML->Z->IO)) {
            $this->_zorder['IO']=$this->_getZIO($XML->Z->IO);
        }
        if (isset($XML->Z->M)) {
            $this->_zorder['M']=$this->_getZM($XML->Z->M);
        }
    }

    public static function parsePackage($PackBody) {
        $cash = new ParseCheck();
        $xml = simplexml_load_string('<?xml version="1.0" standalone="yes" ?>'.$PackBody);
        $cash->_package['cash_id']=(string) $xml['ZN'];
        $cash->_package['cash_id']=  substr($cash->_package['cash_id'], 4);
        $cash->_package['package_date'] = \DateTime::createFromFormat("YmdHis", (string) $xml->TS)->format('Y-m-d H:i:s');
        $cash->_package['di'] =(string) $xml['DI'];
        
        if (isset($xml->Z)) {
            $cash->_package['type']=3;
            $cash->getZOrder($xml);
        }
        if (isset($xml->C)) {
            if (isset($xml->C['T'])) {
                $cash->_package['type']=(int) $xml->C['T'];
                if ($cash->_package['type'] == 0 ) {$cash->getCheck($xml->C);}
                if ($cash->_package['type'] == 1 ) {$cash->getCheck($xml->C);}
                if ($cash->_package['type'] == 2 ) {$cash->getCheckService($xml->C);}
            }
        }
        return $cash;
    }
    
    
}
