<?php
namespace console;

use frontend\models\cash\Check0;
use frontend\models\cash\Check0P;
use frontend\models\cash\Check1;
use frontend\models\cash\CheckService;
use frontend\models\cash\Zorder;
use common\models\LastCashData;
use frontend\models\Packages;

class PackageService 
{
    public static function isSetPackage($package) {
        if ($package->_package['type']==0) {
            $d = Check0::find()->where(['DI'=>$package->_package['di']])->andWhere(['Cash_id'=>$package->_package['cash_id']])->count();
        }
        if ($package->_package['type']==1) {
            $d = Check1::find()->where(['DI'=>$package->_package['di']])->andWhere(['Cash_id'=>$package->_package['cash_id']])->count();
        }
        if ($package->_package['type']==2) {
            $d = CheckService::find()->where(['DI'=>$package->_package['di']])->andWhere(['Cash_id'=>$package->_package['cash_id']])->count();
        }
        if ($package->_package['type']==3) {
            $d = Zorder::find()->where(['DI'=>$package->_package['di']])->andWhere(['Cash_id'=>$package->_package['cash_id']])->count();
        }
        return (bool)$d>0; 
    }
    
    public static function searchPackage($di,$cashId) {
        
        $d = Check0::find()->where(['DI'=>$di])->andWhere(['Cash_id'=>$cashId])->asArray()->one();
        if ($d) {
            $d['package_type'] = 0;
            return $d; 
        }
        $d = Check1::find()->where(['DI'=>$di])->andWhere(['Cash_id'=>$cashId])->asArray()->one();
        if ($d) {
            $d['package_type'] = 1;
            return $d; 
        }
        $d = CheckService::find()->where(['DI'=>$di])->andWhere(['Cash_id'=>$cashId])->asArray()->one();
        if ($d) {
            $d['package_type'] = 2;
            return $d; 
        }
        $d = Zorder::find()->where(['DI'=>$di])->andWhere(['Cash_id'=>$cashId])->asArray()->one();
        if ($d) {
            $d['package_type'] = 3;
            return $d; 
        }
        return FALSE; 
    }
    
    public static function getPackages($limit) {
        $package_id = LastCashData::getLastPackage(0);
        $cash_packs= Packages::find()->where('id >'.$package_id)->orderBy('id')->limit($limit)->all();
        return $cash_packs;
    }
    
    public static function _saveCheck0P($cashId,$packageDate,$product,$id) {
        $checkP = new Check0P();
        $checkP->cash_id = $cashId;
        $checkP->package_date = $packageDate;
        $checkP->c=$product['C'];
        $checkP->n=$product['N'];
        $checkP->nm=$product['NM'];
        $checkP->prc=$product['PRC'];
        $checkP->q=$product['Q'];
        $checkP->sm=$product['SM'];
        $checkP->w=$product['W'];
        $checkP->check0_id=$id;
        $checkP->save();
    }
    
    public static function _saveCheck0($package,$mirrorId){
        if ($package->_check['E']['VD']==0) {
            $check0 = new Check0();
            $check0->cash_id=$package->_package['cash_id'];
            $check0->di=$package->_package['di'];
            $check0->package_date=$package->_package['package_date'];
            $check0->mirror_id=$mirrorId;
            $check0->e_vd=$package->_check['E']['VD'];
            $check0->e_no=$package->_check['E']['NO'];
            $check0->e_sm=$package->_check['E']['SM'];
            $check0->m_type=$package->_check['M']['T'];
            $check0->m_name=$package->_check['M']['NM'];
            $check0->p_sm=$package->_check['payProduct'];    
            $check0->save();
            $products = $package->_check['P'];
            foreach ($products as $product ) {
                PackageService::_saveCheck0P($package->_package['cash_id'], $package->_package['package_date'], $product,$check0->id);
            }
        } 
    }
    
    public static function _saveCheck1($package,$mirrorId){
        
        if ($package->_check['E']['VD']==0) {
            $check1 = new Check1();
            $check1->cash_id=$package->_package['cash_id'];
            $check1->di=$package->_package['di'];
            $check1->package_date=$package->_package['package_date'];
            $check1->mirror_id=$mirrorId;
            $check1->e_vd=$package->_check['E']['VD'];
            $check1->e_no=$package->_check['E']['NO'];
            $check1->e_sm=$package->_check['E']['SM'];
            $check1->m_type=$package->_check['M']['T'];
            $check1->m_name=$package->_check['M']['NM'];
            $check1->p_sm=$package->_check['payProduct']; 
            $check1->save();
        } 
        
    }
    
    public static function _saveCheckService($package,$mirrorId){
        $checkService = new CheckService();
        $checkService->cash_id=$package->_package['cash_id'];
        $checkService->di=$package->_package['di'];
        $checkService->package_date=$package->_package['package_date'];
        $checkService->mirror_id=$mirrorId;
        isset($package->_checkService['O']['SM']) ? $checkService->o_sm=$package->_checkService['O']['SM']:0;
        if (isset($package->_checkService['O']['T'])) {$checkService->m_type=$package->_checkService['O']['T'];}
        if (isset($package->_checkService['O']['NM']))  {$checkService->m_name=$package->_checkService['O']['NM'];}
        isset($package->_checkService['I']['SM']) ? $checkService->i_sm=$package->_checkService['I']['SM']:0;
        if (isset($package->_checkService['I']['T']))  {$checkService->m_type=$package->_checkService['I']['T'];}
        if (isset($package->_checkService['I']['NM']))  {$checkService->m_name=$package->_checkService['I']['NM'];}
        $checkService->save();
    }
    
    public static function _saveZorder($package,$mirrorId){
        $Zorder = new Zorder();
        $Zorder->cash_id=$package->_package['cash_id'];
        $Zorder->di=$package->_package['di'];
        $Zorder->package_date=$package->_package['package_date'];
        $Zorder->mirror_id=$mirrorId;
        isset($package->_zorder['M']['M_SMI_0']) ? $Zorder->m_smi_0=$package->_zorder['M']['M_SMI_0']:0;
        isset($package->_zorder['M']['M_SMI_1']) ? $Zorder->m_smi_1=$package->_zorder['M']['M_SMI_1']:0;
        isset($package->_zorder['M']['M_SMO_0']) ? $Zorder->m_smo_0=$package->_zorder['M']['M_SMO_0']:0;
        isset($package->_zorder['M']['M_SMO_1']) ? $Zorder->m_smo_1=$package->_zorder['M']['M_SMO_1']:0;
        isset($package->_zorder['IO']['IO_SMI_0']) ? $Zorder->io_smi_0=$package->_zorder['IO']['IO_SMI_0']:0;
        isset($package->_zorder['IO']['IO_SMI_1']) ? $Zorder->io_smi_1=$package->_zorder['IO']['IO_SMI_1']:0;
        isset($package->_zorder['IO']['IO_SMO_0']) ? $Zorder->io_smo_0=$package->_zorder['IO']['IO_SMO_0']:0;
        isset($package->_zorder['IO']['IO_SMO_1']) ? $Zorder->io_smo_1=$package->_zorder['IO']['IO_SMO_1']:0;
        $Zorder->nc_ni=$package->_zorder['NC']['NI'];
        $Zorder->nc_no=$package->_zorder['NC']['NO'];
        $Zorder->save();
    }

    public static function savePackage($package,$mirrorId){
        if ($package->_package['type'] == 0) {
            PackageService::_saveCheck0($package,$mirrorId);
        } 
        if ($package->_package['type'] == 1) {
            PackageService::_saveCheck1($package,$mirrorId);
        }
        if ($package->_package['type'] == 2) {
            PackageService::_saveCheckService($package,$mirrorId);
        }
        if ($package->_package['type'] == 3) {
            PackageService::_saveZorder($package,$mirrorId);
        }
    }
}
