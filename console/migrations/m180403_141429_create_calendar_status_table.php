<?php

use yii\db\Migration;

/**
 * Handles the creation of table `calendar_status`.
 */
class m180403_141429_create_calendar_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('calendar_status', [
            'id' => $this->primaryKey(),
            'status' => $this->string(20),
            'comment' => $this->text(),
            'day'=>$this->integer(11)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('calendar_status');
    }
}
