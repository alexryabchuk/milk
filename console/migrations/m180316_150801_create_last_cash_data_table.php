<?php

use yii\db\Migration;

/**
 * Handles the creation of table `last_cash_data`.
 */
class m180316_150801_create_last_cash_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('last_cash_data', [
            'cash_id' => $this->string(11),
            'last_di' => $this->integer(),
            'last_date' => $this->dateTime(),
        ]);
        $this->addPrimaryKey('pk_id','last_cash_data','cash_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('last_cash_data');
    }
}
