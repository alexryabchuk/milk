<?php

use yii\db\Migration;

/**
 * Handles the creation of table `check0_p`.
 */
class m180316_125521_create_check0_p_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('check0_p', [
            'id' => $this->primaryKey(),
            'check0_id' =>$this->integer(),
            'cash_id' => $this->string(10)->comment('Номер касового апарата'),
            'package_date' =>$this->dateTime()->comment('Дата пакета'),
            'n'=>$this->integer()->comment('№ в чеку'),
            'c'=>$this->integer()->comment('Код продукта'),
            'nm' => $this->string(120)->comment('Назва продукту'),
            'sm'=>$this->decimal(10,2)->comment('Сумма '),
            'q'=>$this->decimal(10,3)->comment('Кількість в грамах'),
            'prc'=>$this->decimal(10,2)->comment('Ціна'),
            'w'=>$this->integer()->comment('Ваговий/Штучний')
        ]);
        $this->createIndex(
            'idx-check0_p-check0_id',
            'check0_p',
            'check0_id'
        );
        $this->createIndex(
            'idx-check0_p-package_date',
            'check0_p',
            'package_date'
        );
        $this->createIndex(
            'idx-check0_p-cash_id',
            'check0_p',
            'cash_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('check0_p');
        $this->dropIndex(
            'idx-check0_p-check0_id',
            'check0_p'
        );
        $this->dropIndex(
            'idx-check0_p-package_date',
            'check0_p'
        );
        $this->dropIndex(
            'idx-check0_p-cash_id',
            'check0_p'
        );
    }
}
