<?php

use yii\db\Migration;

/**
 * Class m181221_154649_alter_productlist_table
 */
class m181221_154649_alter_productlist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('productlist','deleted',$this->smallInteger(1)->defaultValue(0)->comment('Елемент знищений'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181221_154649_alter_productlist_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181221_154649_alter_productlist_table cannot be reverted.\n";

        return false;
    }
    */
}
