<?php

use yii\db\Migration;

/**
 * Handles the creation of table `zorder`.
 */
class m180314_132910_create_zorder_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('zorder', [
            'id' => $this->primaryKey(),
            'mirror_id' =>$this->integer()->comment('Ід пакета в зеркалі'),
            'cash_id' => $this->string(10)->comment('Номер касового апарата'),
            'package_date' =>$this->dateTime()->comment('Дата пакета'),
            'di'=>$this->integer()->comment('DI пакета'),
            'm_smi_0'=>$this->decimal(10,2)->comment('Сумма внесення готівка'),
            'm_smo_0'=>$this->decimal(10,2)->comment('Сумма видачі готівка'),
            'm_smi_1'=>$this->decimal(10,2)->comment('Сумма внесення картка'),
            'm_smo_1'=>$this->decimal(10,2)->comment('Сумма видачі картка'),
            'io_smi_0'=>$this->decimal(10,2)->comment('Сумма внесення готівка'),
            'io_smo_0'=>$this->decimal(10,2)->comment('Сумма видачі готівка'),
            'io_smi_1'=>$this->decimal(10,2)->comment('Сумма внесення картка'),
            'io_smo_1'=>$this->decimal(10,2)->comment('Сумма видачі картка'),
            'nc_ni'=>$this->integer()->comment('Кількість чеків оплати'),
            'nc_no'=>$this->integer()->comment('Кількість чеків повернення')
        ]);
        $this->createIndex(
            'idx-zorder-package_date',
            'zorder',
            'package_date'
        );
        $this->createIndex(
            'idx-zorder-cash_id',
            'zorder',
            'cash_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-zorder-package_date',
            'zorder'
        );
        $this->dropIndex(
            'idx-zorder-cash_id',
            'zorder'
        );
        $this->dropTable('zorder');
    }
}
