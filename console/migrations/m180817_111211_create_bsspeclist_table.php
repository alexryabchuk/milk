<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bsspeclist`.
 */
class m180817_111211_create_bsspeclist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bsspeclist', [
            'id' => $this->primaryKey(),
            'pid' => $this->integer(),
            'num' => $this->integer(),
            'varka' => $this->integer(),
            'madedate' => $this->date(),
            'specdate' => $this->date(),
            'used' => $this->integer()->defaultValue(1),
            'country_id' => $this->integer(),
        ]);
        $this->createIndex('idx-bsspeclist-pid', 'bsspeclist', 'pid');
       $this->addForeignKey('fk-bsspeclist-pid', 'bsspeclist', 'pid', 'bscountry', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-bsspeclist-pid', 'bsspeclist');
        $this->dropTable('bsspeclist');
    }
}
