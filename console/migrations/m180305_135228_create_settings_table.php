<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m180305_135228_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'cash_alarm' => $this->text(),
            'check_alarm' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
