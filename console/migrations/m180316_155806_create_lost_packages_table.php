<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lost_packages`.
 */
class m180316_155806_create_lost_packages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lost_packages', [
            'id' => $this->primaryKey(),
            'cash_id'=> $this->string(11)->comment(''),
            'di'=> $this->integer()->comment(''),
            'updated_at'=>$this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('lost_packages');
    }
}
