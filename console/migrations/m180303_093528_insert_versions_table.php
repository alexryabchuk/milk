<?php

use yii\db\Migration;

/**
 * Class m180303_093528_insert_versions_table
 */
class m180303_093528_insert_versions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('versions', [
            'version' => '1.03.01',
            'comments' => "<p class='version'><B>02.03.2018 ver:1.03.01 </b>Додано ланцюги пропущених пакетів. Реалізовано вилучення не потрібних пакетів</p>",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180303_093528_insert_versions_table cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180303_093528_insert_versions_table cannot be reverted.\n";

        return false;
    }
    */
}
