<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bspidrozdil`.
 */
class m180817_111857_create_bspidrozdil_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bspidrozdil', [
            'id' => $this->primaryKey(),
            'name' => $this->string(32),
            'comport' => $this->integer(),
            'printer1' => $this->string(60),
            'printer2' => $this->string(60),
            'printer3' => $this->string(60),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bspidrozdil');
    }
}
