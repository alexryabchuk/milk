<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bsproductlist2`.
 */
class m180817_111153_create_bsproductlist2_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bsproductlist2', [
            'id' => $this->primaryKey(),
            'productname' => $this->string('64')->notNull(),
            'et1' => $this->binary(),
            'used' => $this->integer()->notNull()->defaultValue(1),
            'country_id' => $this->integer()->notNull(),
            'param1' => $this->string('100'),
            'param2' => $this->string('100'),
            'param3' => $this->string('100'),
            'param4' => $this->string('100'),
            'param5' => $this->string('100'),
            'param6' => $this->string('100'),
        ]);
        $this->createIndex('idx-bsproductlist2-country_id', 'bsproductlist2', 'country_id');
       $this->addForeignKey('fk-bsproductlist2-country_id', 'bsproductlist2', 'country_id', 'bscountry', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-bsproductlist2-country_id', 'bsproductlist2');
        $this->dropTable('bsproductlist2');
    }
}
