<?php

use yii\db\Migration;

/**
 * Handles the creation of table `check_service`.
 */
class m180314_123707_create_check_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('check_service', [
            'id' => $this->primaryKey(),
            'mirror_id' =>$this->integer()->comment('Ід пакета в зеркалі'),
            'cash_id' => $this->string(10)->comment('Номер касового апарата'),
            'package_date' =>$this->dateTime()->comment('Дата пакета'),
            'di'=>$this->integer()->comment('DI пакета'),
            'm_type'=>$this->integer()->comment('Тип оплати'),
            'm_name'=>$this->string(32)->comment('Назва типу оплати'),
            'i_sm'=>$this->decimal(10,2)->comment('Сумма внесення'),
            'o_sm'=>$this->decimal(10,2)->comment('Сумма видачі'),
            'e_vd'=>$this->integer()->comment('Відміна')
        ]);
        $this->createIndex(
            'idx-check_service-package_date',
            'check_service',
            'package_date'
        );
        $this->createIndex(
            'idx-check_service-cash_id',
            'check_service',
            'cash_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-check_service-package_date',
            'check_service'
        );
        $this->dropIndex(
            'idx-check_service-cash_id',
            'check_service'
        );
        $this->dropTable('check_service');
    }
}
