<?php

use yii\db\Migration;

/**
 * Class m180413_073938_alter_clientlist_table
 */
class m180413_073938_alter_clientlist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%clientlist}}', 'address', $this->string());
        $this->addColumn('{{%clientlist}}', 'naspunct', $this->integer());
        $this->addColumn('{{%clientlist}}', 'region', $this->integer());
        $this->addColumn('{{%clientlist}}', 'oblast', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180413_073938_alter_clientlist_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180413_073938_alter_clientlist_table cannot be reverted.\n";

        return false;
    }
    */
}
