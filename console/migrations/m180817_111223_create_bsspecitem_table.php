<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bsspecitem`.
 */
class m180817_111223_create_bsspecitem_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bsspecitem', [
            'id' => $this->primaryKey(),
            'pid' => $this->integer(),
            'sid' => $this->integer(),
            'barcode1' => $this->string(13),
            'barcode2' => $this->string(13),
            'useddate'=> $this->date(),
            'place' => $this->integer(),
            'massa' => $this->integer(),
            'yasch' => $this->integer(),
            ]);
        $this->createIndex('idx-bsspecitem-pid', 'bsspecitem', 'pid');
       $this->addForeignKey('fk-bsspecitem-pid', 'bsspecitem', 'pid', 'bsproductlist1', 'id', 'CASCADE');
        $this->createIndex('idx-bsspecitem-sid', 'bsspecitem', 'pid');
       $this->addForeignKey('fk-bsspecitem-sid', 'bsspecitem', 'pid', 'bsspeclist', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-bsspecitem-pid', 'bsspecitem');
        $this->dropForeignKey('fk-bsspecitem-sid', 'bsspecitem');
        $this->dropTable('bsspecitem');
    }
}
