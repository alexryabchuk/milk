<?php

use yii\db\Migration;

/**
 * Class m180302_083228_insert_versions_table
 */
class m180302_083228_insert_versions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('versions', [
            'version' => '1.01',
            'comments' => "<p class='version'><B>01.03.2018 ver:1.01 </b>Додано можливість ручного пошуку та видалення продубльованих чеків</p>",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180302_083228_insert_versions_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180302_083228_insert_versions_table cannot be reverted.\n";

        return false;
    }
    */
}
