<?php

use yii\db\Migration;

/**
 * Class m180305_122746_insert_versions_table
 */
class m180305_122746_insert_versions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('versions', [
            'version' => '1.03.07',
            'comments' => "<p class='version'><B>05.03.2018 ver:1.03.07 </b>Додано редагування профіля приймальників у адмінпанелі роботи з користувачами</p>",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180305_122746_insert_versions_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180305_122746_insert_versions_table cannot be reverted.\n";

        return false;
    }
    */
}
