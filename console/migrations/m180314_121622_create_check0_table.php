<?php

use yii\db\Migration;

/**
 * Handles the creation of table `check0`.
 */
class m180314_121622_create_check0_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('check0', [
            'id' => $this->primaryKey(),
            'mirror_id' =>$this->integer()->comment('Ід пакета в зеркалі'),
            'cash_id' => $this->string(10)->comment('Номер касового апарата'),
            'package_date' =>$this->dateTime()->comment('Дата пакета'),
            'di'=>$this->integer()->comment('DI пакета'),
            'm_type'=>$this->integer()->comment('Тип оплати'),
            'm_name'=>$this->string(32)->comment('Назва типу оплати'),
            'e_no'=>$this->integer()->comment('Номер чека'),
            'e_sm'=>$this->decimal(10,2)->comment('Сумма чека'),
            'p_sm'=>$this->decimal(10,2)->comment('Сумма по продукції'),
            'e_vd'=>$this->integer()->comment('Відміна')
        ]);
        $this->createIndex(
            'idx-check0-package_date',
            'check0',
            'package_date'
        );
        $this->createIndex(
            'idx-check0-cash_id',
            'check0',
            'cash_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-check0-package_date',
            'check0'
        );
        $this->dropIndex(
            'idx-check0-cash_id',
            'check0'
        );
        $this->dropTable('check0');

    }
}
