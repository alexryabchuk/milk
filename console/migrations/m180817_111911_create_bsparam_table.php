<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bsparam`.
 */
class m180817_111911_create_bsparam_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bsparam', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
            'productlist' => $this->string(1024)
            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bsparam');
    }
}
