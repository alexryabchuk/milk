<?php

use yii\db\Migration;

/**
 * Handles the creation of table `current_price`.
 */
class m190109_142617_create_current_price_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('current_price', [
                'id' => $this->primaryKey(),
                'product_id' => $this->integer()->comment('Код продукту'),
                'price_date' => $this->date()->comment('Дата прайсу'),
                'price' => $this->decimal(15,2),
                'last_price' => $this->decimal(15,2),

            ]);
        $this->createIndex('idx-current_price-product_id', 'current_price', 'product_id');
        $this->addForeignKey('fk-current_price-product_id', 'current_price', 'product_id', 'productlist', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('current_price');
    }
}
