<?php

use yii\db\Migration;

/**
 * Handles the creation of table `versions`.
 */
class m180302_082155_create_versions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('versions', [
            'id' => $this->primaryKey(),
            'version' => $this->string(10),
            'comments' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('versions');
    }
}
