<?php

use yii\db\Migration;

/**
 * Class m181222_072343_dropindex_productlist_table
 */
class m181222_072343_dropindex_productlist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('ProductCode','productlist');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181222_072343_dropindex_productlist_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181222_072343_dropindex_productlist_table cannot be reverted.\n";

        return false;
    }
    */
}
