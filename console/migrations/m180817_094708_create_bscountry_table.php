<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bscountry`.
 */
class m180817_094708_create_bscountry_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bscountry', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64),
            'flag' => $this->binary(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bscountry');
    }
}
