<?php

use yii\db\Migration;

/**
 * Class m180302_103408_addgrant_dellostcheck
 */
class m180302_103408_addgrant_dellostcheck extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth= Yii::$app->authManager;
        $cash_workcheck_dellostcheck = $auth->createPermission('p_cash_workcheck_dellostcheck');
        $cash_workcheck_dellostcheck->description='Касові апарати: Знищення пропущених чеків';
        $auth->add($cash_workcheck_dellostcheck);
        $rcash_workcheck_dellostcheck = $auth->createRole('cash_workcheck_dellostcheck');
        $rcash_workcheck_dellostcheck->description='Касові апарати: Знищення пропущених чеків';
        $auth->add($rcash_workcheck_dellostcheck);
        $auth->addChild($rcash_workcheck_dellostcheck,$cash_workcheck_dellostcheck);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180302_103408_addgrant_dellostcheck cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180302_103408_addgrant_dellostcheck cannot be reverted.\n";

        return false;
    }
    */
}
