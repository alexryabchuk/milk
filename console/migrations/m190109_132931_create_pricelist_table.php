<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pricelist`.
 */
class m190109_132931_create_pricelist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pricelist', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->comment('Код продукту'),
            'price_date' => $this->date()->comment('Дата прайсу'),
            'price' => $this->decimal(15,2),

        ]);
        $this->createIndex('idx-pricelist-product_id', 'pricelist', 'product_id');
        $this->addForeignKey('fk-pricelist-product_id', 'pricelist', 'product_id', 'productlist', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pricelist');
    }
}
