<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bsproductlist1`.
 */
class m180817_111143_create_bsproductlist1_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bsproductlist1', [
            'id' => $this->primaryKey(),
            'productname' => $this->string('64')->notNull(),
            'producttype' => $this->string('64')->notNull(),
            'barscale1' => $this->string('13'),
            'barscale2' => $this->string('13'),
            'term1' => $this->integer()->notNull(),
            'term2' => $this->integer()->notNull(),
            'countg' => $this->integer()->notNull(),
            'mtara' => $this->integer()->notNull(),
            'usush' => $this->integer()->notNull(),
            'et1' => $this->binary(),
            'et2' => $this->binary(),
            'used' => $this->integer()->notNull()->defaultValue(1),
            'minmassa' => $this->integer()->notNull(),
            'maxmassa' => $this->integer()->notNull(),
            'code1C' => $this->integer(),
            'country_id' => $this->integer()->notNull(),
            'param1' => $this->string('100'),
            'param2' => $this->string('100'),
            'param3' => $this->string('100'),
            'param4' => $this->string('100'),
            'param5' => $this->string('100'),
            'param6' => $this->string('100'),
        ]);
        $this->createIndex('idx-bsproductlist1-country_id', 'bsproductlist1', 'country_id');
       $this->addForeignKey('fk-bsproductlist1-country_id', 'bsproductlist1', 'country_id', 'bscountry', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-bsproductlist1-country_id', 'bsproductlist1');
        $this->dropTable('bsproductlist1');
    }
}
