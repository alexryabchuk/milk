<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use frontend\service\ZvitService;
use common\models\CalendarStatus;
use DateTime;

class CalendarstatusController extends Controller
{
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    
    public function actionIndex($d = null)    {
        if ($d === null) {
            $d = date ('Y-m-d');
        }
        for ($i=0;$i<20;$i++) {
            echo $i;
            $date = new DateTime($d);
            $date->modify("-".$i." day");
            
            $data = ZvitService::cashzvtZorder($date->format('Y-m-d'), $date->format('Y-m-d'));
            $status1=0; $status2=0;
            foreach ($data['data'] as $dat) {
                    if ($dat['p']!=$dat['e']) {
                        $status1++;
                    } 
                    if ($dat['z']!=$dat['e']) {
                        $status2++;
                    } 
            }
            $day = CalendarStatus::find()->where(['day'=>strtotime($date->format('Y-m-d'))])->one();
            if (!$day) {
                $day = new CalendarStatus();
                $day->day =strtotime($date->format('Y-m-d'));
            }
            $comment='';
            $day->status='status-ok';
            if ($status1>0) {
                $day->status='status-sumerror';
                $comment.="Не відповідність сум продукції і продажу: $status1/n";
            }
            if ($status2>0) {
                $day->status='status-nopackage';
                $comment.="По деяким касовим апаратам є незагружені чеки: $status2/n";
            }
            $day->save();
            var_dump($date);
        }

        
    }





    
}
