<?php
namespace components\widgets;
use yii\base\Widget;
use yii\helpers\Html;
class SelectCash extends Widget
{
    public $model;
    public function init()
    {
        parent::init();
        
    }
    public function run()
    {
        return $this->render('selectCash',['model'=> $this->model]);
    }
}