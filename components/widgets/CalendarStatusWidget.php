<?php
namespace components\widgets;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\CalendarStatus;
use yii\helpers\Url;



/** 
 * Обеспечивает создание календаря и манипулирование событиями 
 * 
 * Версия PHP 5 
 * 
 * ЛИЦЕНЗИЯ: на этот файл распространяется лицензия MIT,
 * доступная по адресу:
 * at http://www.opensource.org/licenses/mit-license.html 
 * 
 * @author     Jason Lengstorf <jason.lengstorf@ennuidesign.com> 
 * @copyright  2009 Ennui Design 
 * @license    http://www.opensource.org/licenses/mit-license.html 
 */ 
class Calendar 
{

	/** 
     * Дата, на основании которой должен строиться календарь
     * 
     * Формат хранения: ГГГГ-ММ-ДД ЧЧ:ММ:СС
     * 
     * @var string: дата, выбранная для построения календаря
     */ 
    private $_useDate;

    /** 
     * Месяц, для которого строится календарь
     * 
     * @var int: выбранный месяц
     */ 
    private $_m;

    /** 
     * Год, из которого выбирается начальный день месяца
     * 
     * @var int: выбранный год
     */ 
    private $_y;

    /** 
     * Количество дней в выбранном месяце
     * 
     * @var int:   количество дней в месяце
     */ 
    private $_daysInMonth;

    /** 
     * Индекс дня недели, с которого начинается месяц (0-6)
     *  
     * @var int: день недели, с которого начинается месяц
     */ 
    private $_startDay;

   /** 
     * Создает объект базы данных и хранит соответствующие данные
     * При создании экземпляра этого класса конструктор принимает
     * объект базы данных в качестве параметра. Если значение этого
     * параметра отлично от null, оно сохраняется в закрытом
     * свойстве $_db. Если же это значение равно null, то вместо
     * этого создается и сохраняется новый РБО-объектю.
     *
     * Этот метод осуществляет сбор и сохранение дополнительной
     * информации, включая месяц, начиная с которого должен строиться
     * календарь, количество дней в указанном месяце, начальный день
     * недели этого месяца, а также текущий день недели.
     *
     * @param object $dbo:   объект базы данных 
     * @param string $useDate: дата, выбранная для построения календаря
     * @return void 
     */ 
    public function __construct($useDate=NULL)
    {
        /*                                                      
        * Собрать и сохранить информацию, относящуюся к месяцу 
        */  
        if ( isset($useDate) )
        {
             $this->_useDate = $useDate;
        }
        else
        {
             $this->_useDate = date('Y-m-d H:i:s');
        }
		/*
		 * Преобразовать во временную метку UNIX, а затем       
		 * определить месяц и год, которые следует использовать 
		 * при построении календаря                         
		 */
        $ts = strtotime($this->_useDate);
        $this->_m = date('m', $ts);
        $this->_y = date('Y', $ts);

		/*                                                      
		 * Определить количество дней, содержащихся в месяце    
		 */
        $this->_daysInMonth = cal_days_in_month(
                CAL_GREGORIAN,
                $this->_m,
                $this->_y
            );

		/*                                                    
         * Определить, с какого дня недели начинается месяц   
         */
        $ts = mktime(0, 0, 0, $this->_m, 1, $this->_y);
        $this->_startDay = date('w', $ts)-1;
        if ($this->_startDay == -1) {
            $this->_startDay = 6;
        }
    }

    /** 
     * Загружает информацию о событии (событиях) в массив 
     * 
     * @return array: массиш событий, извлеченных из базы данных 
     */ 
    private function _loadEventData()
    {
        $start_ts = mktime(0, 0, 0, $this->_m, 1, $this->_y);
        $end_ts = mktime(23, 59, 59, $this->_m+1, 0, $this->_y);
        $eventList = CalendarStatus::find()->where(['between', 'day', $start_ts, $end_ts])->all();
        $events=[];
        foreach ($eventList as $event) {
            $day = (int) date('d',$event->day);
            $events[$day]=$event->status;
        }
        return $events;
    }


    /**                                                                        
     * Возвращает HTML-разметку для отображения календаря и событии            
     *                                                                         
     * На основании информации, хранящейся в свойствах класса,                 
     * загружаются события для данного месяца, генерируется                    
     * календарь и возвращается актуальная разметка.                           
     *                                                                         
     * @return string:   HTML-разметка календаря                               
     */
    public function buildCalendar()
    {
        $months = array( 1 => 'Січень' , 'Лютий' , 'Березень' , 'Квітень' , 'Травень' , 'Червень' , 'Липень' , 'Серпень' , 'Вересень' , 'Жовтень' , 'Листопад' , 'Грудень' );
		/*                                                                     
         * Определить месяц календаря и создать массив сокращенных обозначений 
         * дней недели, которые будут использованы в заголовках столбцов       
         */ 
        $cal_month = date($months[date( 'n' , strtotime($this->_useDate))] .' Y', strtotime($this->_useDate));
        $weekdays = array('Понеділок', 'Вівторок', 'Середа',
                'Четвер', 'Пятниця', 'Субота', 'Неділя');

        /*                                                                     
         * Добавить заголовок в HTML-разметку календаря                        
         */
        $prevMounth = date('Y-m-d', mktime(0, 0, 0, $this->_m-1, 1, $this->_y));
        $nextMounth = date('Y-m-d', mktime(0, 0, 0, $this->_m+1, 1, $this->_y));
        
        $prevUrl =  Html::a('<button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span></button>', Url::to(['cash/main','date'=>$prevMounth]));
        $nextUrl = Html::a('<button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-forward" aria-hidden="true"></span></button>', Url::to(['cash/main','date'=>$nextMounth]));
        $html = "<span>".$prevUrl."</span><span class='calendar-title'>$cal_month</span><span>".$nextUrl."</span>";
        for ( $d=0, $labels=NULL; $d<7; ++$d )
        {
            $labels .= "\n\t\t<li>" . $weekdays[$d] . "</li>";
        }
        $html .= "\n\t<ul class=\"weekdays\">"
            . $labels . "\n\t</ul>";

        /*
         * Загрузить данные о событиях
         */
        $events = $this->_loadEventData();
        

        /*
         * Создать HTML-разметку календаря 
         */
        
        $html .= "\n\t<ul>"; // Начало нового маркированного списка
        for ( $i=1, $c=1, $t=date('j'), $m=date('m'), $y=date('Y');
                $c<=$this->_daysInMonth; ++$i )
        {
		/* 
        * Принять класс "fill" к ячейкам календаря.
        * располагающимся перед первым днем данного месяца
        */
            $class = $i<=$this->_startDay ? "fill" : NULL;

			/* 
			* Добавить класс "today",
			* если дата совпадает с текущей 
			*/ 
            if ( $c==$t && $m==$this->_m && $y==$this->_y )
            {
                $class = "today";
            }


            /*
             * Добавить день месяца, идентифицирующий ячейку календаря
             */
            if ( $this->_startDay<$i && $this->_daysInMonth>=$c)
            {
                if ( isset($events[$c]) )
                    {
                     $class=$events[$c];
                    }    
                $date = sprintf("\n\t\t\t<strong>%02d</strong>",$c++);
            }
            else { $date="&nbsp;"; }

                        /*
             * Создать открывающий и закрывающий дескрипторы элемента списка
             */
            
            $ls = sprintf("\n\t\t<li class=\"%s\">", $class);
            $le = "\n\t\t</li>";

            /*
             * Если текущий день суббота, перейти в следующий ряд 
             */
            $wrap = $i!=0 && $i%7==0 ? "\n\t</ul>\n\t<ul>" : NULL;

            /*
             *Собрать разрозненные части воедино
             */
            $html .= $ls . $date .  $le . $wrap;
        }

        /*
         * Add filler to finish out the last week
         */
        while ( $i%7!=1 )
        {
            $html .= "\n\t\t<li class=\"fill\">&nbsp;</li>";
            ++$i;
        }

        /*
         * Добавить заполнители для завершения последней недели
         */
        $html .= "\n\t</ul>\n\n";

        /*
         * Закрыть окончательный неупорядоченный список
         */
        return $html;
    }

}

class CalendarStatusWidget extends Widget
{
    public $date;
    public function init()
    {
        parent::init();
        if (!$this->date) {
            $this->date = date ('Y-m-d');
        }
    }
    public function run()
    {
        $c= new Calendar($this->date);
        return $c->buildCalendar();
    }
}