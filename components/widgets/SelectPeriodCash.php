<?php
namespace components\widgets;
use yii\base\Widget;
use yii\helpers\Html;
class SelectPeriodCash extends Widget
{
    public $model;
    public function init()
    {
        parent::init();
        
    }
    public function run()
    {
        return $this->render('selectPeriodCash',['model'=> $this->model]);
    }
}