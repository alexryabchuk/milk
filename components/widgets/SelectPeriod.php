<?php
namespace components\widgets;
use yii\base\Widget;
use yii\helpers\Html;
class SelectPeriod extends Widget
{
    public $model;
    public function init()
    {
        parent::init();
        
    }
    public function run()
    {
        return $this->render('selectPeriod',['model'=> $this->model]);
    }
}