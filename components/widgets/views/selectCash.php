<?php
use yii\bootstrap\ActiveForm;
use common\models\Productlist;
use yii\bootstrap\Html;
$clientList = Productlist::getProductListName();
$form = ActiveForm::begin([
    'layout'=>'horizontal',

]);
echo '<div class="row"><div class=\'col-lg-10\'>'.$form->field($model, 'clientid',[
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-2 text-right',
        'offset' => 'col-sm-offset-2',
        'wrapper' => 'col-sm-10',
        'error' => '',
        'hint' => '',
    ]])->dropDownList($clientList).'</div>';
echo "<div class='col-lg-2 text-right'>".Html::submitButton("Зформувати", ["class" => "btn btn-primary ", "name" => "login-button"])."</div></div>";
ActiveForm::end();
?>