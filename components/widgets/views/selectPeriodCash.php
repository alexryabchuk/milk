<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\CashList;
$clientList = CashList::getCashList();
$form = ActiveForm::begin([
    'layout'=>'horizontal',
    'options' => ['class' => 'signup-form form-register1'],
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-6 text-right',
            'offset' => 'col-sm-offset-6',
            'wrapper' => 'col-sm-6',
            'error' => '',
            'hint' => '',
        ],
    ],
]);
echo '<div class="row">'.$form->field($model, 'clientid',[
    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
    'horizontalCssClasses' => [
        'label' => 'col-sm-2 text-right',
        'offset' => 'col-sm-offset-2',
        'wrapper' => 'col-sm-10',
        'error' => '',
        'hint' => '',
    ]])->dropDownList($clientList).'</div>';
echo "<div class='row'><div class='col-lg-4'>".$form->field($model, 'cashdatestart')->input('date')."</div>";
echo "<div class='col-lg-4'>".$form->field($model, 'cashdateend')->input('date')."</div>";
echo "<div class='col-lg-4 text-right'>".Html::submitButton("Зформувати", ["class" => "btn btn-primary ", "name" => "login-button"])."</div></div>";
ActiveForm::end();
?>