<?php
namespace components\widgets;
use yii\base\Widget;
use yii\helpers\Html;
class SelectPeriodProduct extends Widget
{
    public $model;
    public function init()
    {
        parent::init();
        
    }
    public function run()
    {
        return $this->render('SelectPeriodProduct',['model'=> $this->model]);
    }
}