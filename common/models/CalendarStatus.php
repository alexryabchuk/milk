<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calendar_status".
 *
 * @property int $id
 * @property string $status
 * @property string $comment
 * @property int $day
 */
class CalendarStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calendar_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment'], 'string'],
            [['day'], 'integer'],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'comment' => 'Comment',
            'day' => 'Day',
        ];
    }
}

