<?php

namespace common\models;

use yii\base\Model;

/**
 * Signup form
 */
class SelectDate extends Model
{
    public $cashdatestart;
    public $cashdateend;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $this->cashdatestart = date("Y-m-d");
        $this->cashdateend = date("Y-m-d");
    }


    public function rules()
    {
        return [
            ['cashdatestart', 'date'],
            ['cashdateend', 'date'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'cashdatestart' => 'Початкова дата',
            'cashdateend' => 'Кінцева дата',
        ];
    }

}
