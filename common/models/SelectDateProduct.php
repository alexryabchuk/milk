<?php

namespace common\models;

use yii\base\Model;

/**
 * Signup form
 */
class SelectDateProduct extends Model
{
    public $cashdatestart;
    public $cashdateend;
    public $productid;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['cashdatestart', 'date'],
            ['cashdateend', 'date'],
            ['productid', 'integer'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'cashdatestart' => 'Початкова дата',
            'cashdateend' => 'Кінцева дата',
            'productid' => 'Контрагент',

        ];
    }

}
