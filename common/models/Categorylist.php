<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "categorylist".
 *
 * @property int $id
 * @property string $name
 * @property int $deleted
 */
class Categorylist extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorylist';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['deleted'], 'integer'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'name' => 'Название',
            'deleted' => 'Елемент знищений',
        ];
    }

    public static function getAllCategoryList()
    {
        return ArrayHelper::map(static::find()->asArray()->all(), 'id', 'name');
    }
}
