<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "productlist".
 *
 * @property integer $id
 * @property string $name
 * @property string $category
 */
class Productlist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'productlist';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'category'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 64],
            [['category'], 'string', 'max' => 32],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Код',
            'name' => 'Назва',
            'category' => 'Категорія',
        ];
    }

    public static function getProductList()
    {
        $productList = Yii::$app->cache->get('ProductList');
        if ($productList === false) {
            $productList = Productlist::find()->indexBy('id')->asarray()->all();
            Yii::$app->cache->set('ProductList', $productList);
        }
        return $productList;
    }

    public static function getProductName($id)
    {
        return static::find()->where(['id' => $id])->one();
    }

    public static function getProductListName()
    {
        $productListName = Yii::$app->cache->get('ProductListName');
        if ($productListName === false) {
            $productList = Productlist::find()->asarray()->all();
            foreach ($productList as $item) {
                $productListName[$item['id']] = $item['name'];
            }
            Yii::$app->cache->set('ProductListName', $productListName);
        }
        return $productListName;
    }
}
