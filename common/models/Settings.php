<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 */
class Settings extends ActiveRecord
{
    public $cash_alarm_list;
    public $check_alarm_list;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    public function attributeLabels()
    {
        return [
            'cash_alarm' => 'Непрацюючі касові',
            'check_alarm' => 'Пропущені чеки',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cash_alarm_list', 'check_alarm_list'], 'safe'],
        ];
    }

    public function afterFind()
    {
        $this->cash_alarm_list = array_filter(Json::decode($this->getAttribute('cash_alarm')));
        $this->check_alarm_list = array_filter(Json::decode($this->getAttribute('check_alarm')));
        return parent::afterFind();
    }

    public function beforeSave($insert)
    {
        $this->setAttribute('cash_alarm', Json::encode(array_filter($this->cash_alarm_list)));
        $this->setAttribute('check_alarm', Json::encode(array_filter($this->check_alarm_list)));
        return parent::beforeSave($insert);
    }

}
