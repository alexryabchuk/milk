<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "lost_packages".
 *
 * @property int $id
 * @property string $cash_id
 * @property int $di
 * @property int $updated_at
 */
class LostPackages extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lost_packages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['di', 'updated_at'], 'integer'],
            [['cash_id'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cash_id' => 'Cash ID',
            'di' => 'Di',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['updated_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];


    }

    public static function setLostPackages($cashId, $start, $end)
    {
        $d = $end - $start;
        if ($d > 1) {
            Yii::info(date('d.m.y H:i:s') . ' Виявлено пропущені чеки №№ ' . $start . ' - ' . $end, 'parse_package');
        }
        for ($i = $start + 1; $i < $end; $i++) {
            $lost = new LostPackages();
            $lost->cash_id = $cashId;
            $lost->di = $i;
            if (!$lost->save()) {
                print_r($lost->errors);
            }
        }
    }

    public static function delLostPackage($cashId, $Di)
    {
        $lostPackage = LostPackages::findOne(['cash_id' => $cashId, 'di' => $Di]);
        if ($lostPackage) {
            $lostPackage->delete();
        }
    }
}
