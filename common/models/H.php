<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "h".
 *
 * @property string $A
 * @property string $B
 * @property string $C
 * @property string $D
 * @property int $E
 */
class H extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'h';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['E'], 'integer'],
            [['A'], 'string', 'max' => 10],
            [['B'], 'string', 'max' => 17],
            [['C'], 'string', 'max' => 23],
            [['D'], 'string', 'max' => 26],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'A' => 'A',
            'B' => 'B',
            'C' => 'C',
            'D' => 'D',
            'E' => 'E',
        ];
    }
}
