<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cash_list".
 *
 * @property integer $id
 * @property string $cash_number
 * @property string $cash_madedate
 * @property string $version_software
 * @property string $fix_num
 * @property string $town
 * @property string $address
 * @property string $name
 */
class CashList extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash_list';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cash_number', 'cash_madedate', 'version_software', 'fix_num', 'town', 'address', 'name'], 'required'],
            [['cash_madedate'], 'safe'],
            [['cash_number', 'version_software', 'fix_num'], 'string', 'max' => 10],
            [['town', 'address'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 64],
            [['cash_number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cash_number' => 'Cash Number',
            'cash_madedate' => 'Cash Madedate',
            'version_software' => 'Version Software',
            'fix_num' => 'Fix Num',
            'town' => 'Town',
            'address' => 'Address',
            'name' => 'Name',
        ];
    }

    public static function getCashList()
    {
        $cashList = Yii::$app->cache->get('CashList');
        if ($cashList === false) {
            $cashList = CashList::find()->asarray()->all();
            $cashList = ArrayHelper::map($cashList, 'cash_number', 'name');
            Yii::$app->cache->set('CashList', $cashList);
        }
        return $cashList;
    }

}
