<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bsproductlist1;

/**
 * Bsproductlist1Search represents the model behind the search form about `common\models\Bsproductlist1`.
 */
class Bsproductlist1Search extends Bsproductlist1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'term1', 'term2', 'countg', 'mtara', 'usush', 'used', 'minmassa', 'maxmassa', 'code1C', 'country_id'], 'integer'],
            [['productname', 'producttype', 'barscale1', 'barscale2', 'param1', 'param2', 'param3', 'param4', 'param5', 'param6', 'et1', 'et2', 'pic1', 'pic2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Bsproductlist1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'term1' => $this->term1,
            'term2' => $this->term2,
            'countg' => $this->countg,
            'mtara' => $this->mtara,
            'usush' => $this->usush,
            'used' => $this->used,
            'minmassa' => $this->minmassa,
            'maxmassa' => $this->maxmassa,
            'code1C' => $this->code1C,
            'country_id' => $this->country_id,
        ]);

        $query->andFilterWhere(['like', 'productname', $this->productname])
            ->andFilterWhere(['like', 'producttype', $this->producttype])
            ->andFilterWhere(['like', 'barscale1', $this->barscale1])
            ->andFilterWhere(['like', 'barscale2', $this->barscale2])
            ->andFilterWhere(['like', 'param1', $this->param1])
            ->andFilterWhere(['like', 'param2', $this->param2])
            ->andFilterWhere(['like', 'param3', $this->param3])
            ->andFilterWhere(['like', 'param4', $this->param4])
            ->andFilterWhere(['like', 'param5', $this->param5])
            ->andFilterWhere(['like', 'param6', $this->param6]);

        return $dataProvider;
    }
}
