<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dov_region".
 *
 * @property int $code
 * @property int $oblast_code
 * @property string $name_region
 */
class DovRegion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dov_region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oblast_code', 'name_region'], 'required'],
            [['oblast_code'], 'integer'],
            [['name_region'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Код',
            'oblast_code' => 'Область',
            'name_region' => 'Назва району',
        ];
    }


    public static function getRegion($oblast, $name)
    {
        return static::find()->where(['oblast_code' => $oblast])->andWhere(['name_region' => $name])->one();

    }

    public static function getRegionByCode($oblast = null, $region = null)
    {
        if ($oblast === null) {
            $list = static::find()->asArray()->all();
            return ArrayHelper::map($list, 'code', 'name_region');
        } else {
            if ($region === null) {
                $list = static::find()->where(['oblast_code' => $oblast])->asArray()->all();
                return ArrayHelper::map($list, 'code', 'name_region');
            } else {
                $list = static::findOne($region);
                return $list->name_region;
            }

        }

    }

    public static function getOblastRegionByCode()
    {
        $list = static::find()->asArray()->all();
        $oblast = DovOblast::getOblastByCode();
        $t = [];
        foreach ($list as $l) {
            $t[$l['code']] = $l['name_region'] . ',' . $oblast[$l['oblast_code']];
        }
        return $t;
    }

    public static function getRegionByName()
    {
        $list = static::find()->asArray()->all();
        return ArrayHelper::map($list, 'name_region', 'code');
    }
}
