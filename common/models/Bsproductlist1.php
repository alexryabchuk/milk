<?php

namespace common\models;

use Yii;
use common\models\Bscountry;
/**
 * This is the model class for table "bsproductlist1".
 *
 * @property int $id
 * @property string $productname
 * @property string $producttype
 * @property string $barscale1
 * @property string $barscale2
 * @property int $term1
 * @property int $term2
 * @property int $countg
 * @property int $mtara
 * @property int $usush
 * @property resource $et1
 * @property resource $et2
 * @property int $used
 * @property int $minmassa
 * @property int $maxmassa
 * @property int $code1C
 * @property int $country_id
 * @property string $param1
 * @property string $param2
 * @property string $param3
 * @property string $param4
 * @property string $param5
 * @property string $param6
 *
 * @property Bscountry $country
 * @property Bsspecitem[] $bsspecitems
 */
class Bsproductlist1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bsproductlist1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['productname', 'producttype', 'term1', 'term2', 'countg', 'mtara', 'usush', 'minmassa', 'maxmassa', 'country_id'], 'required'],
            [['term1', 'term2', 'countg', 'mtara', 'usush', 'used', 'minmassa', 'maxmassa', 'code1C', 'country_id'], 'integer'],
            [['et1', 'et2'], 'string'],
            [['productname', 'producttype'], 'string', 'max' => 64],
            [['barscale1', 'barscale2'], 'string', 'max' => 13],
            [['param1', 'param2', 'param3', 'param4', 'param5', 'param6'], 'string', 'max' => 100],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bscountry::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'productname' => 'Назва продукту',
            'producttype' => 'Категорія',
            'barscale1' => 'Штрихкод',
            'barscale2' => 'Штрихкод 2',
            'term1' => 'Термін дозрівання',
            'term2' => 'Термін зберігання',
            'countg' => 'К-сть од',
            'mtara' => 'Тара',
            'usush' => 'Усушка',
            'et1' => 'Et1',
            'et2' => 'Et2',
            'used' => 'Використовується',
            'minmassa' => 'Мін. маса',
            'maxmassa' => 'Макс. маса',
            'code1C' => 'Код 1C',
            'country_id' => 'Країна',
            'param1' => 'Параметр1',
            'param2' => 'Параметр2',
            'param3' => 'Параметр3',
            'param4' => 'Параметр4',
            'param5' => 'Параметр5',
            'param6' => 'Параметр6',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Bscountry::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBsspecitems()
    {
        return $this->hasMany(Bsspecitem::className(), ['pid' => 'id']);
    }
    public static function getProductWithCountry() {
        $countrys = \yii\helpers\ArrayHelper::map(Bscountry::find()->asArray()->all(),'id','flag');

        $products = static::find()->select(['id', 'productname', 'country_id'])->orderBy('country_id, productname')->asArray()->all();

        $productList = [];
        foreach ($products as $product) {
            $productList[$product['id']] = $product['productname'].
                '<img style="height:20px" src="data:image/jpeg;base64,'.
                base64_encode( $countrys[$product['country_id']]).'"/>';

        }
        return $productList;
    }

    public static function getProductList()
    {
         return  \yii\helpers\ArrayHelper::map(static::find()->asArray()->all(),'id','productname');

    }
}