<?php

namespace common\models;

/**
 * This is the model class for table "last_cash_data".
 *
 * @property string $cash_id
 * @property int $last_di
 * @property integer $last_date
 */
class LastCashData extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'last_cash_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cash_id'], 'required'],
            [['last_di'], 'integer'],
            [['cash_id'], 'string', 'max' => 11],
            [['cash_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cash_id' => 'Cash ID',
            'last_di' => 'Last Di',
        ];
    }

    public static function getLastPackage($cashId)
    {
        $last = static::findOne(['cash_id' => $cashId]);
        if ($last) {
            return (int)$last->last_di;
        } else {
            return 0;
        }
    }

    public static function setLastPackage($cashId, $di, $lastDate)
    {
        $last = static::findOne(['cash_id' => $cashId]);
        if (!$last) {
            $last = new LastCashData();
        }
        $last->cash_id = $cashId;
        $last->last_di = $di;
        $last->last_date = $lastDate;
        $last->save();

    }
}
