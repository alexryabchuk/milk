<?php

namespace common\models;


use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bscountry".
 *
 * @property int $id
 * @property string $name
 * @property resource $flag
 *
 * @property Bsproductlist1[] $bsproductlist1s
 * @property Bsproductlist2[] $bsproductlist2s
 * @property Bsspeclist[] $bsspeclists
 */
class Bscountry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bscountry';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['flag'], 'string'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Країна',
            'flag' => 'Зображення',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBsproductlist1s()
    {
        return $this->hasMany(Bsproductlist1::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getCountryList()
    {
        $countrys = static::find()->asArray()->all();
        $countryList = [];
        foreach ($countrys as $country) {
            $countryList[$country['id']] = '<img style="height:22px" src="data:image/jpeg;base64,' . base64_encode($country['flag']) . '"/>&nbsp&nbsp' . $country['name'];
        }

        return $countryList;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBsproductlist2s()
    {
        return $this->hasMany(Bsproductlist2::className(), ['country_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBsspeclists()
    {
        return $this->hasMany(Bsspeclist::className(), ['pid' => 'id']);
    }
}