<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dov_oblast".
 *
 * @property int $code
 * @property string $name_oblast
 */
class DovOblast extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dov_oblast';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_oblast'], 'required'],
            [['name_oblast'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Код',
            'name_oblast' => 'Область',
        ];
    }

    public static function getOblastByCode($code = null)
    {
        if ($code === null) {
            $list = static::find()->asArray()->all();
            return ArrayHelper::map($list, 'code', 'name_oblast');
        } else {
            $list = static::findOne($code);
            return $list->name_oblast;
        }

    }

    public static function getOblastForRegion()
    {
        $list = ArrayHelper::map(static::find()->asArray()->all(), 'code', 'name_oblast');
        return ['oblast_code' => $list['code'], 'name_oblast' => $list['name_oblast']];
    }

    public static function getOblastByName()
    {
        $list = static::find()->asArray()->all();
        return ArrayHelper::map($list, 'name_oblast', 'code');
    }
}
