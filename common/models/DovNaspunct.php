<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dov_naspunct".
 *
 * @property int $code
 * @property int $oblast_code
 * @property int $region_code
 * @property string $name_naspunct
 * @property string $postal_code
 */
class DovNaspunct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dov_naspunct';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['oblast_code', 'region_code', 'name_naspunct', 'postal_code'], 'required'],
            [['oblast_code', 'region_code'], 'integer'],
            [['name_naspunct'], 'string', 'max' => 255],
            [['postal_code'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'oblast_code' => 'Область',
            'region_code' => 'Район',
            'name_naspunct' => 'Населений пункт',
            'postal_code' => 'Поштовий індекс',
        ];
    }

    public static function getNaspunctInRegion($region = null)
    {
        if ($region === NULL) {
            $data = static::find()->where(['region_code' => 1])->asArray()->all();
        } else {
            $data = static::find()->where(['region_code' => $region])->asArray()->all();
        }

        return ArrayHelper::map($data, 'code', 'name_naspunct');

    }

    public static function searchByName($g)
    {
        $data = static:: find()->where(['like', 'name_naspunct', $g])->asArray()->all();
        $oblastList = DovOblast::getOblastByCode();
        $regionList = DovRegion::getRegionByCode();
        $res = [];
        foreach ($data as $d) {
            $res[] = ['id' => $d['code'], 'text' => $d['name_naspunct'] . ' ,' . $regionList[$d['region_code']] . ' р-н, ' . $oblastList[$d['oblast_code']] . ' обл.'];
        }
        return $res;
    }

    public static function getNaspunct($id)
    {
        return static::find($id)->one();
    }

    public static function getNaspunctName($id)
    {
        $data = static::findOne(['code' => $id]);
        if ($data) {
            return $data->name_naspunct;
        } else {
            return '';
        }

    }
}
