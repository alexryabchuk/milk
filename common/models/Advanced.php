<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class Advanced extends Model
{
    /**
     * @inheritdoc
     */
    public static function searchIdenticalCheck($rows1, $check, $date)
    {
        $lastDI = 0;
        $lostCount = 0;
        $candel = [];
        foreach ($rows1 as $row) {
            if ($lastDI == $row['di']) {
                $zpId = $row['id'];
                $zpChektable = $check;
                $candel[] = [$zpId, $zpChektable];
                $lostCount++;
                $lostCheck[] = $lastDI;
            }
            $lastDI = $row['di'];
        }
        if ($lostCount > 0) {
            $candel = Yii::$app->db->createCommand()->batchInsert('candel', ['id', 'chektable'], $candel)->execute();
            $connection = Yii::$app->db->createCommand("CALL del_check();")->execute();
            return '<b>' . date("d.m.Y", $date) . ': </b>' . implode(', ', $lostCheck) . '. <b>Знайдено ' . $lostCount . ' </b><br>';
        } else {
            return '<b>' . date("d.m.Y", $date) . ': Не знайдено жодного запису.</b><br>';
        }
    }


    public static function searchIdenticalCheckDate($date)
    {
        $date1 = date("Y-m-d 0:0:0", $date);
        $date2 = date("Y-m-d 23:59:59", $date);
        $rows1 = Yii::$app->db->createCommand('SELECT `id`, `di` FROM `check0` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") order by `cash_id`, `di`')->queryAll();
        $t1 = static::searchIdenticalCheck($rows1, 0, $date);
        $rows1 = Yii::$app->db->createCommand('SELECT `id`, `di` FROM `check1` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") order by `cash_id`, `di`')->queryAll();
        $t2 = static::searchIdenticalCheck($rows1, 1, $date);
        $rows1 = Yii::$app->db->createCommand('SELECT `id`, `di` FROM `check_service` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") order by `cash_id`, `di`')->queryAll();
        $t3 = static::searchIdenticalCheck($rows1, 2, $date);
        $rows1 = Yii::$app->db->createCommand('SELECT `id`, `di` FROM `zorder` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '") order by `cash_id`, `di`')->queryAll();
        $t4 = static::searchIdenticalCheck($rows1, 3, $date);
        return $t1 . $t2 . $t3 . $t4;

    }

    public static function searchIdenticalCheckDate2($date)
    {
        $date1 = date("Y-m-d 0:0:0", $date);
        $date2 = date("Y-m-d 23:59:59", $date);
        $rows1 = Yii::$app->db->createCommand('SELECT sum(SM) as SM, DAT_ID FROM `DAT_P` WHERE `DAT_ID` IN (SELECT `id` FROM `DAT` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '")  AND (`package_type`=0) AND (`pay`>0)) group by DAT_ID')->queryAll();
        $rows2 = Yii::$app->db->createCommand('SELECT `NO`, SM, DAT_ID FROM `DAT_E` WHERE `DAT_ID` IN (SELECT `id` FROM `DAT` WHERE (`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '")  AND (`package_type`=0))')->queryAll();
        $zvtdata = array();
        foreach ($rows1 as $row) {
            $zvtdata[$row['DAT_ID']]['SM'] = $row['SM'];
        }
        foreach ($rows2 as $row) {
            $zvtdata[$row['DAT_ID']]['SM2'] = $row['SM'];
            $zvtdata[$row['DAT_ID']]['NO'] = $row['NO'];
        }
        $lastNo = 0;
        $lostCheck = [];
        $candel = [];
        $lostCount = 0;
        foreach ($zvtdata as $id => $data) {
            if (isset($data['SM2'])) {
                if ($data['NO'] != 0) {
                    if ($lastNo == $data['NO']) {
                        $candel[][0] = $id;
                        $lostCount++;
                        $lostCheck[] = $lastNo;
                    }
                }
            }
            $lastNo = $data['NO'];
        }
        $rows1 = Yii::$app->db->createCommand('SELECT `id`, `DI` FROM `DAT` WHERE ((`package_date` BETWEEN "' . $date1 . '" AND "' . $date2 . '")  AND (`package_type`=3)) order by `Cash_id`, `DI`')->queryAll();
        $lastDI = 0;
        foreach ($rows1 as $row) {
            if ($lastDI == $row['DI']) {
                $candel[][0] = $row['id'];
                $lostCount++;
                $lostCheck[] = $lastDI;
            }
            $lastDI = $row['DI'];
        }
        if ($lostCount > 0) {
            $candel = Yii::$app->db->createCommand()->batchInsert('candel', ['id'], $candel)->execute();
            $connection = Yii::$app->db->createCommand("CALL del_check();")->execute();
            return '<b>' . date("d.m.Y", $date) . ': </b>' . implode(', ', $lostCheck) . '. <b>Знайдено ' . $lostCount . ' </b>';
        } else {
            return '<b>' . date("d.m.Y", $date) . ': Не знайдено жодного запису.</b>';
        }

    }

}
