<?php
$_fn=realpath(__DIR__."/../data")."/data.db";
return [
    'components' => [
         'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=portal',
            'username' => 'sa',
            'password' => 'KlopM,./',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'schemaCacheDuration' => 3600,
        ],
        'db2' => [
            'class' => 'yii\db\Connection',
             'dsn' => 'mysql:host=localhost;dbname=cash',
            'charset' => 'utf8',
            'username' => 'sa',
            'password' => 'KlopM,./',
        ],
        'db3' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'sqlite:../../data/data.db',
            'username' => '',
            'password' => '',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
    ],
];
