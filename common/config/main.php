<?php
return [
    'bootstrap' => ['log'],
    'language' => 'uk-RU',
    'sourceLanguage' => 'uk-RU',
    'timeZone' => 'Europe/Kiev',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],

     'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@frontend/runtime/cache',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'defaultTimeZone' => 'Europe/Moscow',
            'timeZone' => 'Europe/Kiev',
            'dateFormat' => 'dd.MM.yyyy',
            'datetimeFormat' => 'dd.MM.Y H:i:s',
            'timeFormat' => 'H:i:s', 
            'thousandSeparator' => ' ',
            'decimalSeparator' => ',',
            'currencyCode' => '',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'categories' => ['parse_m'],
                    'logTable' => 'log_work_chek',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['parse_package'],
                    'levels' => ['info'],
                    'logFile' => '@runtime/logs/parse_package.log',
                    'logVars' => []
                ],
            ],
        ],
    ],
];
